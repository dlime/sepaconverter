﻿using Sofimo.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sofimo.Services
{
    public class MSAccessService
    {
        public List<Eigendom_Huurder> GetTransferList(DateTime firstDayMonth)
        {
            var result = Data.Eigendom_Huurder.GetAllList().Where(l => l.Eigendom_Huurder_Begin_huur <= firstDayMonth && (l.Eigendom_Huurder_Einde_huur == null || l.Eigendom_Huurder_Einde_huur > firstDayMonth)).ToList();

            return result;
        }

        public Huurder GetHuurder(int huurderfiche_Huurder_ID)
        {
            var huurder = Huurder.Get(huurderfiche_Huurder_ID);
            if (huurder == null)
                throw new Exception($"huurder niet gevonden: {huurderfiche_Huurder_ID}");

            return huurder;
        }

        public Huurderfiche GetHuurderFiche(int eigendom_Huurder_Huurder_ID)
        {
            var huurderFiche = Data.Huurderfiche.Get(eigendom_Huurder_Huurder_ID);
            if (huurderFiche == null)
                throw new Exception($"Huurdersfiche niet gevonden: {eigendom_Huurder_Huurder_ID}");
            else if (!huurderFiche.Huurderfiche_Huurder_ID.HasValue)
                throw new Exception($"HuurderFiche - Geen Huurderfiche_Huurder_ID ingevuld: {eigendom_Huurder_Huurder_ID}");

            return huurderFiche;
        }

        public Eigendom GetEigendom(int eigendom_Huurder_Eigendom_ID)
        {
            var eigendom = Eigendom.Get(eigendom_Huurder_Eigendom_ID);
            if (eigendom == null)
                throw new Exception($"Eigendom niet gevonden: {eigendom_Huurder_Eigendom_ID}");

            return eigendom;
        }

        public Eigenaar GetEigenaar(int eigendom_Eigenaar_ID)
        {
            var eigenaar = Eigenaar.Get(eigendom_Eigenaar_ID);
            if (eigenaar == null)
                throw new Exception($"Eigenaar niet gevonden: {eigendom_Eigenaar_ID}");
            return eigenaar;
        }

        public Eigendom_Eigenaar GetEigendomEigenaar(int eigendomID)
        {
            var eigendomEigenaar = Eigendom_Eigenaar.Get(eigendomID);
            if (eigendomEigenaar == null)
                throw new Exception($"Eigendom eigenaar niet gevonden: {eigendomID}");

            return eigendomEigenaar;
        }
    }
}
