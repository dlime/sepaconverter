﻿using Sofimo.IBANBIC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sofimo.Services
{
    public class IBANService
    {
        private BANBICSoapClient _SoapClient;

        public IBANService()
        {
            _SoapClient = new IBANBIC.BANBICSoapClient("IBANBICSoap");
        }

        public IbanBicResult ConvertRekNummerToIBAN(string rekNummer)
        {
            string rek = rekNummer;
            rek = rek.Replace("-", "").Trim();
            var ibanWithBIC = (_SoapClient.BBANtoIBANandBIC(rekNummer)).Split('#');

            string iban = ibanWithBIC[0].Replace(" ", "").Trim();
            string bic = ibanWithBIC[1].Replace(" ", "").Trim();
            if (bic == "VRIJ")
                bic = "";

            return new IbanBicResult(iban, bic);
        }
    }

    public class IbanBicResult
    {
        public IbanBicResult(string iban, string bic)
        {
            this.IBAN = iban;
            this.BIC = bic;
        }
        public string IBAN { get; set; }
        public string BIC { get; set; }
    }
}
