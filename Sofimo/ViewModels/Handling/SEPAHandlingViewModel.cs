﻿using Sofimo.Data;
using Sofimo.Helper;
using Sofimo.Services;
using SpainHoliday.SepaWriter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sofimo.ViewModels.Handling
{
    public class SEPAHandlingViewModel : Base.BaseScreen
    {
        public MSAccessService _Service;
        public IBANService _IBANService;

        public SEPAHandlingViewModel(MSAccessService service, IBANService ibanService)
        {
            _Service = service;
            _IBANService = ibanService;
        }

        private SepaCreditTransfer _CreditTransfer = null;

        public SepaCreditTransfer CreditTransfer
        {
            get { return _CreditTransfer; }
            set
            {
                _CreditTransfer = value;
                NotifyOfPropertyChange();
            }
        }

        private AsyncObservableCollection<SepaCreditTransferTransaction> _SEPACreditTransfers = new AsyncObservableCollection<SepaCreditTransferTransaction>();

        public AsyncObservableCollection<SepaCreditTransferTransaction> SEPACreditTransfers
        {
            get { return _SEPACreditTransfers; }
            set
            {
                _SEPACreditTransfers = value;
                NotifyOfPropertyChange();
            }
        }

        private AsyncObservableCollection<ErrorSEPAHandling> _Errors = new AsyncObservableCollection<ErrorSEPAHandling>();

        public AsyncObservableCollection<ErrorSEPAHandling> SEPAErrors
        {
            get { return _Errors; }
            set
            {
                _Errors = value;
                NotifyOfPropertyChange();
            }
        }

        public void GetTransfers()
        {
            CreditTransfer = null;
            SEPAErrors.Clear();
            SEPACreditTransfers.Clear();

            RunInTask((taskScheduler) =>
            {
                var creditorName = System.Configuration.ConfigurationManager.AppSettings["Name"];
                var creditorBIC = System.Configuration.ConfigurationManager.AppSettings["BIC"];
                var creditorIBAN = System.Configuration.ConfigurationManager.AppSettings["IBAN"];
                CreditTransfer = new SepaCreditTransfer
                {
                    MessageIdentification = DateTime.Now.ToString("yyyyMMddHHmmssfff"),// Guid.NewGuid().ToString(),
                    InitiatingPartyName = creditorName,
                    // Below, your bank data
                    Debtor = new SepaIbanData { Bic = creditorBIC, Iban = creditorIBAN, Name = creditorName }
                };

                DateTime now = DateTime.Now;
                DateTime dteFirstDayMonth = new DateTime(now.Year, now.Month, 1);
                DateTime dteLastDayOfMonth = dteFirstDayMonth.AddMonths(1).AddDays(-1);

                double btwPercentage = SofimoHelper.GetBTWPercentage();

                var result = _Service.GetTransferList(dteFirstDayMonth);
                Progress.Text = $"Totaal SEPA aan te maken: {result.Count()}";
                int current = 0;
                Parallel.ForEach(result,
                //new ParallelOptions { MaxDegreeOfParallelism = 25 }, 
                (eigendomHuurder) =>
                {
                    // foreach (var eigendomHuurder in result)
                    // {
                    var eigendomEigenaar = _Service.GetEigendomEigenaar(eigendomHuurder.Eigendom_Huurder_Eigendom_ID);
                    var eigenaar = _Service.GetEigenaar(eigendomEigenaar.Eigendom_Eigenaar_ID);
                    var eigendom = _Service.GetEigendom(eigendomHuurder.Eigendom_Huurder_Eigendom_ID);

                    try
                    {
                        var huurderFiche = _Service.GetHuurderFiche(eigendomHuurder.Eigendom_Huurder_Huurder_ID);
                        var huurder = _Service.GetHuurder(huurderFiche.Huurderfiche_Huurder_ID.Value);

                        if (!string.IsNullOrEmpty(eigenaar.Eigenaar_Rekeningnummer))
                        {
                            DateTime eindeHuur = eigendomHuurder.Eigendom_Huurder_Einde_huur.GetValueOrDefault(DateTime.MaxValue);

                            int dagen = 0;
                            try
                            {

                                if (eindeHuur.Year == dteFirstDayMonth.Year && eindeHuur.Month == dteFirstDayMonth.Month)
                                    dagen = eindeHuur.Day;
                                else
                                    dagen = dteLastDayOfMonth.Day;
                            }
                            catch (Exception ex)
                            {
                                dagen = dteLastDayOfMonth.Day;
                            }

                            var eigendom_EigenaarList = Eigendom_Eigenaar.GetAllList(eigendomHuurder.Eigendom_Huurder_Eigendom_ID);

                            foreach (var eel in eigendom_EigenaarList)
                            {
                                if ((!eigendom.Eigendom_Einde_inning.HasValue || eigendom.Eigendom_Einde_inning.Value > dteFirstDayMonth) && !eel.Eigendom_Eigenaar_Geblokkeerd)
                                {
                                    double huurAandeel = Convert.ToDouble(eel.Eigendom_Eigenaar_Huuraandeel) / 100;

                                    double huur = (Convert.ToDouble(eigendomHuurder.Eigendom_Huurder_Huur_Euro) / dteLastDayOfMonth.Day) * dagen * huurAandeel;
                                    double vasteKost = (Convert.ToDouble(eigendomHuurder.Eigendom_Huurder_Syndic_Euro) / dteLastDayOfMonth.Day) * dagen * huurAandeel;
                                    double bedrag = huur + vasteKost;
                                    double commissie = Convert.ToDouble(eigendom.Eigendom_Commissie);
                                    double commissiebedrag = 0;
                                    double btwBedrag = 0;

                                    //Set this guy to FREE!
                                    if (eigendom.Eigendom_Gratis_tot.HasValue)
                                    {
                                        if (dteLastDayOfMonth < eigendom.Eigendom_Gratis_tot.Value)
                                        {
                                            commissie = 0;
                                        }
                                    }

                                    //TODO: is this correct that the payment is only + VAT when there is NO VAT number? Why is this?
                                    if (string.IsNullOrEmpty(eigenaar.Eigenaar_BTW_nummer))
                                    {
                                        if (eigendom.Eigendom_Enkel_huur)
                                        {
                                            bedrag = huur - (huur * commissie * btwPercentage / 100) + vasteKost;
                                            commissiebedrag = huur * commissie / 100;
                                            btwBedrag = huur * commissie / 100 * (btwPercentage - 1);
                                        }
                                        else
                                        {
                                            bedrag = huur + vasteKost - (huur + vasteKost) * commissie * btwPercentage / 100;
                                            commissiebedrag = (huur + vasteKost) * commissie / 100;
                                            btwBedrag = (huur + vasteKost) * commissie / 100 * (btwPercentage - 1);
                                        }
                                    }

                                    if (eigendom.Eigendom_Doorbetalen == 2)
                                    {
                                        if ((DateTime.Now.Month + 2) % 3 == 0)
                                        {
                                            bedrag *= 3;
                                            commissiebedrag *= 3;
                                            btwBedrag *= 3;
                                        }
                                        else
                                        {
                                            bedrag = 0;
                                            commissiebedrag = 0;
                                            btwBedrag = 0;
                                        }
                                    }

                                    if (bedrag > 0)
                                    {
                                        var IBANAndBic = _IBANService.ConvertRekNummerToIBAN(eigenaar.Eigenaar_Rekeningnummer);

                                        var creditor = new SepaIbanData
                                        {
                                            Iban = IBANAndBic.IBAN,
                                            Name = eigenaar.Eigenaar_Naam
                                        };

                                        if (!string.IsNullOrEmpty(IBANAndBic.BIC))
                                            creditor.Bic = IBANAndBic.BIC;
                                        else
                                            creditor.UnknownBic = true;

                                        var sepaTransfer = new SepaCreditTransferTransaction
                                        {
                                            Creditor = creditor,
                                            Amount = Convert.ToDecimal(bedrag.ToString("N2")),
                                            RemittanceInformation = $"huur {huurder.Huurder_Naam}"
                                            //$"{eigenaar.Eigenaar_Naam} (huurder {huurder.Huurder_Naam}): € {bedrag.ToString("N2")}";
                                        };
                                        CreditTransfer.AddCreditTransfer(sepaTransfer);

                                        Task.Factory.StartNew(() =>
                                        {
                                            SEPACreditTransfers.Add(sepaTransfer);
                                        }, CancellationToken.None, TaskCreationOptions.None, taskScheduler);
                                    }
                                }
                                else
                                {
                                    //TODO: message that this one had einde_inning before firstday || geblokkeerd is false
                                    if (eel.Eigendom_Eigenaar_Geblokkeerd)
                                        throw new Exception("Eigendom - eigenaar geblokeerd");
                                    else
                                    {
                                        var eindeInningValue = eigendom.Eigendom_Einde_inning.HasValue ? eigendom.Eigendom_Einde_inning.Value.ToString("dd/MM/yyyy") : "";
                                        throw new Exception($"Eigendom - eigenaar einde inning ({eindeInningValue}) voor eerste dag maand ({dteFirstDayMonth.ToString("dd/MM/yyyy")})");
                                    }
                                }
                            }
                        }
                        else
                        {
                            throw new Exception("Eigenaar heeft geen rekeningnummer ingevuld.");

                        }
                    }
                    catch (Exception ex)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            SEPAErrors.Add(new ErrorSEPAHandling(eigenaar, eigendom, ex.Message));
                        }, CancellationToken.None, TaskCreationOptions.None, taskScheduler);
                    }
                    finally
                    {
                        current++;
                        Progress.Text = $"SEPA progress: {current}/{result.Count()}";
                    }
                    // }
                });

            });
        }

        public void GenerateSEPA()
        {
            if (CreditTransfer != null)
            {
                string folder = @"c:\_temp";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                CreditTransfer.Save(System.IO.Path.Combine(folder, $"SEPA_{CreditTransfer.MessageIdentification}.xml"));

                //TODO: Save the result to MS ACCESS somewhere???
            }
        }
    }
}
