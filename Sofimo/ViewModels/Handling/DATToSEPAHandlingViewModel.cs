﻿using Microsoft.Win32;
using Sofimo.Helper;
using Sofimo.Services;
using SpainHoliday.SepaWriter;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Sofimo.ViewModels.Handling
{
    public class DATToSEPAHandlingViewModel : Base.BaseScreen
    {
        public IBANService _IBANService;
        private SepaCreditTransfer _CreditTransfer = null;
        private AsyncObservableCollection<SepaCreditTransferTransaction> _SEPACreditTransfers = new AsyncObservableCollection<SepaCreditTransferTransaction>();
        private AsyncObservableCollection<ErrorSEPAHandling> _Errors = new AsyncObservableCollection<ErrorSEPAHandling>();

        public DATToSEPAHandlingViewModel(IBANService ibanService)
        {
            _IBANService = ibanService;

            if (!string.IsNullOrEmpty(ParameterHelper.FileFromRun))
            {
                this.FileLocation = ParameterHelper.FileFromRun;
            }
        }

        public SepaCreditTransfer CreditTransfer
        {
            get { return _CreditTransfer; }
            set
            {
                _CreditTransfer = value;
                NotifyOfPropertyChange();
            }
        }

        public AsyncObservableCollection<SepaCreditTransferTransaction> SEPACreditTransfers
        {
            get { return _SEPACreditTransfers; }
            set
            {
                _SEPACreditTransfers = value;
                NotifyOfPropertyChange();
            }
        }

        public AsyncObservableCollection<ErrorSEPAHandling> SEPAErrors
        {
            get { return _Errors; }
            set
            {
                _Errors = value;
                NotifyOfPropertyChange();
            }
        }

        private string _FileLocation;

        public string FileLocation
        {
            get { return _FileLocation; }
            set
            {
                _FileLocation = value;
                NotifyOfPropertyChange();
            }
        }

        public void BrowseFile()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.InitialDirectory = Properties.Settings.Default.Folder_Path;
            openFileDialog1.Filter = "dat files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() ?? false)
            {
                this.FileLocation = openFileDialog1.FileName;

                Properties.Settings.Default.Folder_Path = Path.GetDirectoryName(openFileDialog1.FileName);
                Properties.Settings.Default.Save();
            }
        }

        public void ConvertFromDAT()
        {
            CreditTransfer = null;
            SEPAErrors.Clear();
            SEPACreditTransfers.Clear();

            if (!string.IsNullOrEmpty(FileLocation) && File.Exists(FileLocation))
            {
                RunInTask((taskScheduler) =>
                {
                    string folder = Path.GetDirectoryName(FileLocation);
                    // Read the file 
                    string firstRow = "";
                    int counter = 0;
                    List<string> rows = new List<string>();
                    using (System.IO.StreamReader file = new System.IO.StreamReader(FileLocation))
                    {
                        string fileLine;
                        while ((fileLine = file.ReadLine()) != null)
                        {
                            if (counter == 0)
                                firstRow = fileLine;
                            else
                                rows.Add(fileLine.Replace("Ë", "E").Replace("É", "E"));
                            counter++;
                        }
                    }

                    ProcessHeader(firstRow);

                    counter = 0;
                    int totalRows = rows.Count;

                    Parallel.ForEach(rows,
                   //new ParallelOptions { MaxDegreeOfParallelism = 25 }, 
                   (line) =>
                   {
                       //foreach (var line in rows)
                       // {
                       try
                       {
                           if (line.StartsWith("9"))
                           {
                               //Last line!
                           }
                           else
                           {
                               SepaCreditTransferTransaction sepaTransfer = ProcessRentPayment(line);

                               CreditTransfer.AddCreditTransfer(sepaTransfer);

                               Task.Factory.StartNew(() =>
                               {
                                   SEPACreditTransfers.Add(sepaTransfer);
                               }, CancellationToken.None, TaskCreationOptions.None, taskScheduler);
                           }
                       }
                       catch (Exception ex)
                       {

                       }
                       finally
                       {
                           counter++;
                           Progress.Text = $"SEPA progress: {counter}/{totalRows}";
                       }
                   });

                    if (CreditTransfer != null)
                    {
                        string processedDir = Path.Combine(folder, "Processed");
                        if (!Directory.Exists(processedDir))
                            Directory.CreateDirectory(processedDir);

                        string xmlFile = System.IO.Path.Combine(folder, $"SEPA_{Path.GetFileNameWithoutExtension(FileLocation)}_{CreditTransfer.MessageIdentification}.xml");
                        CreditTransfer.Save(xmlFile);

                        File.Move(FileLocation, Path.Combine(processedDir, Path.GetFileName(FileLocation)));

                        Task.Factory.StartNew(() =>
                        {
                            MessageBox.Show($"Omzetting voltooid! ({xmlFile})");
                        }, CancellationToken.None, TaskCreationOptions.None, taskScheduler);
                    }
                });
            }
            else
            {
                MessageBox.Show($"File: {FileLocation} does not exists.");
            }
        }

        private void ProcessHeader(string firstRow)
        {
            string lineFormatter = firstRow.Remove(0, 5);
            string day = lineFormatter.Substring(0, 2);
            string month = lineFormatter.Substring(2, 2);
            string year = "20" + lineFormatter.Substring(4, 2);

            lineFormatter = lineFormatter.Remove(0, 21);
            string rekNummer = lineFormatter.Substring(0, 12);
            string name = lineFormatter.Substring(12, 26).Trim();

            var IBANAndBic = _IBANService.ConvertRekNummerToIBAN(rekNummer);
            DateTime date = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
            CreditTransfer = new SepaCreditTransfer
            {
                MessageIdentification = date.ToString("yyyyMMdd") + DateTime.Now.ToString("HHmmssfff"),// Guid.NewGuid().ToString(),
                CreationDate = date,
                InitiatingPartyName = name,
                // Below, your bank data
                Debtor = new SepaIbanData { Bic = IBANAndBic.BIC, Iban = IBANAndBic.IBAN, Name = name }
            };
        }

        private SepaCreditTransferTransaction ProcessRentPayment(string line)
        {
            string lineFormatter = line.Remove(0, 23);
            string rekNummer = lineFormatter.Substring(0, 12);
            string bedragString = lineFormatter.Substring(12, 12).Trim();

            string name = RemoveDiacritics(lineFormatter.Substring(24, 26).Trim().Replace("&", "+"));
            lineFormatter = lineFormatter.Remove(0, 50);
            lineFormatter = lineFormatter.Remove(0, 13);
            string huurderName = RemoveDiacritics(lineFormatter.Substring(0, 41).Trim().Replace("&", "+"));

            var bedrag = Convert.ToDecimal(bedragString) / 100;
            var IBANAndBic = _IBANService.ConvertRekNummerToIBAN(rekNummer);
            var creditor = new SepaIbanData
            {
                Iban = IBANAndBic.IBAN,
                Name = name
            };

            if (!string.IsNullOrEmpty(IBANAndBic.BIC))
                creditor.Bic = IBANAndBic.BIC;
            else
                creditor.UnknownBic = true;

            var sepaTransfer = new SepaCreditTransferTransaction
            {
                Creditor = creditor,
                Amount = Convert.ToDecimal(bedrag.ToString("N2")),
                RemittanceInformation = $"huur {huurderName}"
                //$"{eigenaar.Eigenaar_Naam} (huurder {huurder.Huurder_Naam}): € {bedrag.ToString("N2")}";
            };

            return sepaTransfer;
        }

        /// <summary>
        /// https://stackoverflow.com/questions/249087/how-do-i-remove-diacritics-accents-from-a-string-in-net
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        static string RemoveDiacritics(string text)
        {
            if (text.StartsWith("PILLEN REN"))
            {

            }
            text = text.Replace("�", "E").Replace("Ë", "E").Replace("É", "E");

            return text;
            //byte[] tempBytes;
            //tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(text);
            //return System.Text.Encoding.UTF8.GetString(tempBytes);

            //var normalizedString = text.Normalize(NormalizationForm.FormD);
            //var stringBuilder = new StringBuilder();

            //foreach (var c in normalizedString)
            //{
            //    var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
            //    if (unicodeCategory != UnicodeCategory.NonSpacingMark)
            //    {
            //        stringBuilder.Append(c);
            //    }
            //}

            //return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}
