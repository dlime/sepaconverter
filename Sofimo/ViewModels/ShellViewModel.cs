﻿using Caliburn.Micro;
using Sofimo.Helper;
using Sofimo.ViewModels.Handling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Sofimo.ViewModels
{
    public class ShellViewModel : Base.BaseConductor
    {

        public ShellViewModel()
        {
            this.Title = "Sofimo verwerking";
            //this.ActiveItem = IoC.Get<SEPAHandlingViewModel>();
            this.ActiveItem = IoC.Get<DATToSEPAHandlingViewModel>();
        }


        public void AddToRegistry()
        {
            try
            {
                ContextHelper.AddContextMenuItem(".dat", "SEPA", "Omzetten naar SEPA", System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName + " %1");
                MessageBox.Show("Toegevoegd aan explorer, je kunt nu .DAT bestanden rechtstreeks openen naar dit programma.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Om toe te voegen aan explorer extensie moet je dit programma uitvoeren als 'Administrator'");
            }
        }
    }
}
