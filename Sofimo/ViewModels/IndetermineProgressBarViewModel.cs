﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sofimo.ViewModels
{
    public class IndetermineProgressBarViewModel : Screen
    {
        private bool _IsLoading = false;
        private bool _IsLoading2 = false;
        private string _Text;

        public bool IsLoading
        {
            get { return _IsLoading || _IsLoading2; }
            set
            {
                _IsLoading = value;
                NotifyOfPropertyChange();
            }
        }

        public bool IsLoading2
        {
            get { return _IsLoading2; }
            set
            {
                _IsLoading2 = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("IsLoading2");
            }
        }

        public string Text
        {
            get { return _Text; }
            set
            {
                _Text = value;
                NotifyOfPropertyChange();
            }
        }

    }
}
