﻿using Caliburn.Micro;
using Dlime.Framework.WPF.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Sofimo.ViewModels.Base
{
    public class BaseConductor : Conductor<Screen>
    {
        private string _Title;

        public string Title
        {
            get { return _Title; }
            set
            {
                _Title = $"{value} {VersionHelper.GetRunningVersion_String()}";
                NotifyOfPropertyChange();
            }
        }

        private IndetermineProgressBarViewModel _Progress = new IndetermineProgressBarViewModel();

        public IndetermineProgressBarViewModel Progress
        {
            get { return _Progress; }
            set
            {
                _Progress = value;
                NotifyOfPropertyChange();
            }
        }

        public bool IsLoading
        {
            get { return Progress.IsLoading; }
            set
            {
                Progress.IsLoading = value;
                NotifyOfPropertyChange();
            }
        }

        public bool IsLoading2
        {
            get { return Progress.IsLoading2; }
            set
            {
                Progress.IsLoading2 = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("IsLoading");
            }
        }


        /// <summary>
        /// Runs the action in a task
        /// Use exceptionAction if you want another action then Messagebox.show
        /// </summary>
        /// <param name="action"></param>
        /// <param name="exceptionAction"></param>
        public void RunInTask(System.Action<System.Action<string>> action, System.Action<string> exceptionAction = null)
        {
            RunInTask(action, (loading) => Progress.IsLoading = loading, exceptionAction);
        }

        /// <summary>
        /// Runs the action in a task
        /// Use exceptionAction if you want another action then Messagebox.show
        /// </summary>
        /// <param name="action"></param>
        /// <param name="exceptionAction"></param>
        public void RunInTask(System.Action<System.Action<string>> action, System.Action<bool> isLoadingValue, System.Action<string> exceptionAction = null)
        {
            //TODO: same stuff as in RunInTask except for UIScheduler...
            if (exceptionAction == null)
            {
                exceptionAction = (msg) => { MessageBox.Show(msg); };
            }

            var UIScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            isLoadingValue(true);
            Task.Factory.StartNew(() =>
            {
                try
                {
                    action.Invoke(exceptionAction);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message, "EXCEPTION BaseScreen.RunInTask");
                    Task.Factory.StartNew(() =>
                    {
                        exceptionAction.Invoke(ex.Message);
                    }, CancellationToken.None, TaskCreationOptions.None, UIScheduler);
                }
                finally
                {
                    //Task.Factory.StartNew(() =>
                    //{
                    isLoadingValue(false);
                    //}, CancellationToken.None, TaskCreationOptions.None, UIScheduler);
                }
            });
        }

        public void RunInTask(System.Action action, System.Func<Exception> exceptionAction)
        {
            var UIScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            Progress.IsLoading = true;
            Task.Factory.StartNew(() =>
            {
                try
                {
                    action.Invoke();
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message, "EXCEPTION BaseScreen.RunInTask");
                    exceptionAction.Invoke();
                }
                finally
                {
                    Task.Factory.StartNew(() =>
                    {
                        Progress.IsLoading = false;
                    }, CancellationToken.None, TaskCreationOptions.None, UIScheduler);
                }
            });
        }
    }
}
