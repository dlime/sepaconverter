﻿using Caliburn.Micro;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Sofimo.ViewModels;
using Sofimo.Services;
using Sofimo.ViewModels.Handling;
using Sofimo.Helper;

namespace Sofimo
{
    public class Bootstrapper : BootstrapperBase
    {
        private IUnityContainer _container;

        public Bootstrapper()
        {
            try
            {
                Initialize();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bootstrap: " + ex.Message);
            }
        }

        protected override void Configure()
        {
            try
            {
                _container = new UnityContainer();
                _container.RegisterType<ShellViewModel, ShellViewModel>();

                _container.RegisterType<IWindowManager, WindowManager>(new ContainerControlledLifetimeManager());
                _container.RegisterType<SEPAHandlingViewModel, SEPAHandlingViewModel>();

                _container.RegisterType<MSAccessService, MSAccessService>(new ContainerControlledLifetimeManager());
                _container.RegisterType<IBANService, IBANService>(new ContainerControlledLifetimeManager());

                base.Configure();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Config: " + ex.Message);
            }
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            try
            {
                DisplayRootViewFor<ShellViewModel>();
            }
            catch (Exception ex)
            {
                MessageBox.Show("OnStartup: " + ex.Message);
                MessageBox.Show(ex.StackTrace);

                if (ex.InnerException != null)
                    MessageBox.Show(ex.InnerException.Message);
            }
        }

        protected override object GetInstance(Type service, string key)
        {
            if (service != null)
                return _container.Resolve(service);

            if (!string.IsNullOrWhiteSpace(key))
                return _container.Resolve(Type.GetType(key));

            return null;
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.ResolveAll(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

    }
}
