﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sofimo.Helper
{
    public static class ContextHelper
    {
        //Extension - Extension of the file (.zip, .txt etc.)
        //MenuName - Name for the menu item (Play, Open etc.)
        //MenuDescription - The actual text that will be shown
        //MenuCommand - Path to executable
        public static bool AddContextMenuItem(string Extension, string MenuName, string MenuDescription, string MenuCommand)
        {
            bool ret = false;
            RegistryKey rkey = Registry.ClassesRoot.OpenSubKey(Extension, true);
            if (rkey != null)
            {
                rkey.SetValue("", "Sofimo");

                var shellKey = rkey.OpenSubKey("shell");
                if (shellKey == null)
                    shellKey = rkey.CreateSubKey("shell");

                if (shellKey != null)
                {
                    RegistryKey sofimoKey = shellKey.OpenSubKey("Sofimo", true);
                    if (sofimoKey == null)
                        sofimoKey = shellKey.CreateSubKey("Sofimo", true);

                    sofimoKey.SetValue("", MenuName);

                    RegistryKey commandKey = sofimoKey.OpenSubKey("command", true);
                    if (commandKey == null)
                        commandKey = sofimoKey.CreateSubKey("command", true);

                    commandKey.SetValue("", MenuCommand);

                    ret = true;
                    shellKey.Close();
                }
                //    }
                //}
                rkey.Close();
            }
            return ret;
        }
    }
}
