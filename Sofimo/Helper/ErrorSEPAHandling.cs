﻿using Sofimo.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sofimo.Helper
{
    public class ErrorSEPAHandling
    {
        public ErrorSEPAHandling(Eigenaar eigenaar, Eigendom eigendom, string errorMessage)
        {
            this.Eigenaar = eigenaar;
            this.Eigendom = eigendom;
            this.ErrorMessage = errorMessage;
        }
        public Eigenaar Eigenaar { get; set; }
        public Eigendom Eigendom { get; set; }
        public string ErrorMessage { get; set; }

        public override string ToString()
        {
            return $"{Eigenaar.Eigenaar_Naam} - {Eigendom.Eigendom_Straat}: {ErrorMessage}";
        }
    }
}
