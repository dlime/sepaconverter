﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sofimo.Helper
{
    public static class SofimoHelper
    {
        public static double GetBTWPercentage()
        {
            double btw = 1.21;
            double.TryParse(System.Configuration.ConfigurationManager.AppSettings.Get("BTWpercentage"), out btw);
            return btw;
        }
    }
}
