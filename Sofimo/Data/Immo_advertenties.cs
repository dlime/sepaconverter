using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Immo_advertenties
	{
		#region Properties
		public int ImmoAdv_ID { get; set; }
		public int? ImmoAdv_Krant_ID { get; set; }
		public int? ImmoAdv_Immofiche_ID { get; set; }
		public DateTime? ImmoAdv_Datum { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Immo advertenties]
								(
									[ImmoAdv Krant-ID],
									[ImmoAdv Immofiche-ID],
									[ImmoAdv Datum]
								)
								VALUES
								(
									@ImmoAdv_Krant_ID,
									@ImmoAdv_Immofiche_ID,
									@ImmoAdv_Datum
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@ImmoAdv_Krant_ID", OleDbType.Integer).Value = ImmoAdv_Krant_ID == null ? (Object)DBNull.Value : ImmoAdv_Krant_ID;
					cmd.Parameters.Add("@ImmoAdv_Immofiche_ID", OleDbType.Integer).Value = ImmoAdv_Immofiche_ID == null ? (Object)DBNull.Value : ImmoAdv_Immofiche_ID;
					cmd.Parameters.Add("@ImmoAdv_Datum", OleDbType.Date).Value = ImmoAdv_Datum == null ? (Object)DBNull.Value : ImmoAdv_Datum;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					ImmoAdv_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Immo advertenties]
								SET		[ImmoAdv Krant-ID] = @ImmoAdv_Krant_ID,
										[ImmoAdv Immofiche-ID] = @ImmoAdv_Immofiche_ID,
										[ImmoAdv Datum] = @ImmoAdv_Datum
								WHERE	[ImmoAdv-ID] = @ImmoAdv_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@ImmoAdv_Krant_ID", OleDbType.Integer).Value = ImmoAdv_Krant_ID == null ? (Object)DBNull.Value : ImmoAdv_Krant_ID;
					cmd.Parameters.Add("@ImmoAdv_Immofiche_ID", OleDbType.Integer).Value = ImmoAdv_Immofiche_ID == null ? (Object)DBNull.Value : ImmoAdv_Immofiche_ID;
					cmd.Parameters.Add("@ImmoAdv_Datum", OleDbType.Date).Value = ImmoAdv_Datum == null ? (Object)DBNull.Value : ImmoAdv_Datum;
					cmd.Parameters.Add("@ImmoAdv_ID", OleDbType.Integer).Value = ImmoAdv_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int immoAdv_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Immo advertenties]
								WHERE	[ImmoAdv-ID] = @ImmoAdv_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@ImmoAdv_ID", OleDbType.Integer).Value = immoAdv_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Immo_advertenties Get(int immoAdv_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[ImmoAdv-ID],
										[ImmoAdv Krant-ID],
										[ImmoAdv Immofiche-ID],
										[ImmoAdv Datum]
								FROM	[Immo advertenties]
								WHERE	[ImmoAdv-ID] = @ImmoAdv_ID;";

				Immo_advertenties immo_advertenties = new Immo_advertenties();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@ImmoAdv_ID", OleDbType.Integer).Value = immoAdv_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							immo_advertenties.ImmoAdv_ID = Convert.ToInt32(reader["ImmoAdv-ID"]);
							immo_advertenties.ImmoAdv_Krant_ID = reader["ImmoAdv Krant-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["ImmoAdv Krant-ID"]);
							immo_advertenties.ImmoAdv_Immofiche_ID = reader["ImmoAdv Immofiche-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["ImmoAdv Immofiche-ID"]);
							immo_advertenties.ImmoAdv_Datum = reader["ImmoAdv Datum"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["ImmoAdv Datum"]);
						}
					}
				}

				return immo_advertenties;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[ImmoAdv-ID],
										[ImmoAdv Krant-ID],
										[ImmoAdv Immofiche-ID],
										[ImmoAdv Datum]
								FROM	[Immo advertenties];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}