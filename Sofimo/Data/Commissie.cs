using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Commissie
	{
		#region Properties
		public int Commissie_ID { get; set; }
		public string Commissie_Code { get; set; }
		public string Commissie1 { get; set; }
		public short? Commissie_Sortering { get; set; }
		public bool Commissie_Enkel_staat_makelaar { get; set; }
		public byte? Commissie_Binnen_buiten { get; set; }
		public byte? Commissie_Verdeling_binnen { get; set; }
		public byte? Commissie_Verdeling_buiten { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Commissie]
								(
									[Commissie Code],
									[Commissie],
									[Commissie Sortering],
									[Commissie Enkel staat makelaar],
									[Commissie Binnen-buiten],
									[Commissie Verdeling binnen],
									[Commissie Verdeling buiten]
								)
								VALUES
								(
									@Commissie_Code,
									@Commissie,
									@Commissie_Sortering,
									@Commissie_Enkel_staat_makelaar,
									@Commissie_Binnen_buiten,
									@Commissie_Verdeling_binnen,
									@Commissie_Verdeling_buiten
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Commissie_Code", OleDbType.WChar).Value = Commissie_Code == null ? (Object)DBNull.Value : Commissie_Code;
					cmd.Parameters.Add("@Commissie", OleDbType.WChar).Value = Commissie1;
					cmd.Parameters.Add("@Commissie_Sortering", OleDbType.SmallInt).Value = Commissie_Sortering == null ? (Object)DBNull.Value : Commissie_Sortering;
					cmd.Parameters.Add("@Commissie_Enkel_staat_makelaar", OleDbType.Boolean).Value = Commissie_Enkel_staat_makelaar;
					cmd.Parameters.Add("@Commissie_Binnen_buiten", OleDbType.UnsignedTinyInt).Value = Commissie_Binnen_buiten == null ? (Object)DBNull.Value : Commissie_Binnen_buiten;
					cmd.Parameters.Add("@Commissie_Verdeling_binnen", OleDbType.UnsignedTinyInt).Value = Commissie_Verdeling_binnen == null ? (Object)DBNull.Value : Commissie_Verdeling_binnen;
					cmd.Parameters.Add("@Commissie_Verdeling_buiten", OleDbType.UnsignedTinyInt).Value = Commissie_Verdeling_buiten == null ? (Object)DBNull.Value : Commissie_Verdeling_buiten;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Commissie_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Commissie]
								SET		[Commissie Code] = @Commissie_Code,
										[Commissie] = @Commissie,
										[Commissie Sortering] = @Commissie_Sortering,
										[Commissie Enkel staat makelaar] = @Commissie_Enkel_staat_makelaar,
										[Commissie Binnen-buiten] = @Commissie_Binnen_buiten,
										[Commissie Verdeling binnen] = @Commissie_Verdeling_binnen,
										[Commissie Verdeling buiten] = @Commissie_Verdeling_buiten
								WHERE	[Commissie-ID] = @Commissie_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Commissie_Code", OleDbType.WChar).Value = Commissie_Code == null ? (Object)DBNull.Value : Commissie_Code;
					cmd.Parameters.Add("@Commissie", OleDbType.WChar).Value = Commissie1;
					cmd.Parameters.Add("@Commissie_Sortering", OleDbType.SmallInt).Value = Commissie_Sortering == null ? (Object)DBNull.Value : Commissie_Sortering;
					cmd.Parameters.Add("@Commissie_Enkel_staat_makelaar", OleDbType.Boolean).Value = Commissie_Enkel_staat_makelaar;
					cmd.Parameters.Add("@Commissie_Binnen_buiten", OleDbType.UnsignedTinyInt).Value = Commissie_Binnen_buiten == null ? (Object)DBNull.Value : Commissie_Binnen_buiten;
					cmd.Parameters.Add("@Commissie_Verdeling_binnen", OleDbType.UnsignedTinyInt).Value = Commissie_Verdeling_binnen == null ? (Object)DBNull.Value : Commissie_Verdeling_binnen;
					cmd.Parameters.Add("@Commissie_Verdeling_buiten", OleDbType.UnsignedTinyInt).Value = Commissie_Verdeling_buiten == null ? (Object)DBNull.Value : Commissie_Verdeling_buiten;
					cmd.Parameters.Add("@Commissie_ID", OleDbType.Integer).Value = Commissie_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int commissie_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Commissie]
								WHERE	[Commissie-ID] = @Commissie_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Commissie_ID", OleDbType.Integer).Value = commissie_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Commissie Get(int commissie_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Commissie-ID],
										[Commissie Code],
										[Commissie],
										[Commissie Sortering],
										[Commissie Enkel staat makelaar],
										[Commissie Binnen-buiten],
										[Commissie Verdeling binnen],
										[Commissie Verdeling buiten]
								FROM	[Commissie]
								WHERE	[Commissie-ID] = @Commissie_ID;";

				Commissie commissie = new Commissie();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Commissie_ID", OleDbType.Integer).Value = commissie_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							commissie.Commissie_ID = Convert.ToInt32(reader["Commissie-ID"]);
							commissie.Commissie_Code = reader["Commissie Code"] == DBNull.Value ? null : reader["Commissie Code"].ToString();
							commissie.Commissie1 = reader["Commissie"].ToString();
							commissie.Commissie_Sortering = reader["Commissie Sortering"] == DBNull.Value ? (short?)null : Convert.ToInt16(reader["Commissie Sortering"]);
							commissie.Commissie_Enkel_staat_makelaar = Convert.ToBoolean(reader["Commissie Enkel staat makelaar"]);
							commissie.Commissie_Binnen_buiten = reader["Commissie Binnen-buiten"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Commissie Binnen-buiten"]);
							commissie.Commissie_Verdeling_binnen = reader["Commissie Verdeling binnen"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Commissie Verdeling binnen"]);
							commissie.Commissie_Verdeling_buiten = reader["Commissie Verdeling buiten"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Commissie Verdeling buiten"]);
						}
					}
				}

				return commissie;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Commissie-ID],
										[Commissie Code],
										[Commissie],
										[Commissie Sortering],
										[Commissie Enkel staat makelaar],
										[Commissie Binnen-buiten],
										[Commissie Verdeling binnen],
										[Commissie Verdeling buiten]
								FROM	[Commissie];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}