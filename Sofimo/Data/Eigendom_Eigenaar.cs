using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
    public class Eigendom_Eigenaar
    {
        #region Properties
        public int Eigendom_Eigenaar_ID { get; set; }
        public int? Eigendom_Eigenaar_Eigendom_ID { get; set; }
        public int? Eigendom_Eigenaar_Eigenaar_ID { get; set; }
        public int? Eigendom_Eigenaar_Huuraandeel { get; set; }
        public bool Eigendom_Eigenaar_Geblokkeerd { get; set; }
        #endregion

        #region Add
        /// <summary>
        /// Adds a new record.
        /// </summary>
        public void Add()
        {
            String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                String sql = @"INSERT INTO [Eigendom-Eigenaar]
								(
									[Eigendom-Eigenaar Eigendom-ID],
									[Eigendom-Eigenaar Eigenaar-ID],
									[Eigendom-Eigenaar Huuraandeel],
									[Eigendom-Eigenaar Geblokkeerd]
								)
								VALUES
								(
									@Eigendom_Eigenaar_Eigendom_ID,
									@Eigendom_Eigenaar_Eigenaar_ID,
									@Eigendom_Eigenaar_Huuraandeel,
									@Eigendom_Eigenaar_Geblokkeerd
								);";

                con.Open();

                using (OleDbCommand cmd = new OleDbCommand(sql, con))
                {
                    cmd.Parameters.Add("@Eigendom_Eigenaar_Eigendom_ID", OleDbType.Integer).Value = Eigendom_Eigenaar_Eigendom_ID == null ? (Object)DBNull.Value : Eigendom_Eigenaar_Eigendom_ID;
                    cmd.Parameters.Add("@Eigendom_Eigenaar_Eigenaar_ID", OleDbType.Integer).Value = Eigendom_Eigenaar_Eigenaar_ID == null ? (Object)DBNull.Value : Eigendom_Eigenaar_Eigenaar_ID;
                    cmd.Parameters.Add("@Eigendom_Eigenaar_Huuraandeel", OleDbType.Integer).Value = Eigendom_Eigenaar_Huuraandeel == null ? (Object)DBNull.Value : Eigendom_Eigenaar_Huuraandeel;
                    cmd.Parameters.Add("@Eigendom_Eigenaar_Geblokkeerd", OleDbType.Boolean).Value = Eigendom_Eigenaar_Geblokkeerd;
                    cmd.ExecuteNonQuery();

                    // Get value of the identity column.
                    cmd.CommandText = "SELECT @@Identity;";
                    Eigendom_Eigenaar_ID = Convert.ToInt32(cmd.ExecuteScalar());
                }

                con.Close();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Updates an existing record.
        /// </summary>
        public void Update()
        {
            String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                String sql = @"UPDATE	[Eigendom-Eigenaar]
								SET		[Eigendom-Eigenaar Eigendom-ID] = @Eigendom_Eigenaar_Eigendom_ID,
										[Eigendom-Eigenaar Eigenaar-ID] = @Eigendom_Eigenaar_Eigenaar_ID,
										[Eigendom-Eigenaar Huuraandeel] = @Eigendom_Eigenaar_Huuraandeel,
										[Eigendom-Eigenaar Geblokkeerd] = @Eigendom_Eigenaar_Geblokkeerd
								WHERE	[Eigendom-Eigenaar-ID] = @Eigendom_Eigenaar_ID;";

                con.Open();

                using (OleDbCommand cmd = new OleDbCommand(sql, con))
                {
                    cmd.Parameters.Add("@Eigendom_Eigenaar_Eigendom_ID", OleDbType.Integer).Value = Eigendom_Eigenaar_Eigendom_ID == null ? (Object)DBNull.Value : Eigendom_Eigenaar_Eigendom_ID;
                    cmd.Parameters.Add("@Eigendom_Eigenaar_Eigenaar_ID", OleDbType.Integer).Value = Eigendom_Eigenaar_Eigenaar_ID == null ? (Object)DBNull.Value : Eigendom_Eigenaar_Eigenaar_ID;
                    cmd.Parameters.Add("@Eigendom_Eigenaar_Huuraandeel", OleDbType.Integer).Value = Eigendom_Eigenaar_Huuraandeel == null ? (Object)DBNull.Value : Eigendom_Eigenaar_Huuraandeel;
                    cmd.Parameters.Add("@Eigendom_Eigenaar_Geblokkeerd", OleDbType.Boolean).Value = Eigendom_Eigenaar_Geblokkeerd;
                    cmd.Parameters.Add("@Eigendom_Eigenaar_ID", OleDbType.Integer).Value = Eigendom_Eigenaar_ID;
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes an existing record.
        /// </summary>
        public static void Delete(int eigendom_Eigenaar_ID)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                String sql = @"DELETE	FROM [Eigendom-Eigenaar]
								WHERE	[Eigendom-Eigenaar-ID] = @Eigendom_Eigenaar_ID;";

                con.Open();

                using (OleDbCommand cmd = new OleDbCommand(sql, con))
                {
                    cmd.Parameters.Add("@Eigendom_Eigenaar_ID", OleDbType.Integer).Value = eigendom_Eigenaar_ID;
                    cmd.ExecuteNonQuery();
                }

                con.Close();
            }
        }
        #endregion

        #region Get

        /// <summary>
        /// Gets an existing record.
        /// </summary>
        public static List<Eigendom_Eigenaar> GetAllList(int eigendom_Huurder_Eigendom_ID)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                String sql = @"SELECT	[Eigendom-Eigenaar-ID],
										[Eigendom-Eigenaar Eigendom-ID],
										[Eigendom-Eigenaar Eigenaar-ID],
										[Eigendom-Eigenaar Huuraandeel],
										[Eigendom-Eigenaar Geblokkeerd]
								FROM	[Eigendom-Eigenaar]
								WHERE	[Eigendom-Eigenaar Eigendom-ID] = @Eigendom_Eigenaar_ID;";


                List<Eigendom_Eigenaar> eigendom_Eigenaar_List = new List<Eigendom_Eigenaar>();

                con.Open();

                using (OleDbCommand cmd = new OleDbCommand(sql, con))
                {
                    cmd.Parameters.Add("@Eigendom_Eigenaar_ID", OleDbType.Integer).Value = eigendom_Huurder_Eigendom_ID;

                    using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            Eigendom_Eigenaar eigendom_Eigenaar = new Eigendom_Eigenaar();
                            eigendom_Eigenaar.Eigendom_Eigenaar_ID = Convert.ToInt32(reader["Eigendom-Eigenaar-ID"]);
                            eigendom_Eigenaar.Eigendom_Eigenaar_Eigendom_ID = reader["Eigendom-Eigenaar Eigendom-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Eigenaar Eigendom-ID"]);
                            eigendom_Eigenaar.Eigendom_Eigenaar_Eigenaar_ID = reader["Eigendom-Eigenaar Eigenaar-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Eigenaar Eigenaar-ID"]);
                            eigendom_Eigenaar.Eigendom_Eigenaar_Huuraandeel = reader["Eigendom-Eigenaar Huuraandeel"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Eigenaar Huuraandeel"]);
                            eigendom_Eigenaar.Eigendom_Eigenaar_Geblokkeerd = Convert.ToBoolean(reader["Eigendom-Eigenaar Geblokkeerd"]);
                            eigendom_Eigenaar_List.Add(eigendom_Eigenaar);
                        }
                    }
                }

                return eigendom_Eigenaar_List;
            }
        }

        /// <summary>
        /// Gets an existing record.
        /// </summary>
        public static Eigendom_Eigenaar Get(int eigendom_Eigenaar_ID)
        {
            String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                String sql = @"SELECT	[Eigendom-Eigenaar-ID],
										[Eigendom-Eigenaar Eigendom-ID],
										[Eigendom-Eigenaar Eigenaar-ID],
										[Eigendom-Eigenaar Huuraandeel],
										[Eigendom-Eigenaar Geblokkeerd]
								FROM	[Eigendom-Eigenaar]
								WHERE	[Eigendom-Eigenaar-ID] = @Eigendom_Eigenaar_ID;";

                Eigendom_Eigenaar eigendom_Eigenaar = new Eigendom_Eigenaar();

                con.Open();

                using (OleDbCommand cmd = new OleDbCommand(sql, con))
                {
                    cmd.Parameters.Add("@Eigendom_Eigenaar_ID", OleDbType.Integer).Value = eigendom_Eigenaar_ID;

                    using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (reader.Read())
                        {
                            eigendom_Eigenaar.Eigendom_Eigenaar_ID = Convert.ToInt32(reader["Eigendom-Eigenaar-ID"]);
                            eigendom_Eigenaar.Eigendom_Eigenaar_Eigendom_ID = reader["Eigendom-Eigenaar Eigendom-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Eigenaar Eigendom-ID"]);
                            eigendom_Eigenaar.Eigendom_Eigenaar_Eigenaar_ID = reader["Eigendom-Eigenaar Eigenaar-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Eigenaar Eigenaar-ID"]);
                            eigendom_Eigenaar.Eigendom_Eigenaar_Huuraandeel = reader["Eigendom-Eigenaar Huuraandeel"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Eigenaar Huuraandeel"]);
                            eigendom_Eigenaar.Eigendom_Eigenaar_Geblokkeerd = Convert.ToBoolean(reader["Eigendom-Eigenaar Geblokkeerd"]);
                        }
                    }
                }

                return eigendom_Eigenaar;
            }
        }
        #endregion

        #region GetAll
        /// <summary>
        /// Gets all records.
        /// </summary>
        public static DataTable GetAll()
        {
            String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                DataTable dataTable = new DataTable();

                con.Open();

                String sql = @"SELECT	[Eigendom-Eigenaar-ID],
										[Eigendom-Eigenaar Eigendom-ID],
										[Eigendom-Eigenaar Eigenaar-ID],
										[Eigendom-Eigenaar Huuraandeel],
										[Eigendom-Eigenaar Geblokkeerd]
								FROM	[Eigendom-Eigenaar];";

                using (OleDbCommand cmd = new OleDbCommand(sql, con))
                {
                    using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        dataTable.Load(reader);
                    }
                }

                return dataTable;
            }
        }
        #endregion
    }
}