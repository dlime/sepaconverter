using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Firma
	{
		#region Properties
		public int Firma_ID { get; set; }
		public int? Firma_Gemeente_ID { get; set; }
		public int? Firma_Land_ID { get; set; }
		public string Firma_Naam { get; set; }
		public string Firma_Contactpersoon { get; set; }
		public string Firma_Straat { get; set; }
		public int? Firma_Faktuurnummer { get; set; }
		public int? Firma_Credietnota { get; set; }
		public int? Firma_Suwier_Decru { get; set; }
		public int? Firma_Delforge { get; set; }
		public string Firma_Rekeningnummer { get; set; }
		public string Firma_Rekeningnummer_Factuur { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Firma]
								(
									[Firma Gemeente-ID],
									[Firma Land-ID],
									[Firma Naam],
									[Firma Contactpersoon],
									[Firma Straat],
									[Firma Faktuurnummer],
									[Firma Credietnota],
									[Firma Suwier-Decru],
									[Firma Delforge],
									[Firma Rekeningnummer],
									[Firma Rekeningnummer Factuur]
								)
								VALUES
								(
									@Firma_Gemeente_ID,
									@Firma_Land_ID,
									@Firma_Naam,
									@Firma_Contactpersoon,
									@Firma_Straat,
									@Firma_Faktuurnummer,
									@Firma_Credietnota,
									@Firma_Suwier_Decru,
									@Firma_Delforge,
									@Firma_Rekeningnummer,
									@Firma_Rekeningnummer_Factuur
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Firma_Gemeente_ID", OleDbType.Integer).Value = Firma_Gemeente_ID == null ? (Object)DBNull.Value : Firma_Gemeente_ID;
					cmd.Parameters.Add("@Firma_Land_ID", OleDbType.Integer).Value = Firma_Land_ID == null ? (Object)DBNull.Value : Firma_Land_ID;
					cmd.Parameters.Add("@Firma_Naam", OleDbType.WChar).Value = Firma_Naam;
					cmd.Parameters.Add("@Firma_Contactpersoon", OleDbType.WChar).Value = Firma_Contactpersoon == null ? (Object)DBNull.Value : Firma_Contactpersoon;
					cmd.Parameters.Add("@Firma_Straat", OleDbType.WChar).Value = Firma_Straat == null ? (Object)DBNull.Value : Firma_Straat;
					cmd.Parameters.Add("@Firma_Faktuurnummer", OleDbType.Integer).Value = Firma_Faktuurnummer == null ? (Object)DBNull.Value : Firma_Faktuurnummer;
					cmd.Parameters.Add("@Firma_Credietnota", OleDbType.Integer).Value = Firma_Credietnota == null ? (Object)DBNull.Value : Firma_Credietnota;
					cmd.Parameters.Add("@Firma_Suwier_Decru", OleDbType.Integer).Value = Firma_Suwier_Decru == null ? (Object)DBNull.Value : Firma_Suwier_Decru;
					cmd.Parameters.Add("@Firma_Delforge", OleDbType.Integer).Value = Firma_Delforge == null ? (Object)DBNull.Value : Firma_Delforge;
					cmd.Parameters.Add("@Firma_Rekeningnummer", OleDbType.WChar).Value = Firma_Rekeningnummer == null ? (Object)DBNull.Value : Firma_Rekeningnummer;
					cmd.Parameters.Add("@Firma_Rekeningnummer_Factuur", OleDbType.WChar).Value = Firma_Rekeningnummer_Factuur == null ? (Object)DBNull.Value : Firma_Rekeningnummer_Factuur;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Firma_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Firma]
								SET		[Firma Gemeente-ID] = @Firma_Gemeente_ID,
										[Firma Land-ID] = @Firma_Land_ID,
										[Firma Naam] = @Firma_Naam,
										[Firma Contactpersoon] = @Firma_Contactpersoon,
										[Firma Straat] = @Firma_Straat,
										[Firma Faktuurnummer] = @Firma_Faktuurnummer,
										[Firma Credietnota] = @Firma_Credietnota,
										[Firma Suwier-Decru] = @Firma_Suwier_Decru,
										[Firma Delforge] = @Firma_Delforge,
										[Firma Rekeningnummer] = @Firma_Rekeningnummer,
										[Firma Rekeningnummer Factuur] = @Firma_Rekeningnummer_Factuur
								WHERE	[Firma-ID] = @Firma_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Firma_Gemeente_ID", OleDbType.Integer).Value = Firma_Gemeente_ID == null ? (Object)DBNull.Value : Firma_Gemeente_ID;
					cmd.Parameters.Add("@Firma_Land_ID", OleDbType.Integer).Value = Firma_Land_ID == null ? (Object)DBNull.Value : Firma_Land_ID;
					cmd.Parameters.Add("@Firma_Naam", OleDbType.WChar).Value = Firma_Naam;
					cmd.Parameters.Add("@Firma_Contactpersoon", OleDbType.WChar).Value = Firma_Contactpersoon == null ? (Object)DBNull.Value : Firma_Contactpersoon;
					cmd.Parameters.Add("@Firma_Straat", OleDbType.WChar).Value = Firma_Straat == null ? (Object)DBNull.Value : Firma_Straat;
					cmd.Parameters.Add("@Firma_Faktuurnummer", OleDbType.Integer).Value = Firma_Faktuurnummer == null ? (Object)DBNull.Value : Firma_Faktuurnummer;
					cmd.Parameters.Add("@Firma_Credietnota", OleDbType.Integer).Value = Firma_Credietnota == null ? (Object)DBNull.Value : Firma_Credietnota;
					cmd.Parameters.Add("@Firma_Suwier_Decru", OleDbType.Integer).Value = Firma_Suwier_Decru == null ? (Object)DBNull.Value : Firma_Suwier_Decru;
					cmd.Parameters.Add("@Firma_Delforge", OleDbType.Integer).Value = Firma_Delforge == null ? (Object)DBNull.Value : Firma_Delforge;
					cmd.Parameters.Add("@Firma_Rekeningnummer", OleDbType.WChar).Value = Firma_Rekeningnummer == null ? (Object)DBNull.Value : Firma_Rekeningnummer;
					cmd.Parameters.Add("@Firma_Rekeningnummer_Factuur", OleDbType.WChar).Value = Firma_Rekeningnummer_Factuur == null ? (Object)DBNull.Value : Firma_Rekeningnummer_Factuur;
					cmd.Parameters.Add("@Firma_ID", OleDbType.Integer).Value = Firma_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int firma_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Firma]
								WHERE	[Firma-ID] = @Firma_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Firma_ID", OleDbType.Integer).Value = firma_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Firma Get(int firma_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Firma-ID],
										[Firma Gemeente-ID],
										[Firma Land-ID],
										[Firma Naam],
										[Firma Contactpersoon],
										[Firma Straat],
										[Firma Faktuurnummer],
										[Firma Credietnota],
										[Firma Suwier-Decru],
										[Firma Delforge],
										[Firma Rekeningnummer],
										[Firma Rekeningnummer Factuur]
								FROM	[Firma]
								WHERE	[Firma-ID] = @Firma_ID;";

				Firma firma = new Firma();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Firma_ID", OleDbType.Integer).Value = firma_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							firma.Firma_ID = Convert.ToInt32(reader["Firma-ID"]);
							firma.Firma_Gemeente_ID = reader["Firma Gemeente-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Firma Gemeente-ID"]);
							firma.Firma_Land_ID = reader["Firma Land-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Firma Land-ID"]);
							firma.Firma_Naam = reader["Firma Naam"].ToString();
							firma.Firma_Contactpersoon = reader["Firma Contactpersoon"] == DBNull.Value ? null : reader["Firma Contactpersoon"].ToString();
							firma.Firma_Straat = reader["Firma Straat"] == DBNull.Value ? null : reader["Firma Straat"].ToString();
							firma.Firma_Faktuurnummer = reader["Firma Faktuurnummer"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Firma Faktuurnummer"]);
							firma.Firma_Credietnota = reader["Firma Credietnota"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Firma Credietnota"]);
							firma.Firma_Suwier_Decru = reader["Firma Suwier-Decru"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Firma Suwier-Decru"]);
							firma.Firma_Delforge = reader["Firma Delforge"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Firma Delforge"]);
							firma.Firma_Rekeningnummer = reader["Firma Rekeningnummer"] == DBNull.Value ? null : reader["Firma Rekeningnummer"].ToString();
							firma.Firma_Rekeningnummer_Factuur = reader["Firma Rekeningnummer Factuur"] == DBNull.Value ? null : reader["Firma Rekeningnummer Factuur"].ToString();
						}
					}
				}

				return firma;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Firma-ID],
										[Firma Gemeente-ID],
										[Firma Land-ID],
										[Firma Naam],
										[Firma Contactpersoon],
										[Firma Straat],
										[Firma Faktuurnummer],
										[Firma Credietnota],
										[Firma Suwier-Decru],
										[Firma Delforge],
										[Firma Rekeningnummer],
										[Firma Rekeningnummer Factuur]
								FROM	[Firma];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}