using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Huurderfiche
	{
		#region Properties
		public int Huurderfiche_ID { get; set; }
		public int? Huurderfiche_Huurder_ID { get; set; }
		public int Huurderfiche_Eigendom_Huurder_ID { get; set; }
		public string HuurderficheEigenaarLink { get; set; }
		public DateTime? Huurderfiche_Datum { get; set; }
		public int Huurderfiche_Referentie { get; set; }
		public int? Huurderfiche_Bedrag { get; set; }
		public double? Huurderfiche_Bedrag_Euro { get; set; }
		public DateTime? Huurderfiche_Betalen_tegen { get; set; }
		public int? Huurderfiche_Te_betalen_boete { get; set; }
		public double? Huurderfiche_Te_betalen_boete_Euro { get; set; }
		public bool Huurderfiche_Boete_kwijtgescholden { get; set; }
		public DateTime? Huurderfiche_Boete_betaald { get; set; }
		public string Huurderfiche_Commentaar { get; set; }
		public bool Huurderfiche_Lock { get; set; }
		public bool Huurderfiche_Aanmaning { get; set; }
		public DateTime? Huurderfiche_Datum_1ste_aanmaning { get; set; }
		public DateTime? Huurderfiche_Datum_2de_aanmaning { get; set; }
		public DateTime? Huurderfiche_Datum_3de_aanmaning { get; set; }
		public byte? Huurderfiche_Code_aanmaning { get; set; }
		public int? Huurderfiche_Aanmaning_totaal_betaald { get; set; }
		public double? Huurderfiche_Aanmaning_totaal_betaald_Euro { get; set; }
		public DateTime? Huurderfiche_Aanmaning_datum_laatste_betaling { get; set; }
		public int? Huurderfiche_Suwier_Decru { get; set; }
		public int? Huurderfiche_Delforge { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Huurderfiche]
								(
									[Huurderfiche Huurder-ID],
									[HuurderficheEigenaarLink],
									[Huurderfiche Datum],
									[Huurderfiche Bedrag],
									[Huurderfiche Bedrag Euro],
									[Huurderfiche Betalen tegen],
									[Huurderfiche Te betalen boete],
									[Huurderfiche Te betalen boete Euro],
									[Huurderfiche Boete kwijtgescholden],
									[Huurderfiche Boete betaald],
									[Huurderfiche Commentaar],
									[Huurderfiche Lock],
									[Huurderfiche Aanmaning],
									[Huurderfiche Datum 1ste aanmaning],
									[Huurderfiche Datum 2de aanmaning],
									[Huurderfiche Datum 3de aanmaning],
									[Huurderfiche Code aanmaning],
									[Huurderfiche Aanmaning totaal betaald],
									[Huurderfiche Aanmaning totaal betaald Euro],
									[Huurderfiche Aanmaning datum laatste betaling],
									[Huurderfiche Suwier-Decru],
									[Huurderfiche Delforge]
								)
								VALUES
								(
									@Huurderfiche_Huurder_ID,
									@HuurderficheEigenaarLink,
									@Huurderfiche_Datum,
									@Huurderfiche_Bedrag,
									@Huurderfiche_Bedrag_Euro,
									@Huurderfiche_Betalen_tegen,
									@Huurderfiche_Te_betalen_boete,
									@Huurderfiche_Te_betalen_boete_Euro,
									@Huurderfiche_Boete_kwijtgescholden,
									@Huurderfiche_Boete_betaald,
									@Huurderfiche_Commentaar,
									@Huurderfiche_Lock,
									@Huurderfiche_Aanmaning,
									@Huurderfiche_Datum_1ste_aanmaning,
									@Huurderfiche_Datum_2de_aanmaning,
									@Huurderfiche_Datum_3de_aanmaning,
									@Huurderfiche_Code_aanmaning,
									@Huurderfiche_Aanmaning_totaal_betaald,
									@Huurderfiche_Aanmaning_totaal_betaald_Euro,
									@Huurderfiche_Aanmaning_datum_laatste_betaling,
									@Huurderfiche_Suwier_Decru,
									@Huurderfiche_Delforge
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Huurderfiche_Huurder_ID", OleDbType.Integer).Value = Huurderfiche_Huurder_ID == null ? (Object)DBNull.Value : Huurderfiche_Huurder_ID;
					cmd.Parameters.Add("@HuurderficheEigenaarLink", OleDbType.WChar).Value = HuurderficheEigenaarLink == null ? (Object)DBNull.Value : HuurderficheEigenaarLink;
					cmd.Parameters.Add("@Huurderfiche_Datum", OleDbType.Date).Value = Huurderfiche_Datum == null ? (Object)DBNull.Value : Huurderfiche_Datum;
					cmd.Parameters.Add("@Huurderfiche_Bedrag", OleDbType.Integer).Value = Huurderfiche_Bedrag == null ? (Object)DBNull.Value : Huurderfiche_Bedrag;
					cmd.Parameters.Add("@Huurderfiche_Bedrag_Euro", OleDbType.Double).Value = Huurderfiche_Bedrag_Euro == null ? (Object)DBNull.Value : Huurderfiche_Bedrag_Euro;
					cmd.Parameters.Add("@Huurderfiche_Betalen_tegen", OleDbType.Date).Value = Huurderfiche_Betalen_tegen == null ? (Object)DBNull.Value : Huurderfiche_Betalen_tegen;
					cmd.Parameters.Add("@Huurderfiche_Te_betalen_boete", OleDbType.Integer).Value = Huurderfiche_Te_betalen_boete == null ? (Object)DBNull.Value : Huurderfiche_Te_betalen_boete;
					cmd.Parameters.Add("@Huurderfiche_Te_betalen_boete_Euro", OleDbType.Double).Value = Huurderfiche_Te_betalen_boete_Euro == null ? (Object)DBNull.Value : Huurderfiche_Te_betalen_boete_Euro;
					cmd.Parameters.Add("@Huurderfiche_Boete_kwijtgescholden", OleDbType.Boolean).Value = Huurderfiche_Boete_kwijtgescholden;
					cmd.Parameters.Add("@Huurderfiche_Boete_betaald", OleDbType.Date).Value = Huurderfiche_Boete_betaald == null ? (Object)DBNull.Value : Huurderfiche_Boete_betaald;
					cmd.Parameters.Add("@Huurderfiche_Commentaar", OleDbType.WChar).Value = Huurderfiche_Commentaar == null ? (Object)DBNull.Value : Huurderfiche_Commentaar;
					cmd.Parameters.Add("@Huurderfiche_Lock", OleDbType.Boolean).Value = Huurderfiche_Lock;
					cmd.Parameters.Add("@Huurderfiche_Aanmaning", OleDbType.Boolean).Value = Huurderfiche_Aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Datum_1ste_aanmaning", OleDbType.Date).Value = Huurderfiche_Datum_1ste_aanmaning == null ? (Object)DBNull.Value : Huurderfiche_Datum_1ste_aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Datum_2de_aanmaning", OleDbType.Date).Value = Huurderfiche_Datum_2de_aanmaning == null ? (Object)DBNull.Value : Huurderfiche_Datum_2de_aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Datum_3de_aanmaning", OleDbType.Date).Value = Huurderfiche_Datum_3de_aanmaning == null ? (Object)DBNull.Value : Huurderfiche_Datum_3de_aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Code_aanmaning", OleDbType.UnsignedTinyInt).Value = Huurderfiche_Code_aanmaning == null ? (Object)DBNull.Value : Huurderfiche_Code_aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Aanmaning_totaal_betaald", OleDbType.Integer).Value = Huurderfiche_Aanmaning_totaal_betaald == null ? (Object)DBNull.Value : Huurderfiche_Aanmaning_totaal_betaald;
					cmd.Parameters.Add("@Huurderfiche_Aanmaning_totaal_betaald_Euro", OleDbType.Double).Value = Huurderfiche_Aanmaning_totaal_betaald_Euro == null ? (Object)DBNull.Value : Huurderfiche_Aanmaning_totaal_betaald_Euro;
					cmd.Parameters.Add("@Huurderfiche_Aanmaning_datum_laatste_betaling", OleDbType.Date).Value = Huurderfiche_Aanmaning_datum_laatste_betaling == null ? (Object)DBNull.Value : Huurderfiche_Aanmaning_datum_laatste_betaling;
					cmd.Parameters.Add("@Huurderfiche_Suwier_Decru", OleDbType.Integer).Value = Huurderfiche_Suwier_Decru == null ? (Object)DBNull.Value : Huurderfiche_Suwier_Decru;
					cmd.Parameters.Add("@Huurderfiche_Delforge", OleDbType.Integer).Value = Huurderfiche_Delforge == null ? (Object)DBNull.Value : Huurderfiche_Delforge;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Huurderfiche_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Huurderfiche]
								SET		[Huurderfiche Huurder-ID] = @Huurderfiche_Huurder_ID,
										[HuurderficheEigenaarLink] = @HuurderficheEigenaarLink,
										[Huurderfiche Datum] = @Huurderfiche_Datum,
										[Huurderfiche Bedrag] = @Huurderfiche_Bedrag,
										[Huurderfiche Bedrag Euro] = @Huurderfiche_Bedrag_Euro,
										[Huurderfiche Betalen tegen] = @Huurderfiche_Betalen_tegen,
										[Huurderfiche Te betalen boete] = @Huurderfiche_Te_betalen_boete,
										[Huurderfiche Te betalen boete Euro] = @Huurderfiche_Te_betalen_boete_Euro,
										[Huurderfiche Boete kwijtgescholden] = @Huurderfiche_Boete_kwijtgescholden,
										[Huurderfiche Boete betaald] = @Huurderfiche_Boete_betaald,
										[Huurderfiche Commentaar] = @Huurderfiche_Commentaar,
										[Huurderfiche Lock] = @Huurderfiche_Lock,
										[Huurderfiche Aanmaning] = @Huurderfiche_Aanmaning,
										[Huurderfiche Datum 1ste aanmaning] = @Huurderfiche_Datum_1ste_aanmaning,
										[Huurderfiche Datum 2de aanmaning] = @Huurderfiche_Datum_2de_aanmaning,
										[Huurderfiche Datum 3de aanmaning] = @Huurderfiche_Datum_3de_aanmaning,
										[Huurderfiche Code aanmaning] = @Huurderfiche_Code_aanmaning,
										[Huurderfiche Aanmaning totaal betaald] = @Huurderfiche_Aanmaning_totaal_betaald,
										[Huurderfiche Aanmaning totaal betaald Euro] = @Huurderfiche_Aanmaning_totaal_betaald_Euro,
										[Huurderfiche Aanmaning datum laatste betaling] = @Huurderfiche_Aanmaning_datum_laatste_betaling,
										[Huurderfiche Suwier-Decru] = @Huurderfiche_Suwier_Decru,
										[Huurderfiche Delforge] = @Huurderfiche_Delforge
								WHERE	[Huurderfiche-ID] = @Huurderfiche_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Huurderfiche_Huurder_ID", OleDbType.Integer).Value = Huurderfiche_Huurder_ID == null ? (Object)DBNull.Value : Huurderfiche_Huurder_ID;
					cmd.Parameters.Add("@Huurderfiche_Eigendom_Huurder_ID", OleDbType.Integer).Value = Huurderfiche_Eigendom_Huurder_ID;
					cmd.Parameters.Add("@HuurderficheEigenaarLink", OleDbType.WChar).Value = HuurderficheEigenaarLink == null ? (Object)DBNull.Value : HuurderficheEigenaarLink;
					cmd.Parameters.Add("@Huurderfiche_Datum", OleDbType.Date).Value = Huurderfiche_Datum == null ? (Object)DBNull.Value : Huurderfiche_Datum;
					cmd.Parameters.Add("@Huurderfiche_Referentie", OleDbType.Integer).Value = Huurderfiche_Referentie;
					cmd.Parameters.Add("@Huurderfiche_Bedrag", OleDbType.Integer).Value = Huurderfiche_Bedrag == null ? (Object)DBNull.Value : Huurderfiche_Bedrag;
					cmd.Parameters.Add("@Huurderfiche_Bedrag_Euro", OleDbType.Double).Value = Huurderfiche_Bedrag_Euro == null ? (Object)DBNull.Value : Huurderfiche_Bedrag_Euro;
					cmd.Parameters.Add("@Huurderfiche_Betalen_tegen", OleDbType.Date).Value = Huurderfiche_Betalen_tegen == null ? (Object)DBNull.Value : Huurderfiche_Betalen_tegen;
					cmd.Parameters.Add("@Huurderfiche_Te_betalen_boete", OleDbType.Integer).Value = Huurderfiche_Te_betalen_boete == null ? (Object)DBNull.Value : Huurderfiche_Te_betalen_boete;
					cmd.Parameters.Add("@Huurderfiche_Te_betalen_boete_Euro", OleDbType.Double).Value = Huurderfiche_Te_betalen_boete_Euro == null ? (Object)DBNull.Value : Huurderfiche_Te_betalen_boete_Euro;
					cmd.Parameters.Add("@Huurderfiche_Boete_kwijtgescholden", OleDbType.Boolean).Value = Huurderfiche_Boete_kwijtgescholden;
					cmd.Parameters.Add("@Huurderfiche_Boete_betaald", OleDbType.Date).Value = Huurderfiche_Boete_betaald == null ? (Object)DBNull.Value : Huurderfiche_Boete_betaald;
					cmd.Parameters.Add("@Huurderfiche_Commentaar", OleDbType.WChar).Value = Huurderfiche_Commentaar == null ? (Object)DBNull.Value : Huurderfiche_Commentaar;
					cmd.Parameters.Add("@Huurderfiche_Lock", OleDbType.Boolean).Value = Huurderfiche_Lock;
					cmd.Parameters.Add("@Huurderfiche_Aanmaning", OleDbType.Boolean).Value = Huurderfiche_Aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Datum_1ste_aanmaning", OleDbType.Date).Value = Huurderfiche_Datum_1ste_aanmaning == null ? (Object)DBNull.Value : Huurderfiche_Datum_1ste_aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Datum_2de_aanmaning", OleDbType.Date).Value = Huurderfiche_Datum_2de_aanmaning == null ? (Object)DBNull.Value : Huurderfiche_Datum_2de_aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Datum_3de_aanmaning", OleDbType.Date).Value = Huurderfiche_Datum_3de_aanmaning == null ? (Object)DBNull.Value : Huurderfiche_Datum_3de_aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Code_aanmaning", OleDbType.UnsignedTinyInt).Value = Huurderfiche_Code_aanmaning == null ? (Object)DBNull.Value : Huurderfiche_Code_aanmaning;
					cmd.Parameters.Add("@Huurderfiche_Aanmaning_totaal_betaald", OleDbType.Integer).Value = Huurderfiche_Aanmaning_totaal_betaald == null ? (Object)DBNull.Value : Huurderfiche_Aanmaning_totaal_betaald;
					cmd.Parameters.Add("@Huurderfiche_Aanmaning_totaal_betaald_Euro", OleDbType.Double).Value = Huurderfiche_Aanmaning_totaal_betaald_Euro == null ? (Object)DBNull.Value : Huurderfiche_Aanmaning_totaal_betaald_Euro;
					cmd.Parameters.Add("@Huurderfiche_Aanmaning_datum_laatste_betaling", OleDbType.Date).Value = Huurderfiche_Aanmaning_datum_laatste_betaling == null ? (Object)DBNull.Value : Huurderfiche_Aanmaning_datum_laatste_betaling;
					cmd.Parameters.Add("@Huurderfiche_Suwier_Decru", OleDbType.Integer).Value = Huurderfiche_Suwier_Decru == null ? (Object)DBNull.Value : Huurderfiche_Suwier_Decru;
					cmd.Parameters.Add("@Huurderfiche_Delforge", OleDbType.Integer).Value = Huurderfiche_Delforge == null ? (Object)DBNull.Value : Huurderfiche_Delforge;
					cmd.Parameters.Add("@Huurderfiche_ID", OleDbType.Integer).Value = Huurderfiche_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int huurderfiche_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Huurderfiche]
								WHERE	[Huurderfiche-ID] = @Huurderfiche_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Huurderfiche_ID", OleDbType.Integer).Value = huurderfiche_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Huurderfiche Get(int huurderfiche_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Huurderfiche-ID],
										[Huurderfiche Huurder-ID],
										[Huurderfiche Eigendom-Huurder-ID],
										[HuurderficheEigenaarLink],
										[Huurderfiche Datum],
										[Huurderfiche Referentie],
										[Huurderfiche Bedrag],
										[Huurderfiche Bedrag Euro],
										[Huurderfiche Betalen tegen],
										[Huurderfiche Te betalen boete],
										[Huurderfiche Te betalen boete Euro],
										[Huurderfiche Boete kwijtgescholden],
										[Huurderfiche Boete betaald],
										[Huurderfiche Commentaar],
										[Huurderfiche Lock],
										[Huurderfiche Aanmaning],
										[Huurderfiche Datum 1ste aanmaning],
										[Huurderfiche Datum 2de aanmaning],
										[Huurderfiche Datum 3de aanmaning],
										[Huurderfiche Code aanmaning],
										[Huurderfiche Aanmaning totaal betaald],
										[Huurderfiche Aanmaning totaal betaald Euro],
										[Huurderfiche Aanmaning datum laatste betaling],
										[Huurderfiche Suwier-Decru],
										[Huurderfiche Delforge]
								FROM	[Huurderfiche]
								WHERE	[Huurderfiche-ID] = @Huurderfiche_ID;";

				Huurderfiche huurderfiche = new Huurderfiche();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Huurderfiche_ID", OleDbType.Integer).Value = huurderfiche_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							huurderfiche.Huurderfiche_ID = Convert.ToInt32(reader["Huurderfiche-ID"]);
							huurderfiche.Huurderfiche_Huurder_ID = reader["Huurderfiche Huurder-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurderfiche Huurder-ID"]);
							huurderfiche.Huurderfiche_Eigendom_Huurder_ID = Convert.ToInt32(reader["Huurderfiche Eigendom-Huurder-ID"]);
							huurderfiche.HuurderficheEigenaarLink = reader["HuurderficheEigenaarLink"] == DBNull.Value ? null : reader["HuurderficheEigenaarLink"].ToString();
							huurderfiche.Huurderfiche_Datum = reader["Huurderfiche Datum"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Huurderfiche Datum"]);
							huurderfiche.Huurderfiche_Referentie = Convert.ToInt32(reader["Huurderfiche Referentie"]);
							huurderfiche.Huurderfiche_Bedrag = reader["Huurderfiche Bedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurderfiche Bedrag"]);
							huurderfiche.Huurderfiche_Bedrag_Euro = reader["Huurderfiche Bedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Huurderfiche Bedrag Euro"]);
							huurderfiche.Huurderfiche_Betalen_tegen = reader["Huurderfiche Betalen tegen"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Huurderfiche Betalen tegen"]);
							huurderfiche.Huurderfiche_Te_betalen_boete = reader["Huurderfiche Te betalen boete"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurderfiche Te betalen boete"]);
							huurderfiche.Huurderfiche_Te_betalen_boete_Euro = reader["Huurderfiche Te betalen boete Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Huurderfiche Te betalen boete Euro"]);
							huurderfiche.Huurderfiche_Boete_kwijtgescholden = Convert.ToBoolean(reader["Huurderfiche Boete kwijtgescholden"]);
							huurderfiche.Huurderfiche_Boete_betaald = reader["Huurderfiche Boete betaald"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Huurderfiche Boete betaald"]);
							huurderfiche.Huurderfiche_Commentaar = reader["Huurderfiche Commentaar"] == DBNull.Value ? null : reader["Huurderfiche Commentaar"].ToString();
							huurderfiche.Huurderfiche_Lock = Convert.ToBoolean(reader["Huurderfiche Lock"]);
							huurderfiche.Huurderfiche_Aanmaning = Convert.ToBoolean(reader["Huurderfiche Aanmaning"]);
							huurderfiche.Huurderfiche_Datum_1ste_aanmaning = reader["Huurderfiche Datum 1ste aanmaning"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Huurderfiche Datum 1ste aanmaning"]);
							huurderfiche.Huurderfiche_Datum_2de_aanmaning = reader["Huurderfiche Datum 2de aanmaning"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Huurderfiche Datum 2de aanmaning"]);
							huurderfiche.Huurderfiche_Datum_3de_aanmaning = reader["Huurderfiche Datum 3de aanmaning"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Huurderfiche Datum 3de aanmaning"]);
							huurderfiche.Huurderfiche_Code_aanmaning = reader["Huurderfiche Code aanmaning"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Huurderfiche Code aanmaning"]);
							huurderfiche.Huurderfiche_Aanmaning_totaal_betaald = reader["Huurderfiche Aanmaning totaal betaald"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurderfiche Aanmaning totaal betaald"]);
							huurderfiche.Huurderfiche_Aanmaning_totaal_betaald_Euro = reader["Huurderfiche Aanmaning totaal betaald Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Huurderfiche Aanmaning totaal betaald Euro"]);
							huurderfiche.Huurderfiche_Aanmaning_datum_laatste_betaling = reader["Huurderfiche Aanmaning datum laatste betaling"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Huurderfiche Aanmaning datum laatste betaling"]);
							huurderfiche.Huurderfiche_Suwier_Decru = reader["Huurderfiche Suwier-Decru"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurderfiche Suwier-Decru"]);
							huurderfiche.Huurderfiche_Delforge = reader["Huurderfiche Delforge"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurderfiche Delforge"]);
						}
					}
				}

				return huurderfiche;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Huurderfiche-ID],
										[Huurderfiche Huurder-ID],
										[Huurderfiche Eigendom-Huurder-ID],
										[HuurderficheEigenaarLink],
										[Huurderfiche Datum],
										[Huurderfiche Referentie],
										[Huurderfiche Bedrag],
										[Huurderfiche Bedrag Euro],
										[Huurderfiche Betalen tegen],
										[Huurderfiche Te betalen boete],
										[Huurderfiche Te betalen boete Euro],
										[Huurderfiche Boete kwijtgescholden],
										[Huurderfiche Boete betaald],
										[Huurderfiche Commentaar],
										[Huurderfiche Lock],
										[Huurderfiche Aanmaning],
										[Huurderfiche Datum 1ste aanmaning],
										[Huurderfiche Datum 2de aanmaning],
										[Huurderfiche Datum 3de aanmaning],
										[Huurderfiche Code aanmaning],
										[Huurderfiche Aanmaning totaal betaald],
										[Huurderfiche Aanmaning totaal betaald Euro],
										[Huurderfiche Aanmaning datum laatste betaling],
										[Huurderfiche Suwier-Decru],
										[Huurderfiche Delforge]
								FROM	[Huurderfiche];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}