using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Makelaar_Commissie
	{
		#region Properties
		public int Makelaar_Commissie_ID { get; set; }
		public int? Makelaar_Commissie_Makelaar_ID { get; set; }
		public int Makelaar_Commissie_Commissie_ID { get; set; }
		public byte? Makelaar_Commissie_Binnen_ { get; set; }
		public byte? Makelaar_Commissie_Buiten_ { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Makelaar-Commissie]
								(
									[Makelaar-Commissie Makelaar-ID],
									[Makelaar-Commissie Binnen%],
									[Makelaar-Commissie Buiten%]
								)
								VALUES
								(
									@Makelaar_Commissie_Makelaar_ID,
									@Makelaar_Commissie_Binnen_,
									@Makelaar_Commissie_Buiten_
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Makelaar_Commissie_Makelaar_ID", OleDbType.Integer).Value = Makelaar_Commissie_Makelaar_ID == null ? (Object)DBNull.Value : Makelaar_Commissie_Makelaar_ID;
					cmd.Parameters.Add("@Makelaar_Commissie_Binnen_", OleDbType.UnsignedTinyInt).Value = Makelaar_Commissie_Binnen_ == null ? (Object)DBNull.Value : Makelaar_Commissie_Binnen_;
					cmd.Parameters.Add("@Makelaar_Commissie_Buiten_", OleDbType.UnsignedTinyInt).Value = Makelaar_Commissie_Buiten_ == null ? (Object)DBNull.Value : Makelaar_Commissie_Buiten_;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Makelaar_Commissie_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Makelaar-Commissie]
								SET		[Makelaar-Commissie Makelaar-ID] = @Makelaar_Commissie_Makelaar_ID,
										[Makelaar-Commissie Binnen%] = @Makelaar_Commissie_Binnen_,
										[Makelaar-Commissie Buiten%] = @Makelaar_Commissie_Buiten_
								WHERE	[Makelaar-Commissie-ID] = @Makelaar_Commissie_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Makelaar_Commissie_Makelaar_ID", OleDbType.Integer).Value = Makelaar_Commissie_Makelaar_ID == null ? (Object)DBNull.Value : Makelaar_Commissie_Makelaar_ID;
					cmd.Parameters.Add("@Makelaar_Commissie_Commissie_ID", OleDbType.Integer).Value = Makelaar_Commissie_Commissie_ID;
					cmd.Parameters.Add("@Makelaar_Commissie_Binnen_", OleDbType.UnsignedTinyInt).Value = Makelaar_Commissie_Binnen_ == null ? (Object)DBNull.Value : Makelaar_Commissie_Binnen_;
					cmd.Parameters.Add("@Makelaar_Commissie_Buiten_", OleDbType.UnsignedTinyInt).Value = Makelaar_Commissie_Buiten_ == null ? (Object)DBNull.Value : Makelaar_Commissie_Buiten_;
					cmd.Parameters.Add("@Makelaar_Commissie_ID", OleDbType.Integer).Value = Makelaar_Commissie_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int makelaar_Commissie_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Makelaar-Commissie]
								WHERE	[Makelaar-Commissie-ID] = @Makelaar_Commissie_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Makelaar_Commissie_ID", OleDbType.Integer).Value = makelaar_Commissie_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Makelaar_Commissie Get(int makelaar_Commissie_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Makelaar-Commissie-ID],
										[Makelaar-Commissie Makelaar-ID],
										[Makelaar-Commissie Commissie-ID],
										[Makelaar-Commissie Binnen%],
										[Makelaar-Commissie Buiten%]
								FROM	[Makelaar-Commissie]
								WHERE	[Makelaar-Commissie-ID] = @Makelaar_Commissie_ID;";

				Makelaar_Commissie makelaar_Commissie = new Makelaar_Commissie();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Makelaar_Commissie_ID", OleDbType.Integer).Value = makelaar_Commissie_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							makelaar_Commissie.Makelaar_Commissie_ID = Convert.ToInt32(reader["Makelaar-Commissie-ID"]);
							makelaar_Commissie.Makelaar_Commissie_Makelaar_ID = reader["Makelaar-Commissie Makelaar-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Makelaar-Commissie Makelaar-ID"]);
							makelaar_Commissie.Makelaar_Commissie_Commissie_ID = Convert.ToInt32(reader["Makelaar-Commissie Commissie-ID"]);
							makelaar_Commissie.Makelaar_Commissie_Binnen_ = reader["Makelaar-Commissie Binnen%"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Makelaar-Commissie Binnen%"]);
							makelaar_Commissie.Makelaar_Commissie_Buiten_ = reader["Makelaar-Commissie Buiten%"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Makelaar-Commissie Buiten%"]);
						}
					}
				}

				return makelaar_Commissie;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Makelaar-Commissie-ID],
										[Makelaar-Commissie Makelaar-ID],
										[Makelaar-Commissie Commissie-ID],
										[Makelaar-Commissie Binnen%],
										[Makelaar-Commissie Buiten%]
								FROM	[Makelaar-Commissie];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}