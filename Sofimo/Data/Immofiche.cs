using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Immofiche
	{
		#region Properties
		public int Immofiche_ID { get; set; }
		public int? Immofiche_Gemeente_ID_plaats { get; set; }
		public int? Immofiche_Gemeente_ID_naam1 { get; set; }
		public int? Immofiche_Gemeente_ID_naam2 { get; set; }
		public int? Immofiche_Makelaar_ID { get; set; }
		public int? Immofiche_Notaris_ID { get; set; }
		public string Immofiche_Telefoon_naam1 { get; set; }
		public string Immofiche_Naam1 { get; set; }
		public string Immofiche_Straat_naam1 { get; set; }
		public string Immofiche_Telefoon_naam2 { get; set; }
		public string Immofiche_Naam2 { get; set; }
		public string Immofiche_Straat_naam2 { get; set; }
		public string Immofiche_Straat_plaats { get; set; }
		public string Immofiche_Bus_plaats { get; set; }
		public string Immofiche_Kadaster { get; set; }
		public byte? Immofiche_Soort { get; set; }
		public string Immofiche_Wat { get; set; }
		public string Immofiche_Aangebracht { get; set; }
		public int? Immofiche_Prijs { get; set; }
		public byte? Immofiche_Laatste_brief { get; set; }
		public bool Immofiche_Uitgeprint { get; set; }
		public DateTime? Immofiche_Datum_1ste_publicatie { get; set; }
		public bool Immofiche_Binnen { get; set; }
		public string Immofiche_Key { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Immofiche]
								(
									[Immofiche Gemeente-ID plaats],
									[Immofiche Gemeente-ID naam1],
									[Immofiche Gemeente-ID naam2],
									[Immofiche Makelaar-ID],
									[Immofiche Notaris-ID],
									[Immofiche Telefoon naam1],
									[Immofiche Naam1],
									[Immofiche Straat naam1],
									[Immofiche Telefoon naam2],
									[Immofiche Naam2],
									[Immofiche Straat naam2],
									[Immofiche Straat plaats],
									[Immofiche Bus plaats],
									[Immofiche Kadaster],
									[Immofiche Soort],
									[Immofiche Wat],
									[Immofiche Aangebracht],
									[Immofiche Prijs],
									[Immofiche Laatste brief],
									[Immofiche Uitgeprint],
									[Immofiche Datum 1ste publicatie],
									[Immofiche Binnen],
									[Immofiche Key]
								)
								VALUES
								(
									@Immofiche_Gemeente_ID_plaats,
									@Immofiche_Gemeente_ID_naam1,
									@Immofiche_Gemeente_ID_naam2,
									@Immofiche_Makelaar_ID,
									@Immofiche_Notaris_ID,
									@Immofiche_Telefoon_naam1,
									@Immofiche_Naam1,
									@Immofiche_Straat_naam1,
									@Immofiche_Telefoon_naam2,
									@Immofiche_Naam2,
									@Immofiche_Straat_naam2,
									@Immofiche_Straat_plaats,
									@Immofiche_Bus_plaats,
									@Immofiche_Kadaster,
									@Immofiche_Soort,
									@Immofiche_Wat,
									@Immofiche_Aangebracht,
									@Immofiche_Prijs,
									@Immofiche_Laatste_brief,
									@Immofiche_Uitgeprint,
									@Immofiche_Datum_1ste_publicatie,
									@Immofiche_Binnen,
									@Immofiche_Key
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Immofiche_Gemeente_ID_plaats", OleDbType.Integer).Value = Immofiche_Gemeente_ID_plaats == null ? (Object)DBNull.Value : Immofiche_Gemeente_ID_plaats;
					cmd.Parameters.Add("@Immofiche_Gemeente_ID_naam1", OleDbType.Integer).Value = Immofiche_Gemeente_ID_naam1 == null ? (Object)DBNull.Value : Immofiche_Gemeente_ID_naam1;
					cmd.Parameters.Add("@Immofiche_Gemeente_ID_naam2", OleDbType.Integer).Value = Immofiche_Gemeente_ID_naam2 == null ? (Object)DBNull.Value : Immofiche_Gemeente_ID_naam2;
					cmd.Parameters.Add("@Immofiche_Makelaar_ID", OleDbType.Integer).Value = Immofiche_Makelaar_ID == null ? (Object)DBNull.Value : Immofiche_Makelaar_ID;
					cmd.Parameters.Add("@Immofiche_Notaris_ID", OleDbType.Integer).Value = Immofiche_Notaris_ID == null ? (Object)DBNull.Value : Immofiche_Notaris_ID;
					cmd.Parameters.Add("@Immofiche_Telefoon_naam1", OleDbType.WChar).Value = Immofiche_Telefoon_naam1 == null ? (Object)DBNull.Value : Immofiche_Telefoon_naam1;
					cmd.Parameters.Add("@Immofiche_Naam1", OleDbType.WChar).Value = Immofiche_Naam1 == null ? (Object)DBNull.Value : Immofiche_Naam1;
					cmd.Parameters.Add("@Immofiche_Straat_naam1", OleDbType.WChar).Value = Immofiche_Straat_naam1 == null ? (Object)DBNull.Value : Immofiche_Straat_naam1;
					cmd.Parameters.Add("@Immofiche_Telefoon_naam2", OleDbType.WChar).Value = Immofiche_Telefoon_naam2 == null ? (Object)DBNull.Value : Immofiche_Telefoon_naam2;
					cmd.Parameters.Add("@Immofiche_Naam2", OleDbType.WChar).Value = Immofiche_Naam2 == null ? (Object)DBNull.Value : Immofiche_Naam2;
					cmd.Parameters.Add("@Immofiche_Straat_naam2", OleDbType.WChar).Value = Immofiche_Straat_naam2 == null ? (Object)DBNull.Value : Immofiche_Straat_naam2;
					cmd.Parameters.Add("@Immofiche_Straat_plaats", OleDbType.WChar).Value = Immofiche_Straat_plaats == null ? (Object)DBNull.Value : Immofiche_Straat_plaats;
					cmd.Parameters.Add("@Immofiche_Bus_plaats", OleDbType.WChar).Value = Immofiche_Bus_plaats == null ? (Object)DBNull.Value : Immofiche_Bus_plaats;
					cmd.Parameters.Add("@Immofiche_Kadaster", OleDbType.WChar).Value = Immofiche_Kadaster == null ? (Object)DBNull.Value : Immofiche_Kadaster;
					cmd.Parameters.Add("@Immofiche_Soort", OleDbType.UnsignedTinyInt).Value = Immofiche_Soort == null ? (Object)DBNull.Value : Immofiche_Soort;
					cmd.Parameters.Add("@Immofiche_Wat", OleDbType.WChar).Value = Immofiche_Wat == null ? (Object)DBNull.Value : Immofiche_Wat;
					cmd.Parameters.Add("@Immofiche_Aangebracht", OleDbType.WChar).Value = Immofiche_Aangebracht == null ? (Object)DBNull.Value : Immofiche_Aangebracht;
					cmd.Parameters.Add("@Immofiche_Prijs", OleDbType.Integer).Value = Immofiche_Prijs == null ? (Object)DBNull.Value : Immofiche_Prijs;
					cmd.Parameters.Add("@Immofiche_Laatste_brief", OleDbType.UnsignedTinyInt).Value = Immofiche_Laatste_brief == null ? (Object)DBNull.Value : Immofiche_Laatste_brief;
					cmd.Parameters.Add("@Immofiche_Uitgeprint", OleDbType.Boolean).Value = Immofiche_Uitgeprint;
					cmd.Parameters.Add("@Immofiche_Datum_1ste_publicatie", OleDbType.Date).Value = Immofiche_Datum_1ste_publicatie == null ? (Object)DBNull.Value : Immofiche_Datum_1ste_publicatie;
					cmd.Parameters.Add("@Immofiche_Binnen", OleDbType.Boolean).Value = Immofiche_Binnen;
					cmd.Parameters.Add("@Immofiche_Key", OleDbType.WChar).Value = Immofiche_Key == null ? (Object)DBNull.Value : Immofiche_Key;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Immofiche_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Immofiche]
								SET		[Immofiche Gemeente-ID plaats] = @Immofiche_Gemeente_ID_plaats,
										[Immofiche Gemeente-ID naam1] = @Immofiche_Gemeente_ID_naam1,
										[Immofiche Gemeente-ID naam2] = @Immofiche_Gemeente_ID_naam2,
										[Immofiche Makelaar-ID] = @Immofiche_Makelaar_ID,
										[Immofiche Notaris-ID] = @Immofiche_Notaris_ID,
										[Immofiche Telefoon naam1] = @Immofiche_Telefoon_naam1,
										[Immofiche Naam1] = @Immofiche_Naam1,
										[Immofiche Straat naam1] = @Immofiche_Straat_naam1,
										[Immofiche Telefoon naam2] = @Immofiche_Telefoon_naam2,
										[Immofiche Naam2] = @Immofiche_Naam2,
										[Immofiche Straat naam2] = @Immofiche_Straat_naam2,
										[Immofiche Straat plaats] = @Immofiche_Straat_plaats,
										[Immofiche Bus plaats] = @Immofiche_Bus_plaats,
										[Immofiche Kadaster] = @Immofiche_Kadaster,
										[Immofiche Soort] = @Immofiche_Soort,
										[Immofiche Wat] = @Immofiche_Wat,
										[Immofiche Aangebracht] = @Immofiche_Aangebracht,
										[Immofiche Prijs] = @Immofiche_Prijs,
										[Immofiche Laatste brief] = @Immofiche_Laatste_brief,
										[Immofiche Uitgeprint] = @Immofiche_Uitgeprint,
										[Immofiche Datum 1ste publicatie] = @Immofiche_Datum_1ste_publicatie,
										[Immofiche Binnen] = @Immofiche_Binnen,
										[Immofiche Key] = @Immofiche_Key
								WHERE	[Immofiche-ID] = @Immofiche_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Immofiche_Gemeente_ID_plaats", OleDbType.Integer).Value = Immofiche_Gemeente_ID_plaats == null ? (Object)DBNull.Value : Immofiche_Gemeente_ID_plaats;
					cmd.Parameters.Add("@Immofiche_Gemeente_ID_naam1", OleDbType.Integer).Value = Immofiche_Gemeente_ID_naam1 == null ? (Object)DBNull.Value : Immofiche_Gemeente_ID_naam1;
					cmd.Parameters.Add("@Immofiche_Gemeente_ID_naam2", OleDbType.Integer).Value = Immofiche_Gemeente_ID_naam2 == null ? (Object)DBNull.Value : Immofiche_Gemeente_ID_naam2;
					cmd.Parameters.Add("@Immofiche_Makelaar_ID", OleDbType.Integer).Value = Immofiche_Makelaar_ID == null ? (Object)DBNull.Value : Immofiche_Makelaar_ID;
					cmd.Parameters.Add("@Immofiche_Notaris_ID", OleDbType.Integer).Value = Immofiche_Notaris_ID == null ? (Object)DBNull.Value : Immofiche_Notaris_ID;
					cmd.Parameters.Add("@Immofiche_Telefoon_naam1", OleDbType.WChar).Value = Immofiche_Telefoon_naam1 == null ? (Object)DBNull.Value : Immofiche_Telefoon_naam1;
					cmd.Parameters.Add("@Immofiche_Naam1", OleDbType.WChar).Value = Immofiche_Naam1 == null ? (Object)DBNull.Value : Immofiche_Naam1;
					cmd.Parameters.Add("@Immofiche_Straat_naam1", OleDbType.WChar).Value = Immofiche_Straat_naam1 == null ? (Object)DBNull.Value : Immofiche_Straat_naam1;
					cmd.Parameters.Add("@Immofiche_Telefoon_naam2", OleDbType.WChar).Value = Immofiche_Telefoon_naam2 == null ? (Object)DBNull.Value : Immofiche_Telefoon_naam2;
					cmd.Parameters.Add("@Immofiche_Naam2", OleDbType.WChar).Value = Immofiche_Naam2 == null ? (Object)DBNull.Value : Immofiche_Naam2;
					cmd.Parameters.Add("@Immofiche_Straat_naam2", OleDbType.WChar).Value = Immofiche_Straat_naam2 == null ? (Object)DBNull.Value : Immofiche_Straat_naam2;
					cmd.Parameters.Add("@Immofiche_Straat_plaats", OleDbType.WChar).Value = Immofiche_Straat_plaats == null ? (Object)DBNull.Value : Immofiche_Straat_plaats;
					cmd.Parameters.Add("@Immofiche_Bus_plaats", OleDbType.WChar).Value = Immofiche_Bus_plaats == null ? (Object)DBNull.Value : Immofiche_Bus_plaats;
					cmd.Parameters.Add("@Immofiche_Kadaster", OleDbType.WChar).Value = Immofiche_Kadaster == null ? (Object)DBNull.Value : Immofiche_Kadaster;
					cmd.Parameters.Add("@Immofiche_Soort", OleDbType.UnsignedTinyInt).Value = Immofiche_Soort == null ? (Object)DBNull.Value : Immofiche_Soort;
					cmd.Parameters.Add("@Immofiche_Wat", OleDbType.WChar).Value = Immofiche_Wat == null ? (Object)DBNull.Value : Immofiche_Wat;
					cmd.Parameters.Add("@Immofiche_Aangebracht", OleDbType.WChar).Value = Immofiche_Aangebracht == null ? (Object)DBNull.Value : Immofiche_Aangebracht;
					cmd.Parameters.Add("@Immofiche_Prijs", OleDbType.Integer).Value = Immofiche_Prijs == null ? (Object)DBNull.Value : Immofiche_Prijs;
					cmd.Parameters.Add("@Immofiche_Laatste_brief", OleDbType.UnsignedTinyInt).Value = Immofiche_Laatste_brief == null ? (Object)DBNull.Value : Immofiche_Laatste_brief;
					cmd.Parameters.Add("@Immofiche_Uitgeprint", OleDbType.Boolean).Value = Immofiche_Uitgeprint;
					cmd.Parameters.Add("@Immofiche_Datum_1ste_publicatie", OleDbType.Date).Value = Immofiche_Datum_1ste_publicatie == null ? (Object)DBNull.Value : Immofiche_Datum_1ste_publicatie;
					cmd.Parameters.Add("@Immofiche_Binnen", OleDbType.Boolean).Value = Immofiche_Binnen;
					cmd.Parameters.Add("@Immofiche_Key", OleDbType.WChar).Value = Immofiche_Key == null ? (Object)DBNull.Value : Immofiche_Key;
					cmd.Parameters.Add("@Immofiche_ID", OleDbType.Integer).Value = Immofiche_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int immofiche_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Immofiche]
								WHERE	[Immofiche-ID] = @Immofiche_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Immofiche_ID", OleDbType.Integer).Value = immofiche_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Immofiche Get(int immofiche_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Immofiche-ID],
										[Immofiche Gemeente-ID plaats],
										[Immofiche Gemeente-ID naam1],
										[Immofiche Gemeente-ID naam2],
										[Immofiche Makelaar-ID],
										[Immofiche Notaris-ID],
										[Immofiche Telefoon naam1],
										[Immofiche Naam1],
										[Immofiche Straat naam1],
										[Immofiche Telefoon naam2],
										[Immofiche Naam2],
										[Immofiche Straat naam2],
										[Immofiche Straat plaats],
										[Immofiche Bus plaats],
										[Immofiche Kadaster],
										[Immofiche Soort],
										[Immofiche Wat],
										[Immofiche Aangebracht],
										[Immofiche Prijs],
										[Immofiche Laatste brief],
										[Immofiche Uitgeprint],
										[Immofiche Datum 1ste publicatie],
										[Immofiche Binnen],
										[Immofiche Key]
								FROM	[Immofiche]
								WHERE	[Immofiche-ID] = @Immofiche_ID;";

				Immofiche immofiche = new Immofiche();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Immofiche_ID", OleDbType.Integer).Value = immofiche_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							immofiche.Immofiche_ID = Convert.ToInt32(reader["Immofiche-ID"]);
							immofiche.Immofiche_Gemeente_ID_plaats = reader["Immofiche Gemeente-ID plaats"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Immofiche Gemeente-ID plaats"]);
							immofiche.Immofiche_Gemeente_ID_naam1 = reader["Immofiche Gemeente-ID naam1"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Immofiche Gemeente-ID naam1"]);
							immofiche.Immofiche_Gemeente_ID_naam2 = reader["Immofiche Gemeente-ID naam2"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Immofiche Gemeente-ID naam2"]);
							immofiche.Immofiche_Makelaar_ID = reader["Immofiche Makelaar-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Immofiche Makelaar-ID"]);
							immofiche.Immofiche_Notaris_ID = reader["Immofiche Notaris-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Immofiche Notaris-ID"]);
							immofiche.Immofiche_Telefoon_naam1 = reader["Immofiche Telefoon naam1"] == DBNull.Value ? null : reader["Immofiche Telefoon naam1"].ToString();
							immofiche.Immofiche_Naam1 = reader["Immofiche Naam1"] == DBNull.Value ? null : reader["Immofiche Naam1"].ToString();
							immofiche.Immofiche_Straat_naam1 = reader["Immofiche Straat naam1"] == DBNull.Value ? null : reader["Immofiche Straat naam1"].ToString();
							immofiche.Immofiche_Telefoon_naam2 = reader["Immofiche Telefoon naam2"] == DBNull.Value ? null : reader["Immofiche Telefoon naam2"].ToString();
							immofiche.Immofiche_Naam2 = reader["Immofiche Naam2"] == DBNull.Value ? null : reader["Immofiche Naam2"].ToString();
							immofiche.Immofiche_Straat_naam2 = reader["Immofiche Straat naam2"] == DBNull.Value ? null : reader["Immofiche Straat naam2"].ToString();
							immofiche.Immofiche_Straat_plaats = reader["Immofiche Straat plaats"] == DBNull.Value ? null : reader["Immofiche Straat plaats"].ToString();
							immofiche.Immofiche_Bus_plaats = reader["Immofiche Bus plaats"] == DBNull.Value ? null : reader["Immofiche Bus plaats"].ToString();
							immofiche.Immofiche_Kadaster = reader["Immofiche Kadaster"] == DBNull.Value ? null : reader["Immofiche Kadaster"].ToString();
							immofiche.Immofiche_Soort = reader["Immofiche Soort"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Immofiche Soort"]);
							immofiche.Immofiche_Wat = reader["Immofiche Wat"] == DBNull.Value ? null : reader["Immofiche Wat"].ToString();
							immofiche.Immofiche_Aangebracht = reader["Immofiche Aangebracht"] == DBNull.Value ? null : reader["Immofiche Aangebracht"].ToString();
							immofiche.Immofiche_Prijs = reader["Immofiche Prijs"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Immofiche Prijs"]);
							immofiche.Immofiche_Laatste_brief = reader["Immofiche Laatste brief"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Immofiche Laatste brief"]);
							immofiche.Immofiche_Uitgeprint = Convert.ToBoolean(reader["Immofiche Uitgeprint"]);
							immofiche.Immofiche_Datum_1ste_publicatie = reader["Immofiche Datum 1ste publicatie"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Immofiche Datum 1ste publicatie"]);
							immofiche.Immofiche_Binnen = Convert.ToBoolean(reader["Immofiche Binnen"]);
							immofiche.Immofiche_Key = reader["Immofiche Key"] == DBNull.Value ? null : reader["Immofiche Key"].ToString();
						}
					}
				}

				return immofiche;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Immofiche-ID],
										[Immofiche Gemeente-ID plaats],
										[Immofiche Gemeente-ID naam1],
										[Immofiche Gemeente-ID naam2],
										[Immofiche Makelaar-ID],
										[Immofiche Notaris-ID],
										[Immofiche Telefoon naam1],
										[Immofiche Naam1],
										[Immofiche Straat naam1],
										[Immofiche Telefoon naam2],
										[Immofiche Naam2],
										[Immofiche Straat naam2],
										[Immofiche Straat plaats],
										[Immofiche Bus plaats],
										[Immofiche Kadaster],
										[Immofiche Soort],
										[Immofiche Wat],
										[Immofiche Aangebracht],
										[Immofiche Prijs],
										[Immofiche Laatste brief],
										[Immofiche Uitgeprint],
										[Immofiche Datum 1ste publicatie],
										[Immofiche Binnen],
										[Immofiche Key]
								FROM	[Immofiche];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}