using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Makelaar
	{
		#region Properties
		public int Makelaar_ID { get; set; }
		public string Makelaar_Code { get; set; }
		public string Makelaar1 { get; set; }
		public int? Makelaar_Kantoor_ID { get; set; }
		public bool Makelaar_Makelaar { get; set; }
		public bool Makelaar_Fix { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Makelaar]
								(
									[Makelaar Code],
									[Makelaar],
									[Makelaar Kantoor-ID],
									[Makelaar Makelaar],
									[Makelaar Fix]
								)
								VALUES
								(
									@Makelaar_Code,
									@Makelaar,
									@Makelaar_Kantoor_ID,
									@Makelaar_Makelaar,
									@Makelaar_Fix
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Makelaar_Code", OleDbType.WChar).Value = Makelaar_Code == null ? (Object)DBNull.Value : Makelaar_Code;
					cmd.Parameters.Add("@Makelaar", OleDbType.WChar).Value = Makelaar1;
					cmd.Parameters.Add("@Makelaar_Kantoor_ID", OleDbType.Integer).Value = Makelaar_Kantoor_ID == null ? (Object)DBNull.Value : Makelaar_Kantoor_ID;
					cmd.Parameters.Add("@Makelaar_Makelaar", OleDbType.Boolean).Value = Makelaar_Makelaar;
					cmd.Parameters.Add("@Makelaar_Fix", OleDbType.Boolean).Value = Makelaar_Fix;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Makelaar_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Makelaar]
								SET		[Makelaar Code] = @Makelaar_Code,
										[Makelaar] = @Makelaar,
										[Makelaar Kantoor-ID] = @Makelaar_Kantoor_ID,
										[Makelaar Makelaar] = @Makelaar_Makelaar,
										[Makelaar Fix] = @Makelaar_Fix
								WHERE	[Makelaar-ID] = @Makelaar_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Makelaar_Code", OleDbType.WChar).Value = Makelaar_Code == null ? (Object)DBNull.Value : Makelaar_Code;
					cmd.Parameters.Add("@Makelaar", OleDbType.WChar).Value = Makelaar1;
					cmd.Parameters.Add("@Makelaar_Kantoor_ID", OleDbType.Integer).Value = Makelaar_Kantoor_ID == null ? (Object)DBNull.Value : Makelaar_Kantoor_ID;
					cmd.Parameters.Add("@Makelaar_Makelaar", OleDbType.Boolean).Value = Makelaar_Makelaar;
					cmd.Parameters.Add("@Makelaar_Fix", OleDbType.Boolean).Value = Makelaar_Fix;
					cmd.Parameters.Add("@Makelaar_ID", OleDbType.Integer).Value = Makelaar_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int makelaar_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Makelaar]
								WHERE	[Makelaar-ID] = @Makelaar_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Makelaar_ID", OleDbType.Integer).Value = makelaar_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Makelaar Get(int makelaar_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Makelaar-ID],
										[Makelaar Code],
										[Makelaar],
										[Makelaar Kantoor-ID],
										[Makelaar Makelaar],
										[Makelaar Fix]
								FROM	[Makelaar]
								WHERE	[Makelaar-ID] = @Makelaar_ID;";

				Makelaar makelaar = new Makelaar();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Makelaar_ID", OleDbType.Integer).Value = makelaar_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							makelaar.Makelaar_ID = Convert.ToInt32(reader["Makelaar-ID"]);
							makelaar.Makelaar_Code = reader["Makelaar Code"] == DBNull.Value ? null : reader["Makelaar Code"].ToString();
							makelaar.Makelaar1 = reader["Makelaar"].ToString();
							makelaar.Makelaar_Kantoor_ID = reader["Makelaar Kantoor-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Makelaar Kantoor-ID"]);
							makelaar.Makelaar_Makelaar = Convert.ToBoolean(reader["Makelaar Makelaar"]);
							makelaar.Makelaar_Fix = Convert.ToBoolean(reader["Makelaar Fix"]);
						}
					}
				}

				return makelaar;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Makelaar-ID],
										[Makelaar Code],
										[Makelaar],
										[Makelaar Kantoor-ID],
										[Makelaar Makelaar],
										[Makelaar Fix]
								FROM	[Makelaar];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}