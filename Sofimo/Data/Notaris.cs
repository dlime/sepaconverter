using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Notaris
	{
		#region Properties
		public int Notaris_ID { get; set; }
		public string Notaris_Naam { get; set; }
		public string Notaris_Telefoon { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Notaris]
								(
									[Notaris Naam],
									[Notaris Telefoon]
								)
								VALUES
								(
									@Notaris_Naam,
									@Notaris_Telefoon
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Notaris_Naam", OleDbType.WChar).Value = Notaris_Naam;
					cmd.Parameters.Add("@Notaris_Telefoon", OleDbType.WChar).Value = Notaris_Telefoon == null ? (Object)DBNull.Value : Notaris_Telefoon;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Notaris_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Notaris]
								SET		[Notaris Naam] = @Notaris_Naam,
										[Notaris Telefoon] = @Notaris_Telefoon
								WHERE	[Notaris-ID] = @Notaris_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Notaris_Naam", OleDbType.WChar).Value = Notaris_Naam;
					cmd.Parameters.Add("@Notaris_Telefoon", OleDbType.WChar).Value = Notaris_Telefoon == null ? (Object)DBNull.Value : Notaris_Telefoon;
					cmd.Parameters.Add("@Notaris_ID", OleDbType.Integer).Value = Notaris_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int notaris_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Notaris]
								WHERE	[Notaris-ID] = @Notaris_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Notaris_ID", OleDbType.Integer).Value = notaris_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Notaris Get(int notaris_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Notaris-ID],
										[Notaris Naam],
										[Notaris Telefoon]
								FROM	[Notaris]
								WHERE	[Notaris-ID] = @Notaris_ID;";

				Notaris notaris = new Notaris();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Notaris_ID", OleDbType.Integer).Value = notaris_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							notaris.Notaris_ID = Convert.ToInt32(reader["Notaris-ID"]);
							notaris.Notaris_Naam = reader["Notaris Naam"].ToString();
							notaris.Notaris_Telefoon = reader["Notaris Telefoon"] == DBNull.Value ? null : reader["Notaris Telefoon"].ToString();
						}
					}
				}

				return notaris;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Notaris-ID],
										[Notaris Naam],
										[Notaris Telefoon]
								FROM	[Notaris];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}