using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Krant
	{
		#region Properties
		public int Krant_ID { get; set; }
		public string Krant_Code { get; set; }
		public string Krant1 { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Krant]
								(
									[Krant Code],
									[Krant]
								)
								VALUES
								(
									@Krant_Code,
									@Krant
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Krant_Code", OleDbType.WChar).Value = Krant_Code == null ? (Object)DBNull.Value : Krant_Code;
					cmd.Parameters.Add("@Krant", OleDbType.WChar).Value = Krant1 == null ? (Object)DBNull.Value : Krant1;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Krant_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Krant]
								SET		[Krant Code] = @Krant_Code,
										[Krant] = @Krant
								WHERE	[Krant-ID] = @Krant_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Krant_Code", OleDbType.WChar).Value = Krant_Code == null ? (Object)DBNull.Value : Krant_Code;
					cmd.Parameters.Add("@Krant", OleDbType.WChar).Value = Krant1 == null ? (Object)DBNull.Value : Krant1;
					cmd.Parameters.Add("@Krant_ID", OleDbType.Integer).Value = Krant_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int krant_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Krant]
								WHERE	[Krant-ID] = @Krant_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Krant_ID", OleDbType.Integer).Value = krant_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Krant Get(int krant_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Krant-ID],
										[Krant Code],
										[Krant]
								FROM	[Krant]
								WHERE	[Krant-ID] = @Krant_ID;";

				Krant krant = new Krant();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Krant_ID", OleDbType.Integer).Value = krant_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							krant.Krant_ID = Convert.ToInt32(reader["Krant-ID"]);
							krant.Krant_Code = reader["Krant Code"] == DBNull.Value ? null : reader["Krant Code"].ToString();
							krant.Krant1 = reader["Krant"] == DBNull.Value ? null : reader["Krant"].ToString();
						}
					}
				}

				return krant;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Krant-ID],
										[Krant Code],
										[Krant]
								FROM	[Krant];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}