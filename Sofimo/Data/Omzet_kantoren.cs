using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Omzet_kantoren
	{
		#region Properties
		public int OmzKan_ID { get; set; }
		public string OmzKan_Kantoor { get; set; }
		public int? OmzKan_Bedrag { get; set; }
		public double? OmzKan_Bedrag_Euro { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Omzet kantoren]
								(
									[OmzKan Kantoor],
									[OmzKan Bedrag],
									[OmzKan Bedrag Euro]
								)
								VALUES
								(
									@OmzKan_Kantoor,
									@OmzKan_Bedrag,
									@OmzKan_Bedrag_Euro
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzKan_Kantoor", OleDbType.WChar).Value = OmzKan_Kantoor == null ? (Object)DBNull.Value : OmzKan_Kantoor;
					cmd.Parameters.Add("@OmzKan_Bedrag", OleDbType.Integer).Value = OmzKan_Bedrag == null ? (Object)DBNull.Value : OmzKan_Bedrag;
					cmd.Parameters.Add("@OmzKan_Bedrag_Euro", OleDbType.Double).Value = OmzKan_Bedrag_Euro == null ? (Object)DBNull.Value : OmzKan_Bedrag_Euro;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					OmzKan_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Omzet kantoren]
								SET		[OmzKan Kantoor] = @OmzKan_Kantoor,
										[OmzKan Bedrag] = @OmzKan_Bedrag,
										[OmzKan Bedrag Euro] = @OmzKan_Bedrag_Euro
								WHERE	[OmzKan-ID] = @OmzKan_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzKan_Kantoor", OleDbType.WChar).Value = OmzKan_Kantoor == null ? (Object)DBNull.Value : OmzKan_Kantoor;
					cmd.Parameters.Add("@OmzKan_Bedrag", OleDbType.Integer).Value = OmzKan_Bedrag == null ? (Object)DBNull.Value : OmzKan_Bedrag;
					cmd.Parameters.Add("@OmzKan_Bedrag_Euro", OleDbType.Double).Value = OmzKan_Bedrag_Euro == null ? (Object)DBNull.Value : OmzKan_Bedrag_Euro;
					cmd.Parameters.Add("@OmzKan_ID", OleDbType.Integer).Value = OmzKan_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int omzKan_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Omzet kantoren]
								WHERE	[OmzKan-ID] = @OmzKan_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzKan_ID", OleDbType.Integer).Value = omzKan_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Omzet_kantoren Get(int omzKan_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[OmzKan-ID],
										[OmzKan Kantoor],
										[OmzKan Bedrag],
										[OmzKan Bedrag Euro]
								FROM	[Omzet kantoren]
								WHERE	[OmzKan-ID] = @OmzKan_ID;";

				Omzet_kantoren omzet_kantoren = new Omzet_kantoren();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzKan_ID", OleDbType.Integer).Value = omzKan_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							omzet_kantoren.OmzKan_ID = Convert.ToInt32(reader["OmzKan-ID"]);
							omzet_kantoren.OmzKan_Kantoor = reader["OmzKan Kantoor"] == DBNull.Value ? null : reader["OmzKan Kantoor"].ToString();
							omzet_kantoren.OmzKan_Bedrag = reader["OmzKan Bedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["OmzKan Bedrag"]);
							omzet_kantoren.OmzKan_Bedrag_Euro = reader["OmzKan Bedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["OmzKan Bedrag Euro"]);
						}
					}
				}

				return omzet_kantoren;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[OmzKan-ID],
										[OmzKan Kantoor],
										[OmzKan Bedrag],
										[OmzKan Bedrag Euro]
								FROM	[Omzet kantoren];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}