using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Waarborg
	{
		#region Properties
		public int Waarborg_ID { get; set; }
		public string Waarborg_Hoe { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Waarborg]
								(
									[Waarborg Hoe]
								)
								VALUES
								(
									@Waarborg_Hoe
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Waarborg_Hoe", OleDbType.WChar).Value = Waarborg_Hoe == null ? (Object)DBNull.Value : Waarborg_Hoe;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Waarborg_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Waarborg]
								SET		[Waarborg Hoe] = @Waarborg_Hoe
								WHERE	[Waarborg-ID] = @Waarborg_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Waarborg_Hoe", OleDbType.WChar).Value = Waarborg_Hoe == null ? (Object)DBNull.Value : Waarborg_Hoe;
					cmd.Parameters.Add("@Waarborg_ID", OleDbType.Integer).Value = Waarborg_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int waarborg_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Waarborg]
								WHERE	[Waarborg-ID] = @Waarborg_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Waarborg_ID", OleDbType.Integer).Value = waarborg_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Waarborg Get(int waarborg_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Waarborg-ID],
										[Waarborg Hoe]
								FROM	[Waarborg]
								WHERE	[Waarborg-ID] = @Waarborg_ID;";

				Waarborg waarborg = new Waarborg();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Waarborg_ID", OleDbType.Integer).Value = waarborg_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							waarborg.Waarborg_ID = Convert.ToInt32(reader["Waarborg-ID"]);
							waarborg.Waarborg_Hoe = reader["Waarborg Hoe"] == DBNull.Value ? null : reader["Waarborg Hoe"].ToString();
						}
					}
				}

				return waarborg;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Waarborg-ID],
										[Waarborg Hoe]
								FROM	[Waarborg];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}