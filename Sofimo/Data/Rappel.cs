using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Rappel
	{
		#region Properties
		public int RappelID { get; set; }
		public int RappelEigendom_Huurder_ID { get; set; }
		public DateTime? RappelDatum { get; set; }
		public string RappelAard { get; set; }
		public string RappelOmschrijving { get; set; }
		public bool RappelVerzonden { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Rappel]
								(
									[RappelDatum],
									[RappelAard],
									[RappelOmschrijving],
									[RappelVerzonden]
								)
								VALUES
								(
									@RappelDatum,
									@RappelAard,
									@RappelOmschrijving,
									@RappelVerzonden
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@RappelDatum", OleDbType.Date).Value = RappelDatum == null ? (Object)DBNull.Value : RappelDatum;
					cmd.Parameters.Add("@RappelAard", OleDbType.WChar).Value = RappelAard == null ? (Object)DBNull.Value : RappelAard;
					cmd.Parameters.Add("@RappelOmschrijving", OleDbType.WChar).Value = RappelOmschrijving == null ? (Object)DBNull.Value : RappelOmschrijving;
					cmd.Parameters.Add("@RappelVerzonden", OleDbType.Boolean).Value = RappelVerzonden;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					RappelID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Rappel]
								SET		[RappelDatum] = @RappelDatum,
										[RappelAard] = @RappelAard,
										[RappelOmschrijving] = @RappelOmschrijving,
										[RappelVerzonden] = @RappelVerzonden
								WHERE	[RappelID] = @RappelID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@RappelEigendom_Huurder_ID", OleDbType.Integer).Value = RappelEigendom_Huurder_ID;
					cmd.Parameters.Add("@RappelDatum", OleDbType.Date).Value = RappelDatum == null ? (Object)DBNull.Value : RappelDatum;
					cmd.Parameters.Add("@RappelAard", OleDbType.WChar).Value = RappelAard == null ? (Object)DBNull.Value : RappelAard;
					cmd.Parameters.Add("@RappelOmschrijving", OleDbType.WChar).Value = RappelOmschrijving == null ? (Object)DBNull.Value : RappelOmschrijving;
					cmd.Parameters.Add("@RappelVerzonden", OleDbType.Boolean).Value = RappelVerzonden;
					cmd.Parameters.Add("@RappelID", OleDbType.Integer).Value = RappelID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int rappelID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Rappel]
								WHERE	[RappelID] = @RappelID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@RappelID", OleDbType.Integer).Value = rappelID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Rappel Get(int rappelID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[RappelID],
										[RappelEigendom-Huurder-ID],
										[RappelDatum],
										[RappelAard],
										[RappelOmschrijving],
										[RappelVerzonden]
								FROM	[Rappel]
								WHERE	[RappelID] = @RappelID;";

				Rappel rappel = new Rappel();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@RappelID", OleDbType.Integer).Value = rappelID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							rappel.RappelID = Convert.ToInt32(reader["RappelID"]);
							rappel.RappelEigendom_Huurder_ID = Convert.ToInt32(reader["RappelEigendom-Huurder-ID"]);
							rappel.RappelDatum = reader["RappelDatum"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["RappelDatum"]);
							rappel.RappelAard = reader["RappelAard"] == DBNull.Value ? null : reader["RappelAard"].ToString();
							rappel.RappelOmschrijving = reader["RappelOmschrijving"] == DBNull.Value ? null : reader["RappelOmschrijving"].ToString();
							rappel.RappelVerzonden = Convert.ToBoolean(reader["RappelVerzonden"]);
						}
					}
				}

				return rappel;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[RappelID],
										[RappelEigendom-Huurder-ID],
										[RappelDatum],
										[RappelAard],
										[RappelOmschrijving],
										[RappelVerzonden]
								FROM	[Rappel];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}