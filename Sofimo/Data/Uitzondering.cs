using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Uitzondering
	{
		#region Properties
		public int Uitzondering_ID { get; set; }
		public string Uitzondering_Telefoon { get; set; }
		public string Uitzondering_Naam { get; set; }
		public string Uitzondering_Memo { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Uitzondering]
								(
									[Uitzondering Telefoon],
									[Uitzondering Naam],
									[Uitzondering Memo]
								)
								VALUES
								(
									@Uitzondering_Telefoon,
									@Uitzondering_Naam,
									@Uitzondering_Memo
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Uitzondering_Telefoon", OleDbType.WChar).Value = Uitzondering_Telefoon == null ? (Object)DBNull.Value : Uitzondering_Telefoon;
					cmd.Parameters.Add("@Uitzondering_Naam", OleDbType.WChar).Value = Uitzondering_Naam == null ? (Object)DBNull.Value : Uitzondering_Naam;
					cmd.Parameters.Add("@Uitzondering_Memo", OleDbType.WChar).Value = Uitzondering_Memo == null ? (Object)DBNull.Value : Uitzondering_Memo;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Uitzondering_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Uitzondering]
								SET		[Uitzondering Telefoon] = @Uitzondering_Telefoon,
										[Uitzondering Naam] = @Uitzondering_Naam,
										[Uitzondering Memo] = @Uitzondering_Memo
								WHERE	[Uitzondering-ID] = @Uitzondering_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Uitzondering_Telefoon", OleDbType.WChar).Value = Uitzondering_Telefoon == null ? (Object)DBNull.Value : Uitzondering_Telefoon;
					cmd.Parameters.Add("@Uitzondering_Naam", OleDbType.WChar).Value = Uitzondering_Naam == null ? (Object)DBNull.Value : Uitzondering_Naam;
					cmd.Parameters.Add("@Uitzondering_Memo", OleDbType.WChar).Value = Uitzondering_Memo == null ? (Object)DBNull.Value : Uitzondering_Memo;
					cmd.Parameters.Add("@Uitzondering_ID", OleDbType.Integer).Value = Uitzondering_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int uitzondering_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Uitzondering]
								WHERE	[Uitzondering-ID] = @Uitzondering_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Uitzondering_ID", OleDbType.Integer).Value = uitzondering_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Uitzondering Get(int uitzondering_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Uitzondering-ID],
										[Uitzondering Telefoon],
										[Uitzondering Naam],
										[Uitzondering Memo]
								FROM	[Uitzondering]
								WHERE	[Uitzondering-ID] = @Uitzondering_ID;";

				Uitzondering uitzondering = new Uitzondering();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Uitzondering_ID", OleDbType.Integer).Value = uitzondering_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							uitzondering.Uitzondering_ID = Convert.ToInt32(reader["Uitzondering-ID"]);
							uitzondering.Uitzondering_Telefoon = reader["Uitzondering Telefoon"] == DBNull.Value ? null : reader["Uitzondering Telefoon"].ToString();
							uitzondering.Uitzondering_Naam = reader["Uitzondering Naam"] == DBNull.Value ? null : reader["Uitzondering Naam"].ToString();
							uitzondering.Uitzondering_Memo = reader["Uitzondering Memo"] == DBNull.Value ? null : reader["Uitzondering Memo"].ToString();
						}
					}
				}

				return uitzondering;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Uitzondering-ID],
										[Uitzondering Telefoon],
										[Uitzondering Naam],
										[Uitzondering Memo]
								FROM	[Uitzondering];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}