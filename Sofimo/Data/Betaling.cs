using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Betaling
	{
		#region Properties
		public int Betaling_ID { get; set; }
		public int? Betaling_Factuur_ID { get; set; }
		public DateTime? Betaling_Datum { get; set; }
		public int? Betaling_Bedrag { get; set; }
		public double? Betaling_Bedrag_Euro { get; set; }
		public string Betaling_Commentaar { get; set; }
		public string BetalingAddUser { get; set; }
		public DateTime? BetalingAddNow { get; set; }
		public string BetalingChangeUser { get; set; }
		public DateTime? BetalingChangeNow { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Betaling]
								(
									[Betaling Factuur-ID],
									[Betaling Datum],
									[Betaling Bedrag],
									[Betaling Bedrag Euro],
									[Betaling Commentaar],
									[BetalingAddUser],
									[BetalingAddNow],
									[BetalingChangeUser],
									[BetalingChangeNow]
								)
								VALUES
								(
									@Betaling_Factuur_ID,
									@Betaling_Datum,
									@Betaling_Bedrag,
									@Betaling_Bedrag_Euro,
									@Betaling_Commentaar,
									@BetalingAddUser,
									@BetalingAddNow,
									@BetalingChangeUser,
									@BetalingChangeNow
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Betaling_Factuur_ID", OleDbType.Integer).Value = Betaling_Factuur_ID == null ? (Object)DBNull.Value : Betaling_Factuur_ID;
					cmd.Parameters.Add("@Betaling_Datum", OleDbType.Date).Value = Betaling_Datum == null ? (Object)DBNull.Value : Betaling_Datum;
					cmd.Parameters.Add("@Betaling_Bedrag", OleDbType.Integer).Value = Betaling_Bedrag == null ? (Object)DBNull.Value : Betaling_Bedrag;
					cmd.Parameters.Add("@Betaling_Bedrag_Euro", OleDbType.Double).Value = Betaling_Bedrag_Euro == null ? (Object)DBNull.Value : Betaling_Bedrag_Euro;
					cmd.Parameters.Add("@Betaling_Commentaar", OleDbType.WChar).Value = Betaling_Commentaar == null ? (Object)DBNull.Value : Betaling_Commentaar;
					cmd.Parameters.Add("@BetalingAddUser", OleDbType.WChar).Value = BetalingAddUser == null ? (Object)DBNull.Value : BetalingAddUser;
					cmd.Parameters.Add("@BetalingAddNow", OleDbType.Date).Value = BetalingAddNow == null ? (Object)DBNull.Value : BetalingAddNow;
					cmd.Parameters.Add("@BetalingChangeUser", OleDbType.WChar).Value = BetalingChangeUser == null ? (Object)DBNull.Value : BetalingChangeUser;
					cmd.Parameters.Add("@BetalingChangeNow", OleDbType.Date).Value = BetalingChangeNow == null ? (Object)DBNull.Value : BetalingChangeNow;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Betaling_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Betaling]
								SET		[Betaling Factuur-ID] = @Betaling_Factuur_ID,
										[Betaling Datum] = @Betaling_Datum,
										[Betaling Bedrag] = @Betaling_Bedrag,
										[Betaling Bedrag Euro] = @Betaling_Bedrag_Euro,
										[Betaling Commentaar] = @Betaling_Commentaar,
										[BetalingAddUser] = @BetalingAddUser,
										[BetalingAddNow] = @BetalingAddNow,
										[BetalingChangeUser] = @BetalingChangeUser,
										[BetalingChangeNow] = @BetalingChangeNow
								WHERE	[Betaling-ID] = @Betaling_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Betaling_Factuur_ID", OleDbType.Integer).Value = Betaling_Factuur_ID == null ? (Object)DBNull.Value : Betaling_Factuur_ID;
					cmd.Parameters.Add("@Betaling_Datum", OleDbType.Date).Value = Betaling_Datum == null ? (Object)DBNull.Value : Betaling_Datum;
					cmd.Parameters.Add("@Betaling_Bedrag", OleDbType.Integer).Value = Betaling_Bedrag == null ? (Object)DBNull.Value : Betaling_Bedrag;
					cmd.Parameters.Add("@Betaling_Bedrag_Euro", OleDbType.Double).Value = Betaling_Bedrag_Euro == null ? (Object)DBNull.Value : Betaling_Bedrag_Euro;
					cmd.Parameters.Add("@Betaling_Commentaar", OleDbType.WChar).Value = Betaling_Commentaar == null ? (Object)DBNull.Value : Betaling_Commentaar;
					cmd.Parameters.Add("@BetalingAddUser", OleDbType.WChar).Value = BetalingAddUser == null ? (Object)DBNull.Value : BetalingAddUser;
					cmd.Parameters.Add("@BetalingAddNow", OleDbType.Date).Value = BetalingAddNow == null ? (Object)DBNull.Value : BetalingAddNow;
					cmd.Parameters.Add("@BetalingChangeUser", OleDbType.WChar).Value = BetalingChangeUser == null ? (Object)DBNull.Value : BetalingChangeUser;
					cmd.Parameters.Add("@BetalingChangeNow", OleDbType.Date).Value = BetalingChangeNow == null ? (Object)DBNull.Value : BetalingChangeNow;
					cmd.Parameters.Add("@Betaling_ID", OleDbType.Integer).Value = Betaling_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int betaling_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Betaling]
								WHERE	[Betaling-ID] = @Betaling_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Betaling_ID", OleDbType.Integer).Value = betaling_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Betaling Get(int betaling_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Betaling-ID],
										[Betaling Factuur-ID],
										[Betaling Datum],
										[Betaling Bedrag],
										[Betaling Bedrag Euro],
										[Betaling Commentaar],
										[BetalingAddUser],
										[BetalingAddNow],
										[BetalingChangeUser],
										[BetalingChangeNow]
								FROM	[Betaling]
								WHERE	[Betaling-ID] = @Betaling_ID;";

				Betaling betaling = new Betaling();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Betaling_ID", OleDbType.Integer).Value = betaling_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							betaling.Betaling_ID = Convert.ToInt32(reader["Betaling-ID"]);
							betaling.Betaling_Factuur_ID = reader["Betaling Factuur-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Betaling Factuur-ID"]);
							betaling.Betaling_Datum = reader["Betaling Datum"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Betaling Datum"]);
							betaling.Betaling_Bedrag = reader["Betaling Bedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Betaling Bedrag"]);
							betaling.Betaling_Bedrag_Euro = reader["Betaling Bedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Betaling Bedrag Euro"]);
							betaling.Betaling_Commentaar = reader["Betaling Commentaar"] == DBNull.Value ? null : reader["Betaling Commentaar"].ToString();
							betaling.BetalingAddUser = reader["BetalingAddUser"] == DBNull.Value ? null : reader["BetalingAddUser"].ToString();
							betaling.BetalingAddNow = reader["BetalingAddNow"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["BetalingAddNow"]);
							betaling.BetalingChangeUser = reader["BetalingChangeUser"] == DBNull.Value ? null : reader["BetalingChangeUser"].ToString();
							betaling.BetalingChangeNow = reader["BetalingChangeNow"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["BetalingChangeNow"]);
						}
					}
				}

				return betaling;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Betaling-ID],
										[Betaling Factuur-ID],
										[Betaling Datum],
										[Betaling Bedrag],
										[Betaling Bedrag Euro],
										[Betaling Commentaar],
										[BetalingAddUser],
										[BetalingAddNow],
										[BetalingChangeUser],
										[BetalingChangeNow]
								FROM	[Betaling];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}