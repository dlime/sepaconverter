using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Eigendom_Huurder
	{
		#region Properties
		public int Eigendom_Huurder_ID { get; set; }
		public int Eigendom_Huurder_Advokaat_ID { get; set; }
		public int Eigendom_Huurder_Eigendom_ID { get; set; }
		public int Eigendom_Huurder_Waarborg_ID { get; set; }
		public int Eigendom_Huurder_Huurder_ID { get; set; }
		public int? Eigendom_Huurder_Soort_contract_ID { get; set; }
		public bool Eigendom_Huurder_Brandverzekering { get; set; }
		public DateTime? Eigendom_Huurder_Begin_huur { get; set; }
		public DateTime? Eigendom_Huurder_Einde_huur { get; set; }
		public int? Eigendom_Huurder_Basishuur { get; set; }
		public double? Eigendom_Huurder_Basishuur_Euro { get; set; }
		public byte? Eigendom_Huurder_Betalen_tegen { get; set; }
		public int? Eigendom_Huurder_Huur { get; set; }
		public double? Eigendom_Huurder_Huur_Euro { get; set; }
		public int? Eigendom_Huurder_Waarborg { get; set; }
		public double? Eigendom_Huurder_Waarborg_Euro { get; set; }
		public bool Eigendom_Huurder_Verrekend { get; set; }
		public DateTime? Eigendom_Huurder_Datum_Waarborg { get; set; }
		public int? Eigendom_Huurder_Saldo { get; set; }
		public double? Eigendom_Huurder_Saldo_Euro { get; set; }
		public int? Eigendom_Huurder_Aantal_boetes { get; set; }
		public int? Eigendom_Huurder_Boete { get; set; }
		public double? Eigendom_Huurder_Boete_Euro { get; set; }
		public int? Eigendom_Huurder_Syndic { get; set; }
		public double? Eigendom_Huurder_Syndic_Euro { get; set; }
		public int? Eigendom_Huurder_BTW { get; set; }
		public double? Eigendom_Huurder_Basisindex { get; set; }
		public byte? Eigendom_Huurder_Indexverrekening { get; set; }
		public bool Eigendom_Huurder_Index_verrekend { get; set; }
		public string Eigendom_Huurder_Opmerking { get; set; }
		public bool Eigendom_Huurder_Afgehandeld { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Eigendom-Huurder]
								(
									[Eigendom-Huurder Soort contract-ID],
									[Eigendom-Huurder Brandverzekering],
									[Eigendom-Huurder Begin huur],
									[Eigendom-Huurder Einde huur],
									[Eigendom-Huurder Basishuur],
									[Eigendom-Huurder Basishuur Euro],
									[Eigendom-Huurder Betalen tegen],
									[Eigendom-Huurder Huur],
									[Eigendom-Huurder Huur Euro],
									[Eigendom-Huurder Waarborg],
									[Eigendom-Huurder Waarborg Euro],
									[Eigendom-Huurder Verrekend],
									[Eigendom-Huurder Datum Waarborg],
									[Eigendom-Huurder Saldo],
									[Eigendom-Huurder Saldo Euro],
									[Eigendom-Huurder Aantal boetes],
									[Eigendom-Huurder Boete],
									[Eigendom-Huurder Boete Euro],
									[Eigendom-Huurder Syndic],
									[Eigendom-Huurder Syndic Euro],
									[Eigendom-Huurder BTW],
									[Eigendom-Huurder Basisindex],
									[Eigendom-Huurder Indexverrekening],
									[Eigendom-Huurder Index verrekend],
									[Eigendom-Huurder Opmerking],
									[Eigendom-Huurder Afgehandeld]
								)
								VALUES
								(
									@Eigendom_Huurder_Soort_contract_ID,
									@Eigendom_Huurder_Brandverzekering,
									@Eigendom_Huurder_Begin_huur,
									@Eigendom_Huurder_Einde_huur,
									@Eigendom_Huurder_Basishuur,
									@Eigendom_Huurder_Basishuur_Euro,
									@Eigendom_Huurder_Betalen_tegen,
									@Eigendom_Huurder_Huur,
									@Eigendom_Huurder_Huur_Euro,
									@Eigendom_Huurder_Waarborg,
									@Eigendom_Huurder_Waarborg_Euro,
									@Eigendom_Huurder_Verrekend,
									@Eigendom_Huurder_Datum_Waarborg,
									@Eigendom_Huurder_Saldo,
									@Eigendom_Huurder_Saldo_Euro,
									@Eigendom_Huurder_Aantal_boetes,
									@Eigendom_Huurder_Boete,
									@Eigendom_Huurder_Boete_Euro,
									@Eigendom_Huurder_Syndic,
									@Eigendom_Huurder_Syndic_Euro,
									@Eigendom_Huurder_BTW,
									@Eigendom_Huurder_Basisindex,
									@Eigendom_Huurder_Indexverrekening,
									@Eigendom_Huurder_Index_verrekend,
									@Eigendom_Huurder_Opmerking,
									@Eigendom_Huurder_Afgehandeld
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigendom_Huurder_Soort_contract_ID", OleDbType.Integer).Value = Eigendom_Huurder_Soort_contract_ID == null ? (Object)DBNull.Value : Eigendom_Huurder_Soort_contract_ID;
					cmd.Parameters.Add("@Eigendom_Huurder_Brandverzekering", OleDbType.Boolean).Value = Eigendom_Huurder_Brandverzekering;
					cmd.Parameters.Add("@Eigendom_Huurder_Begin_huur", OleDbType.Date).Value = Eigendom_Huurder_Begin_huur == null ? (Object)DBNull.Value : Eigendom_Huurder_Begin_huur;
					cmd.Parameters.Add("@Eigendom_Huurder_Einde_huur", OleDbType.Date).Value = Eigendom_Huurder_Einde_huur == null ? (Object)DBNull.Value : Eigendom_Huurder_Einde_huur;
					cmd.Parameters.Add("@Eigendom_Huurder_Basishuur", OleDbType.Integer).Value = Eigendom_Huurder_Basishuur == null ? (Object)DBNull.Value : Eigendom_Huurder_Basishuur;
					cmd.Parameters.Add("@Eigendom_Huurder_Basishuur_Euro", OleDbType.Double).Value = Eigendom_Huurder_Basishuur_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Basishuur_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Betalen_tegen", OleDbType.UnsignedTinyInt).Value = Eigendom_Huurder_Betalen_tegen == null ? (Object)DBNull.Value : Eigendom_Huurder_Betalen_tegen;
					cmd.Parameters.Add("@Eigendom_Huurder_Huur", OleDbType.Integer).Value = Eigendom_Huurder_Huur == null ? (Object)DBNull.Value : Eigendom_Huurder_Huur;
					cmd.Parameters.Add("@Eigendom_Huurder_Huur_Euro", OleDbType.Double).Value = Eigendom_Huurder_Huur_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Huur_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Waarborg", OleDbType.Integer).Value = Eigendom_Huurder_Waarborg == null ? (Object)DBNull.Value : Eigendom_Huurder_Waarborg;
					cmd.Parameters.Add("@Eigendom_Huurder_Waarborg_Euro", OleDbType.Double).Value = Eigendom_Huurder_Waarborg_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Waarborg_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Verrekend", OleDbType.Boolean).Value = Eigendom_Huurder_Verrekend;
					cmd.Parameters.Add("@Eigendom_Huurder_Datum_Waarborg", OleDbType.Date).Value = Eigendom_Huurder_Datum_Waarborg == null ? (Object)DBNull.Value : Eigendom_Huurder_Datum_Waarborg;
					cmd.Parameters.Add("@Eigendom_Huurder_Saldo", OleDbType.Integer).Value = Eigendom_Huurder_Saldo == null ? (Object)DBNull.Value : Eigendom_Huurder_Saldo;
					cmd.Parameters.Add("@Eigendom_Huurder_Saldo_Euro", OleDbType.Double).Value = Eigendom_Huurder_Saldo_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Saldo_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Aantal_boetes", OleDbType.Integer).Value = Eigendom_Huurder_Aantal_boetes == null ? (Object)DBNull.Value : Eigendom_Huurder_Aantal_boetes;
					cmd.Parameters.Add("@Eigendom_Huurder_Boete", OleDbType.Integer).Value = Eigendom_Huurder_Boete == null ? (Object)DBNull.Value : Eigendom_Huurder_Boete;
					cmd.Parameters.Add("@Eigendom_Huurder_Boete_Euro", OleDbType.Double).Value = Eigendom_Huurder_Boete_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Boete_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Syndic", OleDbType.Integer).Value = Eigendom_Huurder_Syndic == null ? (Object)DBNull.Value : Eigendom_Huurder_Syndic;
					cmd.Parameters.Add("@Eigendom_Huurder_Syndic_Euro", OleDbType.Double).Value = Eigendom_Huurder_Syndic_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Syndic_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_BTW", OleDbType.Integer).Value = Eigendom_Huurder_BTW == null ? (Object)DBNull.Value : Eigendom_Huurder_BTW;
					cmd.Parameters.Add("@Eigendom_Huurder_Basisindex", OleDbType.Double).Value = Eigendom_Huurder_Basisindex == null ? (Object)DBNull.Value : Eigendom_Huurder_Basisindex;
					cmd.Parameters.Add("@Eigendom_Huurder_Indexverrekening", OleDbType.UnsignedTinyInt).Value = Eigendom_Huurder_Indexverrekening == null ? (Object)DBNull.Value : Eigendom_Huurder_Indexverrekening;
					cmd.Parameters.Add("@Eigendom_Huurder_Index_verrekend", OleDbType.Boolean).Value = Eigendom_Huurder_Index_verrekend;
					cmd.Parameters.Add("@Eigendom_Huurder_Opmerking", OleDbType.WChar).Value = Eigendom_Huurder_Opmerking == null ? (Object)DBNull.Value : Eigendom_Huurder_Opmerking;
					cmd.Parameters.Add("@Eigendom_Huurder_Afgehandeld", OleDbType.Boolean).Value = Eigendom_Huurder_Afgehandeld;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Eigendom_Huurder_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Eigendom-Huurder]
								SET		[Eigendom-Huurder Soort contract-ID] = @Eigendom_Huurder_Soort_contract_ID,
										[Eigendom-Huurder Brandverzekering] = @Eigendom_Huurder_Brandverzekering,
										[Eigendom-Huurder Begin huur] = @Eigendom_Huurder_Begin_huur,
										[Eigendom-Huurder Einde huur] = @Eigendom_Huurder_Einde_huur,
										[Eigendom-Huurder Basishuur] = @Eigendom_Huurder_Basishuur,
										[Eigendom-Huurder Basishuur Euro] = @Eigendom_Huurder_Basishuur_Euro,
										[Eigendom-Huurder Betalen tegen] = @Eigendom_Huurder_Betalen_tegen,
										[Eigendom-Huurder Huur] = @Eigendom_Huurder_Huur,
										[Eigendom-Huurder Huur Euro] = @Eigendom_Huurder_Huur_Euro,
										[Eigendom-Huurder Waarborg] = @Eigendom_Huurder_Waarborg,
										[Eigendom-Huurder Waarborg Euro] = @Eigendom_Huurder_Waarborg_Euro,
										[Eigendom-Huurder Verrekend] = @Eigendom_Huurder_Verrekend,
										[Eigendom-Huurder Datum Waarborg] = @Eigendom_Huurder_Datum_Waarborg,
										[Eigendom-Huurder Saldo] = @Eigendom_Huurder_Saldo,
										[Eigendom-Huurder Saldo Euro] = @Eigendom_Huurder_Saldo_Euro,
										[Eigendom-Huurder Aantal boetes] = @Eigendom_Huurder_Aantal_boetes,
										[Eigendom-Huurder Boete] = @Eigendom_Huurder_Boete,
										[Eigendom-Huurder Boete Euro] = @Eigendom_Huurder_Boete_Euro,
										[Eigendom-Huurder Syndic] = @Eigendom_Huurder_Syndic,
										[Eigendom-Huurder Syndic Euro] = @Eigendom_Huurder_Syndic_Euro,
										[Eigendom-Huurder BTW] = @Eigendom_Huurder_BTW,
										[Eigendom-Huurder Basisindex] = @Eigendom_Huurder_Basisindex,
										[Eigendom-Huurder Indexverrekening] = @Eigendom_Huurder_Indexverrekening,
										[Eigendom-Huurder Index verrekend] = @Eigendom_Huurder_Index_verrekend,
										[Eigendom-Huurder Opmerking] = @Eigendom_Huurder_Opmerking,
										[Eigendom-Huurder Afgehandeld] = @Eigendom_Huurder_Afgehandeld
								WHERE	[Eigendom-Huurder-ID] = @Eigendom_Huurder_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigendom_Huurder_Advokaat_ID", OleDbType.Integer).Value = Eigendom_Huurder_Advokaat_ID;
					cmd.Parameters.Add("@Eigendom_Huurder_Eigendom_ID", OleDbType.Integer).Value = Eigendom_Huurder_Eigendom_ID;
					cmd.Parameters.Add("@Eigendom_Huurder_Waarborg_ID", OleDbType.Integer).Value = Eigendom_Huurder_Waarborg_ID;
					cmd.Parameters.Add("@Eigendom_Huurder_Huurder_ID", OleDbType.Integer).Value = Eigendom_Huurder_Huurder_ID;
					cmd.Parameters.Add("@Eigendom_Huurder_Soort_contract_ID", OleDbType.Integer).Value = Eigendom_Huurder_Soort_contract_ID == null ? (Object)DBNull.Value : Eigendom_Huurder_Soort_contract_ID;
					cmd.Parameters.Add("@Eigendom_Huurder_Brandverzekering", OleDbType.Boolean).Value = Eigendom_Huurder_Brandverzekering;
					cmd.Parameters.Add("@Eigendom_Huurder_Begin_huur", OleDbType.Date).Value = Eigendom_Huurder_Begin_huur == null ? (Object)DBNull.Value : Eigendom_Huurder_Begin_huur;
					cmd.Parameters.Add("@Eigendom_Huurder_Einde_huur", OleDbType.Date).Value = Eigendom_Huurder_Einde_huur == null ? (Object)DBNull.Value : Eigendom_Huurder_Einde_huur;
					cmd.Parameters.Add("@Eigendom_Huurder_Basishuur", OleDbType.Integer).Value = Eigendom_Huurder_Basishuur == null ? (Object)DBNull.Value : Eigendom_Huurder_Basishuur;
					cmd.Parameters.Add("@Eigendom_Huurder_Basishuur_Euro", OleDbType.Double).Value = Eigendom_Huurder_Basishuur_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Basishuur_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Betalen_tegen", OleDbType.UnsignedTinyInt).Value = Eigendom_Huurder_Betalen_tegen == null ? (Object)DBNull.Value : Eigendom_Huurder_Betalen_tegen;
					cmd.Parameters.Add("@Eigendom_Huurder_Huur", OleDbType.Integer).Value = Eigendom_Huurder_Huur == null ? (Object)DBNull.Value : Eigendom_Huurder_Huur;
					cmd.Parameters.Add("@Eigendom_Huurder_Huur_Euro", OleDbType.Double).Value = Eigendom_Huurder_Huur_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Huur_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Waarborg", OleDbType.Integer).Value = Eigendom_Huurder_Waarborg == null ? (Object)DBNull.Value : Eigendom_Huurder_Waarborg;
					cmd.Parameters.Add("@Eigendom_Huurder_Waarborg_Euro", OleDbType.Double).Value = Eigendom_Huurder_Waarborg_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Waarborg_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Verrekend", OleDbType.Boolean).Value = Eigendom_Huurder_Verrekend;
					cmd.Parameters.Add("@Eigendom_Huurder_Datum_Waarborg", OleDbType.Date).Value = Eigendom_Huurder_Datum_Waarborg == null ? (Object)DBNull.Value : Eigendom_Huurder_Datum_Waarborg;
					cmd.Parameters.Add("@Eigendom_Huurder_Saldo", OleDbType.Integer).Value = Eigendom_Huurder_Saldo == null ? (Object)DBNull.Value : Eigendom_Huurder_Saldo;
					cmd.Parameters.Add("@Eigendom_Huurder_Saldo_Euro", OleDbType.Double).Value = Eigendom_Huurder_Saldo_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Saldo_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Aantal_boetes", OleDbType.Integer).Value = Eigendom_Huurder_Aantal_boetes == null ? (Object)DBNull.Value : Eigendom_Huurder_Aantal_boetes;
					cmd.Parameters.Add("@Eigendom_Huurder_Boete", OleDbType.Integer).Value = Eigendom_Huurder_Boete == null ? (Object)DBNull.Value : Eigendom_Huurder_Boete;
					cmd.Parameters.Add("@Eigendom_Huurder_Boete_Euro", OleDbType.Double).Value = Eigendom_Huurder_Boete_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Boete_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_Syndic", OleDbType.Integer).Value = Eigendom_Huurder_Syndic == null ? (Object)DBNull.Value : Eigendom_Huurder_Syndic;
					cmd.Parameters.Add("@Eigendom_Huurder_Syndic_Euro", OleDbType.Double).Value = Eigendom_Huurder_Syndic_Euro == null ? (Object)DBNull.Value : Eigendom_Huurder_Syndic_Euro;
					cmd.Parameters.Add("@Eigendom_Huurder_BTW", OleDbType.Integer).Value = Eigendom_Huurder_BTW == null ? (Object)DBNull.Value : Eigendom_Huurder_BTW;
					cmd.Parameters.Add("@Eigendom_Huurder_Basisindex", OleDbType.Double).Value = Eigendom_Huurder_Basisindex == null ? (Object)DBNull.Value : Eigendom_Huurder_Basisindex;
					cmd.Parameters.Add("@Eigendom_Huurder_Indexverrekening", OleDbType.UnsignedTinyInt).Value = Eigendom_Huurder_Indexverrekening == null ? (Object)DBNull.Value : Eigendom_Huurder_Indexverrekening;
					cmd.Parameters.Add("@Eigendom_Huurder_Index_verrekend", OleDbType.Boolean).Value = Eigendom_Huurder_Index_verrekend;
					cmd.Parameters.Add("@Eigendom_Huurder_Opmerking", OleDbType.WChar).Value = Eigendom_Huurder_Opmerking == null ? (Object)DBNull.Value : Eigendom_Huurder_Opmerking;
					cmd.Parameters.Add("@Eigendom_Huurder_Afgehandeld", OleDbType.Boolean).Value = Eigendom_Huurder_Afgehandeld;
					cmd.Parameters.Add("@Eigendom_Huurder_ID", OleDbType.Integer).Value = Eigendom_Huurder_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int eigendom_Huurder_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Eigendom-Huurder]
								WHERE	[Eigendom-Huurder-ID] = @Eigendom_Huurder_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigendom_Huurder_ID", OleDbType.Integer).Value = eigendom_Huurder_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
        #endregion

        #region Get
        /// <summary>
        /// Gets an existing record.
        /// </summary>
        public static List<Eigendom_Huurder> GetAllList()
        {
            String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                String sql = @"SELECT	[Eigendom-Huurder-ID],
										[Eigendom-Huurder Advokaat-ID],
										[Eigendom-Huurder Eigendom-ID],
										[Eigendom-Huurder Waarborg-ID],
										[Eigendom-Huurder Huurder-ID],
										[Eigendom-Huurder Soort contract-ID],
										[Eigendom-Huurder Brandverzekering],
										[Eigendom-Huurder Begin huur],
										[Eigendom-Huurder Einde huur],
										[Eigendom-Huurder Basishuur],
										[Eigendom-Huurder Basishuur Euro],
										[Eigendom-Huurder Betalen tegen],
										[Eigendom-Huurder Huur],
										[Eigendom-Huurder Huur Euro],
										[Eigendom-Huurder Waarborg],
										[Eigendom-Huurder Waarborg Euro],
										[Eigendom-Huurder Verrekend],
										[Eigendom-Huurder Datum Waarborg],
										[Eigendom-Huurder Saldo],
										[Eigendom-Huurder Saldo Euro],
										[Eigendom-Huurder Aantal boetes],
										[Eigendom-Huurder Boete],
										[Eigendom-Huurder Boete Euro],
										[Eigendom-Huurder Syndic],
										[Eigendom-Huurder Syndic Euro],
										[Eigendom-Huurder BTW],
										[Eigendom-Huurder Basisindex],
										[Eigendom-Huurder Indexverrekening],
										[Eigendom-Huurder Index verrekend],
										[Eigendom-Huurder Opmerking],
										[Eigendom-Huurder Afgehandeld]
								FROM	[Eigendom-Huurder];";


                List<Eigendom_Huurder> eigendom_Huurder_list = new List<Eigendom_Huurder>();
                con.Open();

                using (OleDbCommand cmd = new OleDbCommand(sql, con))
                {
                    using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        while (reader.Read())
                        {
                            Eigendom_Huurder eigendom_Huurder = new Eigendom_Huurder();
                            eigendom_Huurder.Eigendom_Huurder_ID = Convert.ToInt32(reader["Eigendom-Huurder-ID"]);
                            eigendom_Huurder.Eigendom_Huurder_Advokaat_ID = Convert.ToInt32(reader["Eigendom-Huurder Advokaat-ID"]);
                            eigendom_Huurder.Eigendom_Huurder_Eigendom_ID = Convert.ToInt32(reader["Eigendom-Huurder Eigendom-ID"]);
                            eigendom_Huurder.Eigendom_Huurder_Waarborg_ID = Convert.ToInt32(reader["Eigendom-Huurder Waarborg-ID"]);
                            eigendom_Huurder.Eigendom_Huurder_Huurder_ID = Convert.ToInt32(reader["Eigendom-Huurder Huurder-ID"]);
                            eigendom_Huurder.Eigendom_Huurder_Soort_contract_ID = reader["Eigendom-Huurder Soort contract-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Soort contract-ID"]);
                            eigendom_Huurder.Eigendom_Huurder_Brandverzekering = Convert.ToBoolean(reader["Eigendom-Huurder Brandverzekering"]);
                            eigendom_Huurder.Eigendom_Huurder_Begin_huur = reader["Eigendom-Huurder Begin huur"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom-Huurder Begin huur"]);
                            eigendom_Huurder.Eigendom_Huurder_Einde_huur = reader["Eigendom-Huurder Einde huur"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom-Huurder Einde huur"]);
                            eigendom_Huurder.Eigendom_Huurder_Basishuur = reader["Eigendom-Huurder Basishuur"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Basishuur"]);
                            eigendom_Huurder.Eigendom_Huurder_Basishuur_Euro = reader["Eigendom-Huurder Basishuur Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Basishuur Euro"]);
                            eigendom_Huurder.Eigendom_Huurder_Betalen_tegen = reader["Eigendom-Huurder Betalen tegen"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Eigendom-Huurder Betalen tegen"]);
                            eigendom_Huurder.Eigendom_Huurder_Huur = reader["Eigendom-Huurder Huur"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Huur"]);
                            eigendom_Huurder.Eigendom_Huurder_Huur_Euro = reader["Eigendom-Huurder Huur Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Huur Euro"]);
                            eigendom_Huurder.Eigendom_Huurder_Waarborg = reader["Eigendom-Huurder Waarborg"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Waarborg"]);
                            eigendom_Huurder.Eigendom_Huurder_Waarborg_Euro = reader["Eigendom-Huurder Waarborg Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Waarborg Euro"]);
                            eigendom_Huurder.Eigendom_Huurder_Verrekend = Convert.ToBoolean(reader["Eigendom-Huurder Verrekend"]);
                            eigendom_Huurder.Eigendom_Huurder_Datum_Waarborg = reader["Eigendom-Huurder Datum Waarborg"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom-Huurder Datum Waarborg"]);
                            eigendom_Huurder.Eigendom_Huurder_Saldo = reader["Eigendom-Huurder Saldo"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Saldo"]);
                            eigendom_Huurder.Eigendom_Huurder_Saldo_Euro = reader["Eigendom-Huurder Saldo Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Saldo Euro"]);
                            eigendom_Huurder.Eigendom_Huurder_Aantal_boetes = reader["Eigendom-Huurder Aantal boetes"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Aantal boetes"]);
                            eigendom_Huurder.Eigendom_Huurder_Boete = reader["Eigendom-Huurder Boete"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Boete"]);
                            eigendom_Huurder.Eigendom_Huurder_Boete_Euro = reader["Eigendom-Huurder Boete Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Boete Euro"]);
                            eigendom_Huurder.Eigendom_Huurder_Syndic = reader["Eigendom-Huurder Syndic"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Syndic"]);
                            eigendom_Huurder.Eigendom_Huurder_Syndic_Euro = reader["Eigendom-Huurder Syndic Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Syndic Euro"]);
                            eigendom_Huurder.Eigendom_Huurder_BTW = reader["Eigendom-Huurder BTW"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder BTW"]);
                            eigendom_Huurder.Eigendom_Huurder_Basisindex = reader["Eigendom-Huurder Basisindex"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Basisindex"]);
                            eigendom_Huurder.Eigendom_Huurder_Indexverrekening = reader["Eigendom-Huurder Indexverrekening"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Eigendom-Huurder Indexverrekening"]);
                            eigendom_Huurder.Eigendom_Huurder_Index_verrekend = Convert.ToBoolean(reader["Eigendom-Huurder Index verrekend"]);
                            eigendom_Huurder.Eigendom_Huurder_Opmerking = reader["Eigendom-Huurder Opmerking"] == DBNull.Value ? null : reader["Eigendom-Huurder Opmerking"].ToString();
                            eigendom_Huurder.Eigendom_Huurder_Afgehandeld = Convert.ToBoolean(reader["Eigendom-Huurder Afgehandeld"]);
                            eigendom_Huurder_list.Add(eigendom_Huurder);
                        }
                    }
                }

                return eigendom_Huurder_list;
            }
        }

        /// <summary>
        /// Gets an existing record.
        /// </summary>
        public static Eigendom_Huurder Get(int eigendom_Huurder_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Eigendom-Huurder-ID],
										[Eigendom-Huurder Advokaat-ID],
										[Eigendom-Huurder Eigendom-ID],
										[Eigendom-Huurder Waarborg-ID],
										[Eigendom-Huurder Huurder-ID],
										[Eigendom-Huurder Soort contract-ID],
										[Eigendom-Huurder Brandverzekering],
										[Eigendom-Huurder Begin huur],
										[Eigendom-Huurder Einde huur],
										[Eigendom-Huurder Basishuur],
										[Eigendom-Huurder Basishuur Euro],
										[Eigendom-Huurder Betalen tegen],
										[Eigendom-Huurder Huur],
										[Eigendom-Huurder Huur Euro],
										[Eigendom-Huurder Waarborg],
										[Eigendom-Huurder Waarborg Euro],
										[Eigendom-Huurder Verrekend],
										[Eigendom-Huurder Datum Waarborg],
										[Eigendom-Huurder Saldo],
										[Eigendom-Huurder Saldo Euro],
										[Eigendom-Huurder Aantal boetes],
										[Eigendom-Huurder Boete],
										[Eigendom-Huurder Boete Euro],
										[Eigendom-Huurder Syndic],
										[Eigendom-Huurder Syndic Euro],
										[Eigendom-Huurder BTW],
										[Eigendom-Huurder Basisindex],
										[Eigendom-Huurder Indexverrekening],
										[Eigendom-Huurder Index verrekend],
										[Eigendom-Huurder Opmerking],
										[Eigendom-Huurder Afgehandeld]
								FROM	[Eigendom-Huurder]
								WHERE	[Eigendom-Huurder-ID] = @Eigendom_Huurder_ID;";

				Eigendom_Huurder eigendom_Huurder = new Eigendom_Huurder();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigendom_Huurder_ID", OleDbType.Integer).Value = eigendom_Huurder_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							eigendom_Huurder.Eigendom_Huurder_ID = Convert.ToInt32(reader["Eigendom-Huurder-ID"]);
							eigendom_Huurder.Eigendom_Huurder_Advokaat_ID = Convert.ToInt32(reader["Eigendom-Huurder Advokaat-ID"]);
							eigendom_Huurder.Eigendom_Huurder_Eigendom_ID = Convert.ToInt32(reader["Eigendom-Huurder Eigendom-ID"]);
							eigendom_Huurder.Eigendom_Huurder_Waarborg_ID = Convert.ToInt32(reader["Eigendom-Huurder Waarborg-ID"]);
							eigendom_Huurder.Eigendom_Huurder_Huurder_ID = Convert.ToInt32(reader["Eigendom-Huurder Huurder-ID"]);
							eigendom_Huurder.Eigendom_Huurder_Soort_contract_ID = reader["Eigendom-Huurder Soort contract-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Soort contract-ID"]);
							eigendom_Huurder.Eigendom_Huurder_Brandverzekering = Convert.ToBoolean(reader["Eigendom-Huurder Brandverzekering"]);
							eigendom_Huurder.Eigendom_Huurder_Begin_huur = reader["Eigendom-Huurder Begin huur"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom-Huurder Begin huur"]);
							eigendom_Huurder.Eigendom_Huurder_Einde_huur = reader["Eigendom-Huurder Einde huur"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom-Huurder Einde huur"]);
							eigendom_Huurder.Eigendom_Huurder_Basishuur = reader["Eigendom-Huurder Basishuur"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Basishuur"]);
							eigendom_Huurder.Eigendom_Huurder_Basishuur_Euro = reader["Eigendom-Huurder Basishuur Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Basishuur Euro"]);
							eigendom_Huurder.Eigendom_Huurder_Betalen_tegen = reader["Eigendom-Huurder Betalen tegen"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Eigendom-Huurder Betalen tegen"]);
							eigendom_Huurder.Eigendom_Huurder_Huur = reader["Eigendom-Huurder Huur"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Huur"]);
							eigendom_Huurder.Eigendom_Huurder_Huur_Euro = reader["Eigendom-Huurder Huur Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Huur Euro"]);
							eigendom_Huurder.Eigendom_Huurder_Waarborg = reader["Eigendom-Huurder Waarborg"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Waarborg"]);
							eigendom_Huurder.Eigendom_Huurder_Waarborg_Euro = reader["Eigendom-Huurder Waarborg Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Waarborg Euro"]);
							eigendom_Huurder.Eigendom_Huurder_Verrekend = Convert.ToBoolean(reader["Eigendom-Huurder Verrekend"]);
							eigendom_Huurder.Eigendom_Huurder_Datum_Waarborg = reader["Eigendom-Huurder Datum Waarborg"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom-Huurder Datum Waarborg"]);
							eigendom_Huurder.Eigendom_Huurder_Saldo = reader["Eigendom-Huurder Saldo"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Saldo"]);
							eigendom_Huurder.Eigendom_Huurder_Saldo_Euro = reader["Eigendom-Huurder Saldo Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Saldo Euro"]);
							eigendom_Huurder.Eigendom_Huurder_Aantal_boetes = reader["Eigendom-Huurder Aantal boetes"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Aantal boetes"]);
							eigendom_Huurder.Eigendom_Huurder_Boete = reader["Eigendom-Huurder Boete"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Boete"]);
							eigendom_Huurder.Eigendom_Huurder_Boete_Euro = reader["Eigendom-Huurder Boete Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Boete Euro"]);
							eigendom_Huurder.Eigendom_Huurder_Syndic = reader["Eigendom-Huurder Syndic"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder Syndic"]);
							eigendom_Huurder.Eigendom_Huurder_Syndic_Euro = reader["Eigendom-Huurder Syndic Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Syndic Euro"]);
							eigendom_Huurder.Eigendom_Huurder_BTW = reader["Eigendom-Huurder BTW"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom-Huurder BTW"]);
							eigendom_Huurder.Eigendom_Huurder_Basisindex = reader["Eigendom-Huurder Basisindex"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom-Huurder Basisindex"]);
							eigendom_Huurder.Eigendom_Huurder_Indexverrekening = reader["Eigendom-Huurder Indexverrekening"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Eigendom-Huurder Indexverrekening"]);
							eigendom_Huurder.Eigendom_Huurder_Index_verrekend = Convert.ToBoolean(reader["Eigendom-Huurder Index verrekend"]);
							eigendom_Huurder.Eigendom_Huurder_Opmerking = reader["Eigendom-Huurder Opmerking"] == DBNull.Value ? null : reader["Eigendom-Huurder Opmerking"].ToString();
							eigendom_Huurder.Eigendom_Huurder_Afgehandeld = Convert.ToBoolean(reader["Eigendom-Huurder Afgehandeld"]);
						}
					}
				}

				return eigendom_Huurder;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Eigendom-Huurder-ID],
										[Eigendom-Huurder Advokaat-ID],
										[Eigendom-Huurder Eigendom-ID],
										[Eigendom-Huurder Waarborg-ID],
										[Eigendom-Huurder Huurder-ID],
										[Eigendom-Huurder Soort contract-ID],
										[Eigendom-Huurder Brandverzekering],
										[Eigendom-Huurder Begin huur],
										[Eigendom-Huurder Einde huur],
										[Eigendom-Huurder Basishuur],
										[Eigendom-Huurder Basishuur Euro],
										[Eigendom-Huurder Betalen tegen],
										[Eigendom-Huurder Huur],
										[Eigendom-Huurder Huur Euro],
										[Eigendom-Huurder Waarborg],
										[Eigendom-Huurder Waarborg Euro],
										[Eigendom-Huurder Verrekend],
										[Eigendom-Huurder Datum Waarborg],
										[Eigendom-Huurder Saldo],
										[Eigendom-Huurder Saldo Euro],
										[Eigendom-Huurder Aantal boetes],
										[Eigendom-Huurder Boete],
										[Eigendom-Huurder Boete Euro],
										[Eigendom-Huurder Syndic],
										[Eigendom-Huurder Syndic Euro],
										[Eigendom-Huurder BTW],
										[Eigendom-Huurder Basisindex],
										[Eigendom-Huurder Indexverrekening],
										[Eigendom-Huurder Index verrekend],
										[Eigendom-Huurder Opmerking],
										[Eigendom-Huurder Afgehandeld]
								FROM	[Eigendom-Huurder];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}