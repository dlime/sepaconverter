using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Eigenaar
	{
		#region Properties
		public int Eigenaar_ID { get; set; }
		public int? Eigenaar_Gemeente_ID { get; set; }
		public int? Eigenaar_Land_ID { get; set; }
		public int? Eigenaar_Eigendom_ID { get; set; }
		public short Eigenaar_Klantnummer { get; set; }
		public string Eigenaar_Naam { get; set; }
		public string Eigenaar_Straat { get; set; }
		public string Eigenaar_Busnummer { get; set; }
		public byte? Eigenaar_Aanspreking { get; set; }
		public byte? Eigenaar_Taal { get; set; }
		public string Eigenaar_Telefoon_1 { get; set; }
		public string Eigenaar_Telefoon_2 { get; set; }
		public string Eigenaar_Fax { get; set; }
		public string Eigenaar_Rekeningnummer { get; set; }
		public string Eigenaar_BTW_nummer { get; set; }
		public bool Eigenaar_Export { get; set; }
		public string Eigenaar_Memo { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Eigenaar]
								(
									[Eigenaar Gemeente-ID],
									[Eigenaar Land-ID],
									[Eigenaar Eigendom-ID],
									[Eigenaar Klantnummer],
									[Eigenaar Naam],
									[Eigenaar Straat],
									[Eigenaar Busnummer],
									[Eigenaar Aanspreking],
									[Eigenaar Taal],
									[Eigenaar Telefoon 1],
									[Eigenaar Telefoon 2],
									[Eigenaar Fax],
									[Eigenaar Rekeningnummer],
									[Eigenaar BTW-nummer],
									[Eigenaar Export],
									[Eigenaar Memo]
								)
								VALUES
								(
									@Eigenaar_Gemeente_ID,
									@Eigenaar_Land_ID,
									@Eigenaar_Eigendom_ID,
									@Eigenaar_Klantnummer,
									@Eigenaar_Naam,
									@Eigenaar_Straat,
									@Eigenaar_Busnummer,
									@Eigenaar_Aanspreking,
									@Eigenaar_Taal,
									@Eigenaar_Telefoon_1,
									@Eigenaar_Telefoon_2,
									@Eigenaar_Fax,
									@Eigenaar_Rekeningnummer,
									@Eigenaar_BTW_nummer,
									@Eigenaar_Export,
									@Eigenaar_Memo
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigenaar_Gemeente_ID", OleDbType.Integer).Value = Eigenaar_Gemeente_ID == null ? (Object)DBNull.Value : Eigenaar_Gemeente_ID;
					cmd.Parameters.Add("@Eigenaar_Land_ID", OleDbType.Integer).Value = Eigenaar_Land_ID == null ? (Object)DBNull.Value : Eigenaar_Land_ID;
					cmd.Parameters.Add("@Eigenaar_Eigendom_ID", OleDbType.Integer).Value = Eigenaar_Eigendom_ID == null ? (Object)DBNull.Value : Eigenaar_Eigendom_ID;
					cmd.Parameters.Add("@Eigenaar_Klantnummer", OleDbType.SmallInt).Value = Eigenaar_Klantnummer;
					cmd.Parameters.Add("@Eigenaar_Naam", OleDbType.WChar).Value = Eigenaar_Naam;
					cmd.Parameters.Add("@Eigenaar_Straat", OleDbType.WChar).Value = Eigenaar_Straat == null ? (Object)DBNull.Value : Eigenaar_Straat;
					cmd.Parameters.Add("@Eigenaar_Busnummer", OleDbType.WChar).Value = Eigenaar_Busnummer == null ? (Object)DBNull.Value : Eigenaar_Busnummer;
					cmd.Parameters.Add("@Eigenaar_Aanspreking", OleDbType.UnsignedTinyInt).Value = Eigenaar_Aanspreking == null ? (Object)DBNull.Value : Eigenaar_Aanspreking;
					cmd.Parameters.Add("@Eigenaar_Taal", OleDbType.UnsignedTinyInt).Value = Eigenaar_Taal == null ? (Object)DBNull.Value : Eigenaar_Taal;
					cmd.Parameters.Add("@Eigenaar_Telefoon_1", OleDbType.WChar).Value = Eigenaar_Telefoon_1 == null ? (Object)DBNull.Value : Eigenaar_Telefoon_1;
					cmd.Parameters.Add("@Eigenaar_Telefoon_2", OleDbType.WChar).Value = Eigenaar_Telefoon_2 == null ? (Object)DBNull.Value : Eigenaar_Telefoon_2;
					cmd.Parameters.Add("@Eigenaar_Fax", OleDbType.WChar).Value = Eigenaar_Fax == null ? (Object)DBNull.Value : Eigenaar_Fax;
					cmd.Parameters.Add("@Eigenaar_Rekeningnummer", OleDbType.WChar).Value = Eigenaar_Rekeningnummer == null ? (Object)DBNull.Value : Eigenaar_Rekeningnummer;
					cmd.Parameters.Add("@Eigenaar_BTW_nummer", OleDbType.WChar).Value = Eigenaar_BTW_nummer == null ? (Object)DBNull.Value : Eigenaar_BTW_nummer;
					cmd.Parameters.Add("@Eigenaar_Export", OleDbType.Boolean).Value = Eigenaar_Export;
					cmd.Parameters.Add("@Eigenaar_Memo", OleDbType.WChar).Value = Eigenaar_Memo == null ? (Object)DBNull.Value : Eigenaar_Memo;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Eigenaar_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Eigenaar]
								SET		[Eigenaar Gemeente-ID] = @Eigenaar_Gemeente_ID,
										[Eigenaar Land-ID] = @Eigenaar_Land_ID,
										[Eigenaar Eigendom-ID] = @Eigenaar_Eigendom_ID,
										[Eigenaar Klantnummer] = @Eigenaar_Klantnummer,
										[Eigenaar Naam] = @Eigenaar_Naam,
										[Eigenaar Straat] = @Eigenaar_Straat,
										[Eigenaar Busnummer] = @Eigenaar_Busnummer,
										[Eigenaar Aanspreking] = @Eigenaar_Aanspreking,
										[Eigenaar Taal] = @Eigenaar_Taal,
										[Eigenaar Telefoon 1] = @Eigenaar_Telefoon_1,
										[Eigenaar Telefoon 2] = @Eigenaar_Telefoon_2,
										[Eigenaar Fax] = @Eigenaar_Fax,
										[Eigenaar Rekeningnummer] = @Eigenaar_Rekeningnummer,
										[Eigenaar BTW-nummer] = @Eigenaar_BTW_nummer,
										[Eigenaar Export] = @Eigenaar_Export,
										[Eigenaar Memo] = @Eigenaar_Memo
								WHERE	[Eigenaar-ID] = @Eigenaar_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigenaar_Gemeente_ID", OleDbType.Integer).Value = Eigenaar_Gemeente_ID == null ? (Object)DBNull.Value : Eigenaar_Gemeente_ID;
					cmd.Parameters.Add("@Eigenaar_Land_ID", OleDbType.Integer).Value = Eigenaar_Land_ID == null ? (Object)DBNull.Value : Eigenaar_Land_ID;
					cmd.Parameters.Add("@Eigenaar_Eigendom_ID", OleDbType.Integer).Value = Eigenaar_Eigendom_ID == null ? (Object)DBNull.Value : Eigenaar_Eigendom_ID;
					cmd.Parameters.Add("@Eigenaar_Klantnummer", OleDbType.SmallInt).Value = Eigenaar_Klantnummer;
					cmd.Parameters.Add("@Eigenaar_Naam", OleDbType.WChar).Value = Eigenaar_Naam;
					cmd.Parameters.Add("@Eigenaar_Straat", OleDbType.WChar).Value = Eigenaar_Straat == null ? (Object)DBNull.Value : Eigenaar_Straat;
					cmd.Parameters.Add("@Eigenaar_Busnummer", OleDbType.WChar).Value = Eigenaar_Busnummer == null ? (Object)DBNull.Value : Eigenaar_Busnummer;
					cmd.Parameters.Add("@Eigenaar_Aanspreking", OleDbType.UnsignedTinyInt).Value = Eigenaar_Aanspreking == null ? (Object)DBNull.Value : Eigenaar_Aanspreking;
					cmd.Parameters.Add("@Eigenaar_Taal", OleDbType.UnsignedTinyInt).Value = Eigenaar_Taal == null ? (Object)DBNull.Value : Eigenaar_Taal;
					cmd.Parameters.Add("@Eigenaar_Telefoon_1", OleDbType.WChar).Value = Eigenaar_Telefoon_1 == null ? (Object)DBNull.Value : Eigenaar_Telefoon_1;
					cmd.Parameters.Add("@Eigenaar_Telefoon_2", OleDbType.WChar).Value = Eigenaar_Telefoon_2 == null ? (Object)DBNull.Value : Eigenaar_Telefoon_2;
					cmd.Parameters.Add("@Eigenaar_Fax", OleDbType.WChar).Value = Eigenaar_Fax == null ? (Object)DBNull.Value : Eigenaar_Fax;
					cmd.Parameters.Add("@Eigenaar_Rekeningnummer", OleDbType.WChar).Value = Eigenaar_Rekeningnummer == null ? (Object)DBNull.Value : Eigenaar_Rekeningnummer;
					cmd.Parameters.Add("@Eigenaar_BTW_nummer", OleDbType.WChar).Value = Eigenaar_BTW_nummer == null ? (Object)DBNull.Value : Eigenaar_BTW_nummer;
					cmd.Parameters.Add("@Eigenaar_Export", OleDbType.Boolean).Value = Eigenaar_Export;
					cmd.Parameters.Add("@Eigenaar_Memo", OleDbType.WChar).Value = Eigenaar_Memo == null ? (Object)DBNull.Value : Eigenaar_Memo;
					cmd.Parameters.Add("@Eigenaar_ID", OleDbType.Integer).Value = Eigenaar_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int eigenaar_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Eigenaar]
								WHERE	[Eigenaar-ID] = @Eigenaar_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigenaar_ID", OleDbType.Integer).Value = eigenaar_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Eigenaar Get(int eigenaar_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Eigenaar-ID],
										[Eigenaar Gemeente-ID],
										[Eigenaar Land-ID],
										[Eigenaar Eigendom-ID],
										[Eigenaar Klantnummer],
										[Eigenaar Naam],
										[Eigenaar Straat],
										[Eigenaar Busnummer],
										[Eigenaar Aanspreking],
										[Eigenaar Taal],
										[Eigenaar Telefoon 1],
										[Eigenaar Telefoon 2],
										[Eigenaar Fax],
										[Eigenaar Rekeningnummer],
										[Eigenaar BTW-nummer],
										[Eigenaar Export],
										[Eigenaar Memo]
								FROM	[Eigenaar]
								WHERE	[Eigenaar-ID] = @Eigenaar_ID;";

				Eigenaar eigenaar = new Eigenaar();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigenaar_ID", OleDbType.Integer).Value = eigenaar_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							eigenaar.Eigenaar_ID = Convert.ToInt32(reader["Eigenaar-ID"]);
							eigenaar.Eigenaar_Gemeente_ID = reader["Eigenaar Gemeente-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigenaar Gemeente-ID"]);
							eigenaar.Eigenaar_Land_ID = reader["Eigenaar Land-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigenaar Land-ID"]);
							eigenaar.Eigenaar_Eigendom_ID = reader["Eigenaar Eigendom-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigenaar Eigendom-ID"]);
							eigenaar.Eigenaar_Klantnummer = Convert.ToInt16(reader["Eigenaar Klantnummer"]);
							eigenaar.Eigenaar_Naam = reader["Eigenaar Naam"].ToString();
							eigenaar.Eigenaar_Straat = reader["Eigenaar Straat"] == DBNull.Value ? null : reader["Eigenaar Straat"].ToString();
							eigenaar.Eigenaar_Busnummer = reader["Eigenaar Busnummer"] == DBNull.Value ? null : reader["Eigenaar Busnummer"].ToString();
							eigenaar.Eigenaar_Aanspreking = reader["Eigenaar Aanspreking"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Eigenaar Aanspreking"]);
							eigenaar.Eigenaar_Taal = reader["Eigenaar Taal"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Eigenaar Taal"]);
							eigenaar.Eigenaar_Telefoon_1 = reader["Eigenaar Telefoon 1"] == DBNull.Value ? null : reader["Eigenaar Telefoon 1"].ToString();
							eigenaar.Eigenaar_Telefoon_2 = reader["Eigenaar Telefoon 2"] == DBNull.Value ? null : reader["Eigenaar Telefoon 2"].ToString();
							eigenaar.Eigenaar_Fax = reader["Eigenaar Fax"] == DBNull.Value ? null : reader["Eigenaar Fax"].ToString();
							eigenaar.Eigenaar_Rekeningnummer = reader["Eigenaar Rekeningnummer"] == DBNull.Value ? null : reader["Eigenaar Rekeningnummer"].ToString();
							eigenaar.Eigenaar_BTW_nummer = reader["Eigenaar BTW-nummer"] == DBNull.Value ? null : reader["Eigenaar BTW-nummer"].ToString();
							eigenaar.Eigenaar_Export = Convert.ToBoolean(reader["Eigenaar Export"]);
							eigenaar.Eigenaar_Memo = reader["Eigenaar Memo"] == DBNull.Value ? null : reader["Eigenaar Memo"].ToString();
						}
					}
				}

				return eigenaar;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Eigenaar-ID],
										[Eigenaar Gemeente-ID],
										[Eigenaar Land-ID],
										[Eigenaar Eigendom-ID],
										[Eigenaar Klantnummer],
										[Eigenaar Naam],
										[Eigenaar Straat],
										[Eigenaar Busnummer],
										[Eigenaar Aanspreking],
										[Eigenaar Taal],
										[Eigenaar Telefoon 1],
										[Eigenaar Telefoon 2],
										[Eigenaar Fax],
										[Eigenaar Rekeningnummer],
										[Eigenaar BTW-nummer],
										[Eigenaar Export],
										[Eigenaar Memo]
								FROM	[Eigenaar];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}