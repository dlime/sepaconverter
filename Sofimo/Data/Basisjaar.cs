using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Basisjaar
	{
		#region Properties
		public int Basisjaar_ID { get; set; }
		public DateTime Basisjaar_Maand { get; set; }
		public double? Basisjaar_Coëfficiënt { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Basisjaar]
								(
									[Basisjaar Maand],
									[Basisjaar Coëfficiënt]
								)
								VALUES
								(
									@Basisjaar_Maand,
									@Basisjaar_Coëfficiënt
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Basisjaar_Maand", OleDbType.Date).Value = Basisjaar_Maand;
					cmd.Parameters.Add("@Basisjaar_Coëfficiënt", OleDbType.Double).Value = Basisjaar_Coëfficiënt == null ? (Object)DBNull.Value : Basisjaar_Coëfficiënt;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Basisjaar_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Basisjaar]
								SET		[Basisjaar Maand] = @Basisjaar_Maand,
										[Basisjaar Coëfficiënt] = @Basisjaar_Coëfficiënt
								WHERE	[Basisjaar-ID] = @Basisjaar_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Basisjaar_Maand", OleDbType.Date).Value = Basisjaar_Maand;
					cmd.Parameters.Add("@Basisjaar_Coëfficiënt", OleDbType.Double).Value = Basisjaar_Coëfficiënt == null ? (Object)DBNull.Value : Basisjaar_Coëfficiënt;
					cmd.Parameters.Add("@Basisjaar_ID", OleDbType.Integer).Value = Basisjaar_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int basisjaar_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Basisjaar]
								WHERE	[Basisjaar-ID] = @Basisjaar_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Basisjaar_ID", OleDbType.Integer).Value = basisjaar_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Basisjaar Get(int basisjaar_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Basisjaar-ID],
										[Basisjaar Maand],
										[Basisjaar Coëfficiënt]
								FROM	[Basisjaar]
								WHERE	[Basisjaar-ID] = @Basisjaar_ID;";

				Basisjaar basisjaar = new Basisjaar();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Basisjaar_ID", OleDbType.Integer).Value = basisjaar_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							basisjaar.Basisjaar_ID = Convert.ToInt32(reader["Basisjaar-ID"]);
							basisjaar.Basisjaar_Maand = Convert.ToDateTime(reader["Basisjaar Maand"]);
							basisjaar.Basisjaar_Coëfficiënt = reader["Basisjaar Coëfficiënt"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Basisjaar Coëfficiënt"]);
						}
					}
				}

				return basisjaar;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Basisjaar-ID],
										[Basisjaar Maand],
										[Basisjaar Coëfficiënt]
								FROM	[Basisjaar];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}