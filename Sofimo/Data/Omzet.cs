using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Omzet
	{
		#region Properties
		public int Omzet_ID { get; set; }
		public string Omzet_Makelaar_Code { get; set; }
		public int? Omzet_Bedrag { get; set; }
		public double? Omzet_Bedrag_Euro { get; set; }
		public string Omzet_Naam { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Omzet]
								(
									[Omzet Makelaar Code],
									[Omzet Bedrag],
									[Omzet Bedrag Euro],
									[Omzet Naam]
								)
								VALUES
								(
									@Omzet_Makelaar_Code,
									@Omzet_Bedrag,
									@Omzet_Bedrag_Euro,
									@Omzet_Naam
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Omzet_Makelaar_Code", OleDbType.WChar).Value = Omzet_Makelaar_Code == null ? (Object)DBNull.Value : Omzet_Makelaar_Code;
					cmd.Parameters.Add("@Omzet_Bedrag", OleDbType.Integer).Value = Omzet_Bedrag == null ? (Object)DBNull.Value : Omzet_Bedrag;
					cmd.Parameters.Add("@Omzet_Bedrag_Euro", OleDbType.Double).Value = Omzet_Bedrag_Euro == null ? (Object)DBNull.Value : Omzet_Bedrag_Euro;
					cmd.Parameters.Add("@Omzet_Naam", OleDbType.WChar).Value = Omzet_Naam == null ? (Object)DBNull.Value : Omzet_Naam;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Omzet_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Omzet]
								SET		[Omzet Makelaar Code] = @Omzet_Makelaar_Code,
										[Omzet Bedrag] = @Omzet_Bedrag,
										[Omzet Bedrag Euro] = @Omzet_Bedrag_Euro,
										[Omzet Naam] = @Omzet_Naam
								WHERE	[Omzet-ID] = @Omzet_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Omzet_Makelaar_Code", OleDbType.WChar).Value = Omzet_Makelaar_Code == null ? (Object)DBNull.Value : Omzet_Makelaar_Code;
					cmd.Parameters.Add("@Omzet_Bedrag", OleDbType.Integer).Value = Omzet_Bedrag == null ? (Object)DBNull.Value : Omzet_Bedrag;
					cmd.Parameters.Add("@Omzet_Bedrag_Euro", OleDbType.Double).Value = Omzet_Bedrag_Euro == null ? (Object)DBNull.Value : Omzet_Bedrag_Euro;
					cmd.Parameters.Add("@Omzet_Naam", OleDbType.WChar).Value = Omzet_Naam == null ? (Object)DBNull.Value : Omzet_Naam;
					cmd.Parameters.Add("@Omzet_ID", OleDbType.Integer).Value = Omzet_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int omzet_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Omzet]
								WHERE	[Omzet-ID] = @Omzet_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Omzet_ID", OleDbType.Integer).Value = omzet_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Omzet Get(int omzet_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Omzet-ID],
										[Omzet Makelaar Code],
										[Omzet Bedrag],
										[Omzet Bedrag Euro],
										[Omzet Naam]
								FROM	[Omzet]
								WHERE	[Omzet-ID] = @Omzet_ID;";

				Omzet omzet = new Omzet();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Omzet_ID", OleDbType.Integer).Value = omzet_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							omzet.Omzet_ID = Convert.ToInt32(reader["Omzet-ID"]);
							omzet.Omzet_Makelaar_Code = reader["Omzet Makelaar Code"] == DBNull.Value ? null : reader["Omzet Makelaar Code"].ToString();
							omzet.Omzet_Bedrag = reader["Omzet Bedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Omzet Bedrag"]);
							omzet.Omzet_Bedrag_Euro = reader["Omzet Bedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Omzet Bedrag Euro"]);
							omzet.Omzet_Naam = reader["Omzet Naam"] == DBNull.Value ? null : reader["Omzet Naam"].ToString();
						}
					}
				}

				return omzet;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Omzet-ID],
										[Omzet Makelaar Code],
										[Omzet Bedrag],
										[Omzet Bedrag Euro],
										[Omzet Naam]
								FROM	[Omzet];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}