using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Eigendom
	{
		#region Properties
		public int Eigendom_ID { get; set; }
		public int Eigendom_Firma_ID { get; set; }
		public int? Eigendom_Gemeente_ID { get; set; }
		public int? Eigendom_Land_ID { get; set; }
		public int? Eigendom_Eigenaar_ID { get; set; }
		public int? Eigendom_Huurder_ID { get; set; }
		public string Eigendom_Wat { get; set; }
		public string Eigendom_Toelichting { get; set; }
		public string Eigendom_Straat { get; set; }
		public string Eigendom_Busnummer { get; set; }
		public double? Eigendom_Commissie { get; set; }
		public bool Eigendom_Enkel_huur { get; set; }
		public bool Eigendom_Gratis { get; set; }
		public DateTime? Eigendom_Gratis_tot { get; set; }
		public DateTime? Eigendom_Begin_inning { get; set; }
		public DateTime? Eigendom_Einde_inning { get; set; }
		public byte? Eigendom_Doorbetalen { get; set; }
		public bool Eigendom_Commissie_Factureren { get; set; }
		public bool Eigendom_Huur___BTW { get; set; }
		public bool Eigendom_Faktuur { get; set; }
		public DateTime? Eigendom_Datum_telleropname { get; set; }
		public string Eigendom_Electriciteit_dag_meter { get; set; }
		public int? Eigendom_Electriciteit_dag_stand { get; set; }
		public string Eigendom_Electriciteit_nacht_meter { get; set; }
		public int? Eigendom_Electriciteit_nacht_stand { get; set; }
		public string Eigendom_Gas_meter { get; set; }
		public int? Eigendom_Gas_stand { get; set; }
		public string Eigendom_Water_meter { get; set; }
		public int? Eigendom_Water_stand { get; set; }
		public string Eigendom_Memo { get; set; }
		public string Eigendom_Memo_verlies { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Eigendom]
								(
									[Eigendom Gemeente-ID],
									[Eigendom Land-ID],
									[Eigendom Eigenaar-ID],
									[Eigendom Huurder-ID],
									[Eigendom Wat],
									[Eigendom Toelichting],
									[Eigendom Straat],
									[Eigendom Busnummer],
									[Eigendom Commissie],
									[Eigendom Enkel huur],
									[Eigendom Gratis],
									[Eigendom Gratis tot],
									[Eigendom Begin inning],
									[Eigendom Einde inning],
									[Eigendom Doorbetalen],
									[Eigendom Commissie Factureren],
									[Eigendom Huur + BTW],
									[Eigendom Faktuur],
									[Eigendom Datum telleropname],
									[Eigendom Electriciteit dag meter],
									[Eigendom Electriciteit dag stand],
									[Eigendom Electriciteit nacht meter],
									[Eigendom Electriciteit nacht stand],
									[Eigendom Gas meter],
									[Eigendom Gas stand],
									[Eigendom Water meter],
									[Eigendom Water stand],
									[Eigendom Memo],
									[Eigendom Memo verlies]
								)
								VALUES
								(
									@Eigendom_Gemeente_ID,
									@Eigendom_Land_ID,
									@Eigendom_Eigenaar_ID,
									@Eigendom_Huurder_ID,
									@Eigendom_Wat,
									@Eigendom_Toelichting,
									@Eigendom_Straat,
									@Eigendom_Busnummer,
									@Eigendom_Commissie,
									@Eigendom_Enkel_huur,
									@Eigendom_Gratis,
									@Eigendom_Gratis_tot,
									@Eigendom_Begin_inning,
									@Eigendom_Einde_inning,
									@Eigendom_Doorbetalen,
									@Eigendom_Commissie_Factureren,
									@Eigendom_Huur___BTW,
									@Eigendom_Faktuur,
									@Eigendom_Datum_telleropname,
									@Eigendom_Electriciteit_dag_meter,
									@Eigendom_Electriciteit_dag_stand,
									@Eigendom_Electriciteit_nacht_meter,
									@Eigendom_Electriciteit_nacht_stand,
									@Eigendom_Gas_meter,
									@Eigendom_Gas_stand,
									@Eigendom_Water_meter,
									@Eigendom_Water_stand,
									@Eigendom_Memo,
									@Eigendom_Memo_verlies
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigendom_Gemeente_ID", OleDbType.Integer).Value = Eigendom_Gemeente_ID == null ? (Object)DBNull.Value : Eigendom_Gemeente_ID;
					cmd.Parameters.Add("@Eigendom_Land_ID", OleDbType.Integer).Value = Eigendom_Land_ID == null ? (Object)DBNull.Value : Eigendom_Land_ID;
					cmd.Parameters.Add("@Eigendom_Eigenaar_ID", OleDbType.Integer).Value = Eigendom_Eigenaar_ID == null ? (Object)DBNull.Value : Eigendom_Eigenaar_ID;
					cmd.Parameters.Add("@Eigendom_Huurder_ID", OleDbType.Integer).Value = Eigendom_Huurder_ID == null ? (Object)DBNull.Value : Eigendom_Huurder_ID;
					cmd.Parameters.Add("@Eigendom_Wat", OleDbType.WChar).Value = Eigendom_Wat == null ? (Object)DBNull.Value : Eigendom_Wat;
					cmd.Parameters.Add("@Eigendom_Toelichting", OleDbType.WChar).Value = Eigendom_Toelichting == null ? (Object)DBNull.Value : Eigendom_Toelichting;
					cmd.Parameters.Add("@Eigendom_Straat", OleDbType.WChar).Value = Eigendom_Straat == null ? (Object)DBNull.Value : Eigendom_Straat;
					cmd.Parameters.Add("@Eigendom_Busnummer", OleDbType.WChar).Value = Eigendom_Busnummer == null ? (Object)DBNull.Value : Eigendom_Busnummer;
					cmd.Parameters.Add("@Eigendom_Commissie", OleDbType.Double).Value = Eigendom_Commissie == null ? (Object)DBNull.Value : Eigendom_Commissie;
					cmd.Parameters.Add("@Eigendom_Enkel_huur", OleDbType.Boolean).Value = Eigendom_Enkel_huur;
					cmd.Parameters.Add("@Eigendom_Gratis", OleDbType.Boolean).Value = Eigendom_Gratis;
					cmd.Parameters.Add("@Eigendom_Gratis_tot", OleDbType.Date).Value = Eigendom_Gratis_tot == null ? (Object)DBNull.Value : Eigendom_Gratis_tot;
					cmd.Parameters.Add("@Eigendom_Begin_inning", OleDbType.Date).Value = Eigendom_Begin_inning == null ? (Object)DBNull.Value : Eigendom_Begin_inning;
					cmd.Parameters.Add("@Eigendom_Einde_inning", OleDbType.Date).Value = Eigendom_Einde_inning == null ? (Object)DBNull.Value : Eigendom_Einde_inning;
					cmd.Parameters.Add("@Eigendom_Doorbetalen", OleDbType.UnsignedTinyInt).Value = Eigendom_Doorbetalen == null ? (Object)DBNull.Value : Eigendom_Doorbetalen;
					cmd.Parameters.Add("@Eigendom_Commissie_Factureren", OleDbType.Boolean).Value = Eigendom_Commissie_Factureren;
					cmd.Parameters.Add("@Eigendom_Huur___BTW", OleDbType.Boolean).Value = Eigendom_Huur___BTW;
					cmd.Parameters.Add("@Eigendom_Faktuur", OleDbType.Boolean).Value = Eigendom_Faktuur;
					cmd.Parameters.Add("@Eigendom_Datum_telleropname", OleDbType.Date).Value = Eigendom_Datum_telleropname == null ? (Object)DBNull.Value : Eigendom_Datum_telleropname;
					cmd.Parameters.Add("@Eigendom_Electriciteit_dag_meter", OleDbType.WChar).Value = Eigendom_Electriciteit_dag_meter == null ? (Object)DBNull.Value : Eigendom_Electriciteit_dag_meter;
					cmd.Parameters.Add("@Eigendom_Electriciteit_dag_stand", OleDbType.Integer).Value = Eigendom_Electriciteit_dag_stand == null ? (Object)DBNull.Value : Eigendom_Electriciteit_dag_stand;
					cmd.Parameters.Add("@Eigendom_Electriciteit_nacht_meter", OleDbType.WChar).Value = Eigendom_Electriciteit_nacht_meter == null ? (Object)DBNull.Value : Eigendom_Electriciteit_nacht_meter;
					cmd.Parameters.Add("@Eigendom_Electriciteit_nacht_stand", OleDbType.Integer).Value = Eigendom_Electriciteit_nacht_stand == null ? (Object)DBNull.Value : Eigendom_Electriciteit_nacht_stand;
					cmd.Parameters.Add("@Eigendom_Gas_meter", OleDbType.WChar).Value = Eigendom_Gas_meter == null ? (Object)DBNull.Value : Eigendom_Gas_meter;
					cmd.Parameters.Add("@Eigendom_Gas_stand", OleDbType.Integer).Value = Eigendom_Gas_stand == null ? (Object)DBNull.Value : Eigendom_Gas_stand;
					cmd.Parameters.Add("@Eigendom_Water_meter", OleDbType.WChar).Value = Eigendom_Water_meter == null ? (Object)DBNull.Value : Eigendom_Water_meter;
					cmd.Parameters.Add("@Eigendom_Water_stand", OleDbType.Integer).Value = Eigendom_Water_stand == null ? (Object)DBNull.Value : Eigendom_Water_stand;
					cmd.Parameters.Add("@Eigendom_Memo", OleDbType.WChar).Value = Eigendom_Memo == null ? (Object)DBNull.Value : Eigendom_Memo;
					cmd.Parameters.Add("@Eigendom_Memo_verlies", OleDbType.WChar).Value = Eigendom_Memo_verlies == null ? (Object)DBNull.Value : Eigendom_Memo_verlies;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Eigendom_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Eigendom]
								SET		[Eigendom Gemeente-ID] = @Eigendom_Gemeente_ID,
										[Eigendom Land-ID] = @Eigendom_Land_ID,
										[Eigendom Eigenaar-ID] = @Eigendom_Eigenaar_ID,
										[Eigendom Huurder-ID] = @Eigendom_Huurder_ID,
										[Eigendom Wat] = @Eigendom_Wat,
										[Eigendom Toelichting] = @Eigendom_Toelichting,
										[Eigendom Straat] = @Eigendom_Straat,
										[Eigendom Busnummer] = @Eigendom_Busnummer,
										[Eigendom Commissie] = @Eigendom_Commissie,
										[Eigendom Enkel huur] = @Eigendom_Enkel_huur,
										[Eigendom Gratis] = @Eigendom_Gratis,
										[Eigendom Gratis tot] = @Eigendom_Gratis_tot,
										[Eigendom Begin inning] = @Eigendom_Begin_inning,
										[Eigendom Einde inning] = @Eigendom_Einde_inning,
										[Eigendom Doorbetalen] = @Eigendom_Doorbetalen,
										[Eigendom Commissie Factureren] = @Eigendom_Commissie_Factureren,
										[Eigendom Huur + BTW] = @Eigendom_Huur___BTW,
										[Eigendom Faktuur] = @Eigendom_Faktuur,
										[Eigendom Datum telleropname] = @Eigendom_Datum_telleropname,
										[Eigendom Electriciteit dag meter] = @Eigendom_Electriciteit_dag_meter,
										[Eigendom Electriciteit dag stand] = @Eigendom_Electriciteit_dag_stand,
										[Eigendom Electriciteit nacht meter] = @Eigendom_Electriciteit_nacht_meter,
										[Eigendom Electriciteit nacht stand] = @Eigendom_Electriciteit_nacht_stand,
										[Eigendom Gas meter] = @Eigendom_Gas_meter,
										[Eigendom Gas stand] = @Eigendom_Gas_stand,
										[Eigendom Water meter] = @Eigendom_Water_meter,
										[Eigendom Water stand] = @Eigendom_Water_stand,
										[Eigendom Memo] = @Eigendom_Memo,
										[Eigendom Memo verlies] = @Eigendom_Memo_verlies
								WHERE	[Eigendom-ID] = @Eigendom_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigendom_Firma_ID", OleDbType.Integer).Value = Eigendom_Firma_ID;
					cmd.Parameters.Add("@Eigendom_Gemeente_ID", OleDbType.Integer).Value = Eigendom_Gemeente_ID == null ? (Object)DBNull.Value : Eigendom_Gemeente_ID;
					cmd.Parameters.Add("@Eigendom_Land_ID", OleDbType.Integer).Value = Eigendom_Land_ID == null ? (Object)DBNull.Value : Eigendom_Land_ID;
					cmd.Parameters.Add("@Eigendom_Eigenaar_ID", OleDbType.Integer).Value = Eigendom_Eigenaar_ID == null ? (Object)DBNull.Value : Eigendom_Eigenaar_ID;
					cmd.Parameters.Add("@Eigendom_Huurder_ID", OleDbType.Integer).Value = Eigendom_Huurder_ID == null ? (Object)DBNull.Value : Eigendom_Huurder_ID;
					cmd.Parameters.Add("@Eigendom_Wat", OleDbType.WChar).Value = Eigendom_Wat == null ? (Object)DBNull.Value : Eigendom_Wat;
					cmd.Parameters.Add("@Eigendom_Toelichting", OleDbType.WChar).Value = Eigendom_Toelichting == null ? (Object)DBNull.Value : Eigendom_Toelichting;
					cmd.Parameters.Add("@Eigendom_Straat", OleDbType.WChar).Value = Eigendom_Straat == null ? (Object)DBNull.Value : Eigendom_Straat;
					cmd.Parameters.Add("@Eigendom_Busnummer", OleDbType.WChar).Value = Eigendom_Busnummer == null ? (Object)DBNull.Value : Eigendom_Busnummer;
					cmd.Parameters.Add("@Eigendom_Commissie", OleDbType.Double).Value = Eigendom_Commissie == null ? (Object)DBNull.Value : Eigendom_Commissie;
					cmd.Parameters.Add("@Eigendom_Enkel_huur", OleDbType.Boolean).Value = Eigendom_Enkel_huur;
					cmd.Parameters.Add("@Eigendom_Gratis", OleDbType.Boolean).Value = Eigendom_Gratis;
					cmd.Parameters.Add("@Eigendom_Gratis_tot", OleDbType.Date).Value = Eigendom_Gratis_tot == null ? (Object)DBNull.Value : Eigendom_Gratis_tot;
					cmd.Parameters.Add("@Eigendom_Begin_inning", OleDbType.Date).Value = Eigendom_Begin_inning == null ? (Object)DBNull.Value : Eigendom_Begin_inning;
					cmd.Parameters.Add("@Eigendom_Einde_inning", OleDbType.Date).Value = Eigendom_Einde_inning == null ? (Object)DBNull.Value : Eigendom_Einde_inning;
					cmd.Parameters.Add("@Eigendom_Doorbetalen", OleDbType.UnsignedTinyInt).Value = Eigendom_Doorbetalen == null ? (Object)DBNull.Value : Eigendom_Doorbetalen;
					cmd.Parameters.Add("@Eigendom_Commissie_Factureren", OleDbType.Boolean).Value = Eigendom_Commissie_Factureren;
					cmd.Parameters.Add("@Eigendom_Huur___BTW", OleDbType.Boolean).Value = Eigendom_Huur___BTW;
					cmd.Parameters.Add("@Eigendom_Faktuur", OleDbType.Boolean).Value = Eigendom_Faktuur;
					cmd.Parameters.Add("@Eigendom_Datum_telleropname", OleDbType.Date).Value = Eigendom_Datum_telleropname == null ? (Object)DBNull.Value : Eigendom_Datum_telleropname;
					cmd.Parameters.Add("@Eigendom_Electriciteit_dag_meter", OleDbType.WChar).Value = Eigendom_Electriciteit_dag_meter == null ? (Object)DBNull.Value : Eigendom_Electriciteit_dag_meter;
					cmd.Parameters.Add("@Eigendom_Electriciteit_dag_stand", OleDbType.Integer).Value = Eigendom_Electriciteit_dag_stand == null ? (Object)DBNull.Value : Eigendom_Electriciteit_dag_stand;
					cmd.Parameters.Add("@Eigendom_Electriciteit_nacht_meter", OleDbType.WChar).Value = Eigendom_Electriciteit_nacht_meter == null ? (Object)DBNull.Value : Eigendom_Electriciteit_nacht_meter;
					cmd.Parameters.Add("@Eigendom_Electriciteit_nacht_stand", OleDbType.Integer).Value = Eigendom_Electriciteit_nacht_stand == null ? (Object)DBNull.Value : Eigendom_Electriciteit_nacht_stand;
					cmd.Parameters.Add("@Eigendom_Gas_meter", OleDbType.WChar).Value = Eigendom_Gas_meter == null ? (Object)DBNull.Value : Eigendom_Gas_meter;
					cmd.Parameters.Add("@Eigendom_Gas_stand", OleDbType.Integer).Value = Eigendom_Gas_stand == null ? (Object)DBNull.Value : Eigendom_Gas_stand;
					cmd.Parameters.Add("@Eigendom_Water_meter", OleDbType.WChar).Value = Eigendom_Water_meter == null ? (Object)DBNull.Value : Eigendom_Water_meter;
					cmd.Parameters.Add("@Eigendom_Water_stand", OleDbType.Integer).Value = Eigendom_Water_stand == null ? (Object)DBNull.Value : Eigendom_Water_stand;
					cmd.Parameters.Add("@Eigendom_Memo", OleDbType.WChar).Value = Eigendom_Memo == null ? (Object)DBNull.Value : Eigendom_Memo;
					cmd.Parameters.Add("@Eigendom_Memo_verlies", OleDbType.WChar).Value = Eigendom_Memo_verlies == null ? (Object)DBNull.Value : Eigendom_Memo_verlies;
					cmd.Parameters.Add("@Eigendom_ID", OleDbType.Integer).Value = Eigendom_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int eigendom_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Eigendom]
								WHERE	[Eigendom-ID] = @Eigendom_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigendom_ID", OleDbType.Integer).Value = eigendom_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Eigendom Get(int eigendom_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Eigendom-ID],
										[Eigendom Firma-ID],
										[Eigendom Gemeente-ID],
										[Eigendom Land-ID],
										[Eigendom Eigenaar-ID],
										[Eigendom Huurder-ID],
										[Eigendom Wat],
										[Eigendom Toelichting],
										[Eigendom Straat],
										[Eigendom Busnummer],
										[Eigendom Commissie],
										[Eigendom Enkel huur],
										[Eigendom Gratis],
										[Eigendom Gratis tot],
										[Eigendom Begin inning],
										[Eigendom Einde inning],
										[Eigendom Doorbetalen],
										[Eigendom Commissie Factureren],
										[Eigendom Huur + BTW],
										[Eigendom Faktuur],
										[Eigendom Datum telleropname],
										[Eigendom Electriciteit dag meter],
										[Eigendom Electriciteit dag stand],
										[Eigendom Electriciteit nacht meter],
										[Eigendom Electriciteit nacht stand],
										[Eigendom Gas meter],
										[Eigendom Gas stand],
										[Eigendom Water meter],
										[Eigendom Water stand],
										[Eigendom Memo],
										[Eigendom Memo verlies]
								FROM	[Eigendom]
								WHERE	[Eigendom-ID] = @Eigendom_ID;";

				Eigendom eigendom = new Eigendom();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigendom_ID", OleDbType.Integer).Value = eigendom_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							eigendom.Eigendom_ID = Convert.ToInt32(reader["Eigendom-ID"]);
							eigendom.Eigendom_Firma_ID = Convert.ToInt32(reader["Eigendom Firma-ID"]);
							eigendom.Eigendom_Gemeente_ID = reader["Eigendom Gemeente-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom Gemeente-ID"]);
							eigendom.Eigendom_Land_ID = reader["Eigendom Land-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom Land-ID"]);
							eigendom.Eigendom_Eigenaar_ID = reader["Eigendom Eigenaar-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom Eigenaar-ID"]);
							eigendom.Eigendom_Huurder_ID = reader["Eigendom Huurder-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom Huurder-ID"]);
							eigendom.Eigendom_Wat = reader["Eigendom Wat"] == DBNull.Value ? null : reader["Eigendom Wat"].ToString();
							eigendom.Eigendom_Toelichting = reader["Eigendom Toelichting"] == DBNull.Value ? null : reader["Eigendom Toelichting"].ToString();
							eigendom.Eigendom_Straat = reader["Eigendom Straat"] == DBNull.Value ? null : reader["Eigendom Straat"].ToString();
							eigendom.Eigendom_Busnummer = reader["Eigendom Busnummer"] == DBNull.Value ? null : reader["Eigendom Busnummer"].ToString();
							eigendom.Eigendom_Commissie = reader["Eigendom Commissie"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigendom Commissie"]);
							eigendom.Eigendom_Enkel_huur = Convert.ToBoolean(reader["Eigendom Enkel huur"]);
							eigendom.Eigendom_Gratis = Convert.ToBoolean(reader["Eigendom Gratis"]);
							eigendom.Eigendom_Gratis_tot = reader["Eigendom Gratis tot"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom Gratis tot"]);
							eigendom.Eigendom_Begin_inning = reader["Eigendom Begin inning"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom Begin inning"]);
							eigendom.Eigendom_Einde_inning = reader["Eigendom Einde inning"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom Einde inning"]);
							eigendom.Eigendom_Doorbetalen = reader["Eigendom Doorbetalen"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Eigendom Doorbetalen"]);
							eigendom.Eigendom_Commissie_Factureren = Convert.ToBoolean(reader["Eigendom Commissie Factureren"]);
							eigendom.Eigendom_Huur___BTW = Convert.ToBoolean(reader["Eigendom Huur + BTW"]);
							eigendom.Eigendom_Faktuur = Convert.ToBoolean(reader["Eigendom Faktuur"]);
							eigendom.Eigendom_Datum_telleropname = reader["Eigendom Datum telleropname"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigendom Datum telleropname"]);
							eigendom.Eigendom_Electriciteit_dag_meter = reader["Eigendom Electriciteit dag meter"] == DBNull.Value ? null : reader["Eigendom Electriciteit dag meter"].ToString();
							eigendom.Eigendom_Electriciteit_dag_stand = reader["Eigendom Electriciteit dag stand"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom Electriciteit dag stand"]);
							eigendom.Eigendom_Electriciteit_nacht_meter = reader["Eigendom Electriciteit nacht meter"] == DBNull.Value ? null : reader["Eigendom Electriciteit nacht meter"].ToString();
							eigendom.Eigendom_Electriciteit_nacht_stand = reader["Eigendom Electriciteit nacht stand"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom Electriciteit nacht stand"]);
							eigendom.Eigendom_Gas_meter = reader["Eigendom Gas meter"] == DBNull.Value ? null : reader["Eigendom Gas meter"].ToString();
							eigendom.Eigendom_Gas_stand = reader["Eigendom Gas stand"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom Gas stand"]);
							eigendom.Eigendom_Water_meter = reader["Eigendom Water meter"] == DBNull.Value ? null : reader["Eigendom Water meter"].ToString();
							eigendom.Eigendom_Water_stand = reader["Eigendom Water stand"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigendom Water stand"]);
							eigendom.Eigendom_Memo = reader["Eigendom Memo"] == DBNull.Value ? null : reader["Eigendom Memo"].ToString();
							eigendom.Eigendom_Memo_verlies = reader["Eigendom Memo verlies"] == DBNull.Value ? null : reader["Eigendom Memo verlies"].ToString();
						}
					}
				}

				return eigendom;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Eigendom-ID],
										[Eigendom Firma-ID],
										[Eigendom Gemeente-ID],
										[Eigendom Land-ID],
										[Eigendom Eigenaar-ID],
										[Eigendom Huurder-ID],
										[Eigendom Wat],
										[Eigendom Toelichting],
										[Eigendom Straat],
										[Eigendom Busnummer],
										[Eigendom Commissie],
										[Eigendom Enkel huur],
										[Eigendom Gratis],
										[Eigendom Gratis tot],
										[Eigendom Begin inning],
										[Eigendom Einde inning],
										[Eigendom Doorbetalen],
										[Eigendom Commissie Factureren],
										[Eigendom Huur + BTW],
										[Eigendom Faktuur],
										[Eigendom Datum telleropname],
										[Eigendom Electriciteit dag meter],
										[Eigendom Electriciteit dag stand],
										[Eigendom Electriciteit nacht meter],
										[Eigendom Electriciteit nacht stand],
										[Eigendom Gas meter],
										[Eigendom Gas stand],
										[Eigendom Water meter],
										[Eigendom Water stand],
										[Eigendom Memo],
										[Eigendom Memo verlies]
								FROM	[Eigendom];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}