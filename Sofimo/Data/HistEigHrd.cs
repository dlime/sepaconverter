using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class HistEigHrd
	{
		#region Properties
		public int HistEigHrdID { get; set; }
		public DateTime? HistEigHrdDatum { get; set; }
		public byte? HistEigHrdCode { get; set; }
		public int? HistEigHrdHuur { get; set; }
		public double? HistEigHrdHuurEuro { get; set; }
		public int? HistEigHrdEigenaar { get; set; }
		public double? HistEigHrdEigenaarEuro { get; set; }
		public int? HistEigHrdHuurder { get; set; }
		public double? HistEigHrdHuurderEuro { get; set; }
		public int? HistEigHrdBoete { get; set; }
		public double? HistEigHrdBoeteEuro { get; set; }
		public bool HistEigHrdKwijtgescholden { get; set; }
		public DateTime? HistEigHrdBetaald { get; set; }
		public string HistEigHrdCommentaar { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [HistEigHrd]
								(
									[HistEigHrdDatum],
									[HistEigHrdCode],
									[HistEigHrdHuur],
									[HistEigHrdHuurEuro],
									[HistEigHrdEigenaar],
									[HistEigHrdEigenaarEuro],
									[HistEigHrdHuurder],
									[HistEigHrdHuurderEuro],
									[HistEigHrdBoete],
									[HistEigHrdBoeteEuro],
									[HistEigHrdKwijtgescholden],
									[HistEigHrdBetaald],
									[HistEigHrdCommentaar]
								)
								VALUES
								(
									@HistEigHrdDatum,
									@HistEigHrdCode,
									@HistEigHrdHuur,
									@HistEigHrdHuurEuro,
									@HistEigHrdEigenaar,
									@HistEigHrdEigenaarEuro,
									@HistEigHrdHuurder,
									@HistEigHrdHuurderEuro,
									@HistEigHrdBoete,
									@HistEigHrdBoeteEuro,
									@HistEigHrdKwijtgescholden,
									@HistEigHrdBetaald,
									@HistEigHrdCommentaar
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@HistEigHrdDatum", OleDbType.Date).Value = HistEigHrdDatum == null ? (Object)DBNull.Value : HistEigHrdDatum;
					cmd.Parameters.Add("@HistEigHrdCode", OleDbType.UnsignedTinyInt).Value = HistEigHrdCode == null ? (Object)DBNull.Value : HistEigHrdCode;
					cmd.Parameters.Add("@HistEigHrdHuur", OleDbType.Integer).Value = HistEigHrdHuur == null ? (Object)DBNull.Value : HistEigHrdHuur;
					cmd.Parameters.Add("@HistEigHrdHuurEuro", OleDbType.Double).Value = HistEigHrdHuurEuro == null ? (Object)DBNull.Value : HistEigHrdHuurEuro;
					cmd.Parameters.Add("@HistEigHrdEigenaar", OleDbType.Integer).Value = HistEigHrdEigenaar == null ? (Object)DBNull.Value : HistEigHrdEigenaar;
					cmd.Parameters.Add("@HistEigHrdEigenaarEuro", OleDbType.Double).Value = HistEigHrdEigenaarEuro == null ? (Object)DBNull.Value : HistEigHrdEigenaarEuro;
					cmd.Parameters.Add("@HistEigHrdHuurder", OleDbType.Integer).Value = HistEigHrdHuurder == null ? (Object)DBNull.Value : HistEigHrdHuurder;
					cmd.Parameters.Add("@HistEigHrdHuurderEuro", OleDbType.Double).Value = HistEigHrdHuurderEuro == null ? (Object)DBNull.Value : HistEigHrdHuurderEuro;
					cmd.Parameters.Add("@HistEigHrdBoete", OleDbType.Integer).Value = HistEigHrdBoete == null ? (Object)DBNull.Value : HistEigHrdBoete;
					cmd.Parameters.Add("@HistEigHrdBoeteEuro", OleDbType.Double).Value = HistEigHrdBoeteEuro == null ? (Object)DBNull.Value : HistEigHrdBoeteEuro;
					cmd.Parameters.Add("@HistEigHrdKwijtgescholden", OleDbType.Boolean).Value = HistEigHrdKwijtgescholden;
					cmd.Parameters.Add("@HistEigHrdBetaald", OleDbType.Date).Value = HistEigHrdBetaald == null ? (Object)DBNull.Value : HistEigHrdBetaald;
					cmd.Parameters.Add("@HistEigHrdCommentaar", OleDbType.WChar).Value = HistEigHrdCommentaar == null ? (Object)DBNull.Value : HistEigHrdCommentaar;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					HistEigHrdID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[HistEigHrd]
								SET		[HistEigHrdDatum] = @HistEigHrdDatum,
										[HistEigHrdCode] = @HistEigHrdCode,
										[HistEigHrdHuur] = @HistEigHrdHuur,
										[HistEigHrdHuurEuro] = @HistEigHrdHuurEuro,
										[HistEigHrdEigenaar] = @HistEigHrdEigenaar,
										[HistEigHrdEigenaarEuro] = @HistEigHrdEigenaarEuro,
										[HistEigHrdHuurder] = @HistEigHrdHuurder,
										[HistEigHrdHuurderEuro] = @HistEigHrdHuurderEuro,
										[HistEigHrdBoete] = @HistEigHrdBoete,
										[HistEigHrdBoeteEuro] = @HistEigHrdBoeteEuro,
										[HistEigHrdKwijtgescholden] = @HistEigHrdKwijtgescholden,
										[HistEigHrdBetaald] = @HistEigHrdBetaald,
										[HistEigHrdCommentaar] = @HistEigHrdCommentaar
								WHERE	[HistEigHrdID] = @HistEigHrdID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@HistEigHrdDatum", OleDbType.Date).Value = HistEigHrdDatum == null ? (Object)DBNull.Value : HistEigHrdDatum;
					cmd.Parameters.Add("@HistEigHrdCode", OleDbType.UnsignedTinyInt).Value = HistEigHrdCode == null ? (Object)DBNull.Value : HistEigHrdCode;
					cmd.Parameters.Add("@HistEigHrdHuur", OleDbType.Integer).Value = HistEigHrdHuur == null ? (Object)DBNull.Value : HistEigHrdHuur;
					cmd.Parameters.Add("@HistEigHrdHuurEuro", OleDbType.Double).Value = HistEigHrdHuurEuro == null ? (Object)DBNull.Value : HistEigHrdHuurEuro;
					cmd.Parameters.Add("@HistEigHrdEigenaar", OleDbType.Integer).Value = HistEigHrdEigenaar == null ? (Object)DBNull.Value : HistEigHrdEigenaar;
					cmd.Parameters.Add("@HistEigHrdEigenaarEuro", OleDbType.Double).Value = HistEigHrdEigenaarEuro == null ? (Object)DBNull.Value : HistEigHrdEigenaarEuro;
					cmd.Parameters.Add("@HistEigHrdHuurder", OleDbType.Integer).Value = HistEigHrdHuurder == null ? (Object)DBNull.Value : HistEigHrdHuurder;
					cmd.Parameters.Add("@HistEigHrdHuurderEuro", OleDbType.Double).Value = HistEigHrdHuurderEuro == null ? (Object)DBNull.Value : HistEigHrdHuurderEuro;
					cmd.Parameters.Add("@HistEigHrdBoete", OleDbType.Integer).Value = HistEigHrdBoete == null ? (Object)DBNull.Value : HistEigHrdBoete;
					cmd.Parameters.Add("@HistEigHrdBoeteEuro", OleDbType.Double).Value = HistEigHrdBoeteEuro == null ? (Object)DBNull.Value : HistEigHrdBoeteEuro;
					cmd.Parameters.Add("@HistEigHrdKwijtgescholden", OleDbType.Boolean).Value = HistEigHrdKwijtgescholden;
					cmd.Parameters.Add("@HistEigHrdBetaald", OleDbType.Date).Value = HistEigHrdBetaald == null ? (Object)DBNull.Value : HistEigHrdBetaald;
					cmd.Parameters.Add("@HistEigHrdCommentaar", OleDbType.WChar).Value = HistEigHrdCommentaar == null ? (Object)DBNull.Value : HistEigHrdCommentaar;
					cmd.Parameters.Add("@HistEigHrdID", OleDbType.Integer).Value = HistEigHrdID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int histEigHrdID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [HistEigHrd]
								WHERE	[HistEigHrdID] = @HistEigHrdID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@HistEigHrdID", OleDbType.Integer).Value = histEigHrdID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static HistEigHrd Get(int histEigHrdID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[HistEigHrdID],
										[HistEigHrdDatum],
										[HistEigHrdCode],
										[HistEigHrdHuur],
										[HistEigHrdHuurEuro],
										[HistEigHrdEigenaar],
										[HistEigHrdEigenaarEuro],
										[HistEigHrdHuurder],
										[HistEigHrdHuurderEuro],
										[HistEigHrdBoete],
										[HistEigHrdBoeteEuro],
										[HistEigHrdKwijtgescholden],
										[HistEigHrdBetaald],
										[HistEigHrdCommentaar]
								FROM	[HistEigHrd]
								WHERE	[HistEigHrdID] = @HistEigHrdID;";

				HistEigHrd histEigHrd = new HistEigHrd();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@HistEigHrdID", OleDbType.Integer).Value = histEigHrdID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							histEigHrd.HistEigHrdID = Convert.ToInt32(reader["HistEigHrdID"]);
							histEigHrd.HistEigHrdDatum = reader["HistEigHrdDatum"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["HistEigHrdDatum"]);
							histEigHrd.HistEigHrdCode = reader["HistEigHrdCode"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["HistEigHrdCode"]);
							histEigHrd.HistEigHrdHuur = reader["HistEigHrdHuur"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["HistEigHrdHuur"]);
							histEigHrd.HistEigHrdHuurEuro = reader["HistEigHrdHuurEuro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["HistEigHrdHuurEuro"]);
							histEigHrd.HistEigHrdEigenaar = reader["HistEigHrdEigenaar"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["HistEigHrdEigenaar"]);
							histEigHrd.HistEigHrdEigenaarEuro = reader["HistEigHrdEigenaarEuro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["HistEigHrdEigenaarEuro"]);
							histEigHrd.HistEigHrdHuurder = reader["HistEigHrdHuurder"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["HistEigHrdHuurder"]);
							histEigHrd.HistEigHrdHuurderEuro = reader["HistEigHrdHuurderEuro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["HistEigHrdHuurderEuro"]);
							histEigHrd.HistEigHrdBoete = reader["HistEigHrdBoete"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["HistEigHrdBoete"]);
							histEigHrd.HistEigHrdBoeteEuro = reader["HistEigHrdBoeteEuro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["HistEigHrdBoeteEuro"]);
							histEigHrd.HistEigHrdKwijtgescholden = Convert.ToBoolean(reader["HistEigHrdKwijtgescholden"]);
							histEigHrd.HistEigHrdBetaald = reader["HistEigHrdBetaald"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["HistEigHrdBetaald"]);
							histEigHrd.HistEigHrdCommentaar = reader["HistEigHrdCommentaar"] == DBNull.Value ? null : reader["HistEigHrdCommentaar"].ToString();
						}
					}
				}

				return histEigHrd;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[HistEigHrdID],
										[HistEigHrdDatum],
										[HistEigHrdCode],
										[HistEigHrdHuur],
										[HistEigHrdHuurEuro],
										[HistEigHrdEigenaar],
										[HistEigHrdEigenaarEuro],
										[HistEigHrdHuurder],
										[HistEigHrdHuurderEuro],
										[HistEigHrdBoete],
										[HistEigHrdBoeteEuro],
										[HistEigHrdKwijtgescholden],
										[HistEigHrdBetaald],
										[HistEigHrdCommentaar]
								FROM	[HistEigHrd];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}