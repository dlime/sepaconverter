using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Advokaat
	{
		#region Properties
		public int Advokaat_ID { get; set; }
		public string Advokaat_Naam { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Advokaat]
								(
									[Advokaat Naam]
								)
								VALUES
								(
									@Advokaat_Naam
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Advokaat_Naam", OleDbType.WChar).Value = Advokaat_Naam == null ? (Object)DBNull.Value : Advokaat_Naam;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Advokaat_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Advokaat]
								SET		[Advokaat Naam] = @Advokaat_Naam
								WHERE	[Advokaat-ID] = @Advokaat_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Advokaat_Naam", OleDbType.WChar).Value = Advokaat_Naam == null ? (Object)DBNull.Value : Advokaat_Naam;
					cmd.Parameters.Add("@Advokaat_ID", OleDbType.Integer).Value = Advokaat_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int advokaat_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Advokaat]
								WHERE	[Advokaat-ID] = @Advokaat_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Advokaat_ID", OleDbType.Integer).Value = advokaat_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Advokaat Get(int advokaat_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Advokaat-ID],
										[Advokaat Naam]
								FROM	[Advokaat]
								WHERE	[Advokaat-ID] = @Advokaat_ID;";

				Advokaat advokaat = new Advokaat();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Advokaat_ID", OleDbType.Integer).Value = advokaat_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							advokaat.Advokaat_ID = Convert.ToInt32(reader["Advokaat-ID"]);
							advokaat.Advokaat_Naam = reader["Advokaat Naam"] == DBNull.Value ? null : reader["Advokaat Naam"].ToString();
						}
					}
				}

				return advokaat;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Advokaat-ID],
										[Advokaat Naam]
								FROM	[Advokaat];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}