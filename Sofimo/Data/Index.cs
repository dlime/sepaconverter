using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Index
	{
		#region Properties
		public int Index_ID { get; set; }
		public DateTime Index_Maand { get; set; }
		public double? Index_Index { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Index]
								(
									[Index Maand],
									[Index Index]
								)
								VALUES
								(
									@Index_Maand,
									@Index_Index
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Index_Maand", OleDbType.Date).Value = Index_Maand;
					cmd.Parameters.Add("@Index_Index", OleDbType.Double).Value = Index_Index == null ? (Object)DBNull.Value : Index_Index;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Index_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Index]
								SET		[Index Maand] = @Index_Maand,
										[Index Index] = @Index_Index
								WHERE	[Index-ID] = @Index_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Index_Maand", OleDbType.Date).Value = Index_Maand;
					cmd.Parameters.Add("@Index_Index", OleDbType.Double).Value = Index_Index == null ? (Object)DBNull.Value : Index_Index;
					cmd.Parameters.Add("@Index_ID", OleDbType.Integer).Value = Index_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int index_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Index]
								WHERE	[Index-ID] = @Index_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Index_ID", OleDbType.Integer).Value = index_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Index Get(int index_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Index-ID],
										[Index Maand],
										[Index Index]
								FROM	[Index]
								WHERE	[Index-ID] = @Index_ID;";

				Index index = new Index();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Index_ID", OleDbType.Integer).Value = index_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							index.Index_ID = Convert.ToInt32(reader["Index-ID"]);
							index.Index_Maand = Convert.ToDateTime(reader["Index Maand"]);
							index.Index_Index = reader["Index Index"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Index Index"]);
						}
					}
				}

				return index;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Index-ID],
										[Index Maand],
										[Index Index]
								FROM	[Index];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}