using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Factuurlijn
	{
		#region Properties
		public int Factuurlijn_ID { get; set; }
		public int? Factuurlijn_Factuur_ID { get; set; }
		public string Factuurlijn_Omschrijving { get; set; }
		public int? Factuurlijn_Bedrag { get; set; }
		public double? Factuurlijn_Bedrag_Euro { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Factuurlijn]
								(
									[Factuurlijn Factuur-ID],
									[Factuurlijn Omschrijving],
									[Factuurlijn Bedrag],
									[Factuurlijn Bedrag Euro]
								)
								VALUES
								(
									@Factuurlijn_Factuur_ID,
									@Factuurlijn_Omschrijving,
									@Factuurlijn_Bedrag,
									@Factuurlijn_Bedrag_Euro
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Factuurlijn_Factuur_ID", OleDbType.Integer).Value = Factuurlijn_Factuur_ID == null ? (Object)DBNull.Value : Factuurlijn_Factuur_ID;
					cmd.Parameters.Add("@Factuurlijn_Omschrijving", OleDbType.WChar).Value = Factuurlijn_Omschrijving == null ? (Object)DBNull.Value : Factuurlijn_Omschrijving;
					cmd.Parameters.Add("@Factuurlijn_Bedrag", OleDbType.Integer).Value = Factuurlijn_Bedrag == null ? (Object)DBNull.Value : Factuurlijn_Bedrag;
					cmd.Parameters.Add("@Factuurlijn_Bedrag_Euro", OleDbType.Double).Value = Factuurlijn_Bedrag_Euro == null ? (Object)DBNull.Value : Factuurlijn_Bedrag_Euro;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Factuurlijn_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Factuurlijn]
								SET		[Factuurlijn Factuur-ID] = @Factuurlijn_Factuur_ID,
										[Factuurlijn Omschrijving] = @Factuurlijn_Omschrijving,
										[Factuurlijn Bedrag] = @Factuurlijn_Bedrag,
										[Factuurlijn Bedrag Euro] = @Factuurlijn_Bedrag_Euro
								WHERE	[Factuurlijn-ID] = @Factuurlijn_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Factuurlijn_Factuur_ID", OleDbType.Integer).Value = Factuurlijn_Factuur_ID == null ? (Object)DBNull.Value : Factuurlijn_Factuur_ID;
					cmd.Parameters.Add("@Factuurlijn_Omschrijving", OleDbType.WChar).Value = Factuurlijn_Omschrijving == null ? (Object)DBNull.Value : Factuurlijn_Omschrijving;
					cmd.Parameters.Add("@Factuurlijn_Bedrag", OleDbType.Integer).Value = Factuurlijn_Bedrag == null ? (Object)DBNull.Value : Factuurlijn_Bedrag;
					cmd.Parameters.Add("@Factuurlijn_Bedrag_Euro", OleDbType.Double).Value = Factuurlijn_Bedrag_Euro == null ? (Object)DBNull.Value : Factuurlijn_Bedrag_Euro;
					cmd.Parameters.Add("@Factuurlijn_ID", OleDbType.Integer).Value = Factuurlijn_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int factuurlijn_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Factuurlijn]
								WHERE	[Factuurlijn-ID] = @Factuurlijn_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Factuurlijn_ID", OleDbType.Integer).Value = factuurlijn_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Factuurlijn Get(int factuurlijn_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Factuurlijn-ID],
										[Factuurlijn Factuur-ID],
										[Factuurlijn Omschrijving],
										[Factuurlijn Bedrag],
										[Factuurlijn Bedrag Euro]
								FROM	[Factuurlijn]
								WHERE	[Factuurlijn-ID] = @Factuurlijn_ID;";

				Factuurlijn factuurlijn = new Factuurlijn();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Factuurlijn_ID", OleDbType.Integer).Value = factuurlijn_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							factuurlijn.Factuurlijn_ID = Convert.ToInt32(reader["Factuurlijn-ID"]);
							factuurlijn.Factuurlijn_Factuur_ID = reader["Factuurlijn Factuur-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Factuurlijn Factuur-ID"]);
							factuurlijn.Factuurlijn_Omschrijving = reader["Factuurlijn Omschrijving"] == DBNull.Value ? null : reader["Factuurlijn Omschrijving"].ToString();
							factuurlijn.Factuurlijn_Bedrag = reader["Factuurlijn Bedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Factuurlijn Bedrag"]);
							factuurlijn.Factuurlijn_Bedrag_Euro = reader["Factuurlijn Bedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Factuurlijn Bedrag Euro"]);
						}
					}
				}

				return factuurlijn;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Factuurlijn-ID],
										[Factuurlijn Factuur-ID],
										[Factuurlijn Omschrijving],
										[Factuurlijn Bedrag],
										[Factuurlijn Bedrag Euro]
								FROM	[Factuurlijn];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}