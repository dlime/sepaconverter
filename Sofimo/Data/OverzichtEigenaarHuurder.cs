using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class OverzichtEigenaarHuurder
	{
		#region Properties
		public int OvzEigHrdID { get; set; }
		public int OvzEigHrdEigendomID { get; set; }
		public int OvzEigHrdEigenaarID { get; set; }
		public int OvzEigHrdHuurderID { get; set; }
		public int? OvzEigHrdBedragEigenaar { get; set; }
		public double? OvzEigHrdBedragEigenaarEuro { get; set; }
		public int? OvzEigHrdProcentEigenaar { get; set; }
		public int? OvzEigHrdBedragHuurder { get; set; }
		public double? OvzEigHrdBedragHuurderEuro { get; set; }
		public int? OvzEigHrdProcentHuurder { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [OverzichtEigenaarHuurder]
								(
									[OvzEigHrdEigendomID],
									[OvzEigHrdEigenaarID],
									[OvzEigHrdHuurderID],
									[OvzEigHrdBedragEigenaar],
									[OvzEigHrdBedragEigenaarEuro],
									[OvzEigHrdProcentEigenaar],
									[OvzEigHrdBedragHuurder],
									[OvzEigHrdBedragHuurderEuro],
									[OvzEigHrdProcentHuurder]
								)
								VALUES
								(
									@OvzEigHrdEigendomID,
									@OvzEigHrdEigenaarID,
									@OvzEigHrdHuurderID,
									@OvzEigHrdBedragEigenaar,
									@OvzEigHrdBedragEigenaarEuro,
									@OvzEigHrdProcentEigenaar,
									@OvzEigHrdBedragHuurder,
									@OvzEigHrdBedragHuurderEuro,
									@OvzEigHrdProcentHuurder
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OvzEigHrdEigendomID", OleDbType.Integer).Value = OvzEigHrdEigendomID;
					cmd.Parameters.Add("@OvzEigHrdEigenaarID", OleDbType.Integer).Value = OvzEigHrdEigenaarID;
					cmd.Parameters.Add("@OvzEigHrdHuurderID", OleDbType.Integer).Value = OvzEigHrdHuurderID;
					cmd.Parameters.Add("@OvzEigHrdBedragEigenaar", OleDbType.Integer).Value = OvzEigHrdBedragEigenaar == null ? (Object)DBNull.Value : OvzEigHrdBedragEigenaar;
					cmd.Parameters.Add("@OvzEigHrdBedragEigenaarEuro", OleDbType.Double).Value = OvzEigHrdBedragEigenaarEuro == null ? (Object)DBNull.Value : OvzEigHrdBedragEigenaarEuro;
					cmd.Parameters.Add("@OvzEigHrdProcentEigenaar", OleDbType.Integer).Value = OvzEigHrdProcentEigenaar == null ? (Object)DBNull.Value : OvzEigHrdProcentEigenaar;
					cmd.Parameters.Add("@OvzEigHrdBedragHuurder", OleDbType.Integer).Value = OvzEigHrdBedragHuurder == null ? (Object)DBNull.Value : OvzEigHrdBedragHuurder;
					cmd.Parameters.Add("@OvzEigHrdBedragHuurderEuro", OleDbType.Double).Value = OvzEigHrdBedragHuurderEuro == null ? (Object)DBNull.Value : OvzEigHrdBedragHuurderEuro;
					cmd.Parameters.Add("@OvzEigHrdProcentHuurder", OleDbType.Integer).Value = OvzEigHrdProcentHuurder == null ? (Object)DBNull.Value : OvzEigHrdProcentHuurder;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					OvzEigHrdID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[OverzichtEigenaarHuurder]
								SET		[OvzEigHrdBedragEigenaar] = @OvzEigHrdBedragEigenaar,
										[OvzEigHrdBedragEigenaarEuro] = @OvzEigHrdBedragEigenaarEuro,
										[OvzEigHrdProcentEigenaar] = @OvzEigHrdProcentEigenaar,
										[OvzEigHrdBedragHuurder] = @OvzEigHrdBedragHuurder,
										[OvzEigHrdBedragHuurderEuro] = @OvzEigHrdBedragHuurderEuro,
										[OvzEigHrdProcentHuurder] = @OvzEigHrdProcentHuurder
								WHERE	[OvzEigHrdEigendomID] = @OvzEigHrdEigendomID
								AND		[OvzEigHrdEigenaarID] = @OvzEigHrdEigenaarID
								AND		[OvzEigHrdHuurderID] = @OvzEigHrdHuurderID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OvzEigHrdID", OleDbType.Integer).Value = OvzEigHrdID;
					cmd.Parameters.Add("@OvzEigHrdBedragEigenaar", OleDbType.Integer).Value = OvzEigHrdBedragEigenaar == null ? (Object)DBNull.Value : OvzEigHrdBedragEigenaar;
					cmd.Parameters.Add("@OvzEigHrdBedragEigenaarEuro", OleDbType.Double).Value = OvzEigHrdBedragEigenaarEuro == null ? (Object)DBNull.Value : OvzEigHrdBedragEigenaarEuro;
					cmd.Parameters.Add("@OvzEigHrdProcentEigenaar", OleDbType.Integer).Value = OvzEigHrdProcentEigenaar == null ? (Object)DBNull.Value : OvzEigHrdProcentEigenaar;
					cmd.Parameters.Add("@OvzEigHrdBedragHuurder", OleDbType.Integer).Value = OvzEigHrdBedragHuurder == null ? (Object)DBNull.Value : OvzEigHrdBedragHuurder;
					cmd.Parameters.Add("@OvzEigHrdBedragHuurderEuro", OleDbType.Double).Value = OvzEigHrdBedragHuurderEuro == null ? (Object)DBNull.Value : OvzEigHrdBedragHuurderEuro;
					cmd.Parameters.Add("@OvzEigHrdProcentHuurder", OleDbType.Integer).Value = OvzEigHrdProcentHuurder == null ? (Object)DBNull.Value : OvzEigHrdProcentHuurder;
					cmd.Parameters.Add("@OvzEigHrdEigendomID", OleDbType.Integer).Value = OvzEigHrdEigendomID;
					cmd.Parameters.Add("@OvzEigHrdEigenaarID", OleDbType.Integer).Value = OvzEigHrdEigenaarID;
					cmd.Parameters.Add("@OvzEigHrdHuurderID", OleDbType.Integer).Value = OvzEigHrdHuurderID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int ovzEigHrdEigendomID, int ovzEigHrdEigenaarID, int ovzEigHrdHuurderID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [OverzichtEigenaarHuurder]
								WHERE	[OvzEigHrdEigendomID] = @OvzEigHrdEigendomID
								AND		[OvzEigHrdEigenaarID] = @OvzEigHrdEigenaarID
								AND		[OvzEigHrdHuurderID] = @OvzEigHrdHuurderID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OvzEigHrdEigendomID", OleDbType.Integer).Value = ovzEigHrdEigendomID;
					cmd.Parameters.Add("@OvzEigHrdEigenaarID", OleDbType.Integer).Value = ovzEigHrdEigenaarID;
					cmd.Parameters.Add("@OvzEigHrdHuurderID", OleDbType.Integer).Value = ovzEigHrdHuurderID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static OverzichtEigenaarHuurder Get(int ovzEigHrdEigendomID, int ovzEigHrdEigenaarID, int ovzEigHrdHuurderID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[OvzEigHrdID],
										[OvzEigHrdEigendomID],
										[OvzEigHrdEigenaarID],
										[OvzEigHrdHuurderID],
										[OvzEigHrdBedragEigenaar],
										[OvzEigHrdBedragEigenaarEuro],
										[OvzEigHrdProcentEigenaar],
										[OvzEigHrdBedragHuurder],
										[OvzEigHrdBedragHuurderEuro],
										[OvzEigHrdProcentHuurder]
								FROM	[OverzichtEigenaarHuurder]
								WHERE	[OvzEigHrdEigendomID] = @OvzEigHrdEigendomID
								AND		[OvzEigHrdEigenaarID] = @OvzEigHrdEigenaarID
								AND		[OvzEigHrdHuurderID] = @OvzEigHrdHuurderID;";

				OverzichtEigenaarHuurder overzichtEigenaarHuurder = new OverzichtEigenaarHuurder();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OvzEigHrdEigendomID", OleDbType.Integer).Value = ovzEigHrdEigendomID;
					cmd.Parameters.Add("@OvzEigHrdEigenaarID", OleDbType.Integer).Value = ovzEigHrdEigenaarID;
					cmd.Parameters.Add("@OvzEigHrdHuurderID", OleDbType.Integer).Value = ovzEigHrdHuurderID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							overzichtEigenaarHuurder.OvzEigHrdID = Convert.ToInt32(reader["OvzEigHrdID"]);
							overzichtEigenaarHuurder.OvzEigHrdEigendomID = Convert.ToInt32(reader["OvzEigHrdEigendomID"]);
							overzichtEigenaarHuurder.OvzEigHrdEigenaarID = Convert.ToInt32(reader["OvzEigHrdEigenaarID"]);
							overzichtEigenaarHuurder.OvzEigHrdHuurderID = Convert.ToInt32(reader["OvzEigHrdHuurderID"]);
							overzichtEigenaarHuurder.OvzEigHrdBedragEigenaar = reader["OvzEigHrdBedragEigenaar"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["OvzEigHrdBedragEigenaar"]);
							overzichtEigenaarHuurder.OvzEigHrdBedragEigenaarEuro = reader["OvzEigHrdBedragEigenaarEuro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["OvzEigHrdBedragEigenaarEuro"]);
							overzichtEigenaarHuurder.OvzEigHrdProcentEigenaar = reader["OvzEigHrdProcentEigenaar"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["OvzEigHrdProcentEigenaar"]);
							overzichtEigenaarHuurder.OvzEigHrdBedragHuurder = reader["OvzEigHrdBedragHuurder"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["OvzEigHrdBedragHuurder"]);
							overzichtEigenaarHuurder.OvzEigHrdBedragHuurderEuro = reader["OvzEigHrdBedragHuurderEuro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["OvzEigHrdBedragHuurderEuro"]);
							overzichtEigenaarHuurder.OvzEigHrdProcentHuurder = reader["OvzEigHrdProcentHuurder"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["OvzEigHrdProcentHuurder"]);
						}
					}
				}

				return overzichtEigenaarHuurder;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[OvzEigHrdID],
										[OvzEigHrdEigendomID],
										[OvzEigHrdEigenaarID],
										[OvzEigHrdHuurderID],
										[OvzEigHrdBedragEigenaar],
										[OvzEigHrdBedragEigenaarEuro],
										[OvzEigHrdProcentEigenaar],
										[OvzEigHrdBedragHuurder],
										[OvzEigHrdBedragHuurderEuro],
										[OvzEigHrdProcentHuurder]
								FROM	[OverzichtEigenaarHuurder];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}