using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Gemeente
	{
		#region Properties
		public int Gemeente_ID { get; set; }
		public string Gemeente_Postcode { get; set; }
		public string Gemeente_Naam { get; set; }
		public string Gemeente_Kadaster { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Gemeente]
								(
									[Gemeente Postcode],
									[Gemeente Naam],
									[Gemeente Kadaster]
								)
								VALUES
								(
									@Gemeente_Postcode,
									@Gemeente_Naam,
									@Gemeente_Kadaster
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Gemeente_Postcode", OleDbType.WChar).Value = Gemeente_Postcode == null ? (Object)DBNull.Value : Gemeente_Postcode;
					cmd.Parameters.Add("@Gemeente_Naam", OleDbType.WChar).Value = Gemeente_Naam;
					cmd.Parameters.Add("@Gemeente_Kadaster", OleDbType.WChar).Value = Gemeente_Kadaster == null ? (Object)DBNull.Value : Gemeente_Kadaster;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Gemeente_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Gemeente]
								SET		[Gemeente Postcode] = @Gemeente_Postcode,
										[Gemeente Naam] = @Gemeente_Naam,
										[Gemeente Kadaster] = @Gemeente_Kadaster
								WHERE	[Gemeente-ID] = @Gemeente_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Gemeente_Postcode", OleDbType.WChar).Value = Gemeente_Postcode == null ? (Object)DBNull.Value : Gemeente_Postcode;
					cmd.Parameters.Add("@Gemeente_Naam", OleDbType.WChar).Value = Gemeente_Naam;
					cmd.Parameters.Add("@Gemeente_Kadaster", OleDbType.WChar).Value = Gemeente_Kadaster == null ? (Object)DBNull.Value : Gemeente_Kadaster;
					cmd.Parameters.Add("@Gemeente_ID", OleDbType.Integer).Value = Gemeente_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int gemeente_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Gemeente]
								WHERE	[Gemeente-ID] = @Gemeente_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Gemeente_ID", OleDbType.Integer).Value = gemeente_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Gemeente Get(int gemeente_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Gemeente-ID],
										[Gemeente Postcode],
										[Gemeente Naam],
										[Gemeente Kadaster]
								FROM	[Gemeente]
								WHERE	[Gemeente-ID] = @Gemeente_ID;";

				Gemeente gemeente = new Gemeente();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Gemeente_ID", OleDbType.Integer).Value = gemeente_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							gemeente.Gemeente_ID = Convert.ToInt32(reader["Gemeente-ID"]);
							gemeente.Gemeente_Postcode = reader["Gemeente Postcode"] == DBNull.Value ? null : reader["Gemeente Postcode"].ToString();
							gemeente.Gemeente_Naam = reader["Gemeente Naam"].ToString();
							gemeente.Gemeente_Kadaster = reader["Gemeente Kadaster"] == DBNull.Value ? null : reader["Gemeente Kadaster"].ToString();
						}
					}
				}

				return gemeente;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Gemeente-ID],
										[Gemeente Postcode],
										[Gemeente Naam],
										[Gemeente Kadaster]
								FROM	[Gemeente];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}