using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Eigenaarfiche
	{
		#region Properties
		public int Eigenaarfiche_ID { get; set; }
		public int? Eigenaarfiche_Eigenaar_ID { get; set; }
		public int? Eigenaarfiche_Eigendom_Eigenaar_ID { get; set; }
		public int? Eigenaarfiche_Huurder_ID { get; set; }
		public DateTime? Eigenaarfiche_Datum { get; set; }
		public int Eigenaarfiche_Referentie { get; set; }
		public int? Eigenaarfiche_Bedrag { get; set; }
		public double? Eigenaarfiche_Bedrag_Euro { get; set; }
		public string Eigenaarfiche_Commentaar { get; set; }
		public int? Eigenaarfiche_Commissiebedrag { get; set; }
		public double? Eigenaarfiche_Commissiebedrag_Euro { get; set; }
		public int? Eigenaarfiche_BTWBedrag { get; set; }
		public double? Eigenaarfiche_BTWBedrag_Euro { get; set; }
		public bool Eigenaarfiche_Lock { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Eigenaarfiche]
								(
									[Eigenaarfiche Eigenaar-ID],
									[Eigenaarfiche Eigendom-Eigenaar-ID],
									[Eigenaarfiche Huurder-ID],
									[Eigenaarfiche Datum],
									[Eigenaarfiche Bedrag],
									[Eigenaarfiche Bedrag Euro],
									[Eigenaarfiche Commentaar],
									[Eigenaarfiche Commissiebedrag],
									[Eigenaarfiche Commissiebedrag Euro],
									[Eigenaarfiche BTWBedrag],
									[Eigenaarfiche BTWBedrag Euro],
									[Eigenaarfiche Lock]
								)
								VALUES
								(
									@Eigenaarfiche_Eigenaar_ID,
									@Eigenaarfiche_Eigendom_Eigenaar_ID,
									@Eigenaarfiche_Huurder_ID,
									@Eigenaarfiche_Datum,
									@Eigenaarfiche_Bedrag,
									@Eigenaarfiche_Bedrag_Euro,
									@Eigenaarfiche_Commentaar,
									@Eigenaarfiche_Commissiebedrag,
									@Eigenaarfiche_Commissiebedrag_Euro,
									@Eigenaarfiche_BTWBedrag,
									@Eigenaarfiche_BTWBedrag_Euro,
									@Eigenaarfiche_Lock
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigenaarfiche_Eigenaar_ID", OleDbType.Integer).Value = Eigenaarfiche_Eigenaar_ID == null ? (Object)DBNull.Value : Eigenaarfiche_Eigenaar_ID;
					cmd.Parameters.Add("@Eigenaarfiche_Eigendom_Eigenaar_ID", OleDbType.Integer).Value = Eigenaarfiche_Eigendom_Eigenaar_ID == null ? (Object)DBNull.Value : Eigenaarfiche_Eigendom_Eigenaar_ID;
					cmd.Parameters.Add("@Eigenaarfiche_Huurder_ID", OleDbType.Integer).Value = Eigenaarfiche_Huurder_ID == null ? (Object)DBNull.Value : Eigenaarfiche_Huurder_ID;
					cmd.Parameters.Add("@Eigenaarfiche_Datum", OleDbType.Date).Value = Eigenaarfiche_Datum == null ? (Object)DBNull.Value : Eigenaarfiche_Datum;
					cmd.Parameters.Add("@Eigenaarfiche_Bedrag", OleDbType.Integer).Value = Eigenaarfiche_Bedrag == null ? (Object)DBNull.Value : Eigenaarfiche_Bedrag;
					cmd.Parameters.Add("@Eigenaarfiche_Bedrag_Euro", OleDbType.Double).Value = Eigenaarfiche_Bedrag_Euro == null ? (Object)DBNull.Value : Eigenaarfiche_Bedrag_Euro;
					cmd.Parameters.Add("@Eigenaarfiche_Commentaar", OleDbType.WChar).Value = Eigenaarfiche_Commentaar == null ? (Object)DBNull.Value : Eigenaarfiche_Commentaar;
					cmd.Parameters.Add("@Eigenaarfiche_Commissiebedrag", OleDbType.Integer).Value = Eigenaarfiche_Commissiebedrag == null ? (Object)DBNull.Value : Eigenaarfiche_Commissiebedrag;
					cmd.Parameters.Add("@Eigenaarfiche_Commissiebedrag_Euro", OleDbType.Double).Value = Eigenaarfiche_Commissiebedrag_Euro == null ? (Object)DBNull.Value : Eigenaarfiche_Commissiebedrag_Euro;
					cmd.Parameters.Add("@Eigenaarfiche_BTWBedrag", OleDbType.Integer).Value = Eigenaarfiche_BTWBedrag == null ? (Object)DBNull.Value : Eigenaarfiche_BTWBedrag;
					cmd.Parameters.Add("@Eigenaarfiche_BTWBedrag_Euro", OleDbType.Double).Value = Eigenaarfiche_BTWBedrag_Euro == null ? (Object)DBNull.Value : Eigenaarfiche_BTWBedrag_Euro;
					cmd.Parameters.Add("@Eigenaarfiche_Lock", OleDbType.Boolean).Value = Eigenaarfiche_Lock;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Eigenaarfiche_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Eigenaarfiche]
								SET		[Eigenaarfiche Eigenaar-ID] = @Eigenaarfiche_Eigenaar_ID,
										[Eigenaarfiche Eigendom-Eigenaar-ID] = @Eigenaarfiche_Eigendom_Eigenaar_ID,
										[Eigenaarfiche Huurder-ID] = @Eigenaarfiche_Huurder_ID,
										[Eigenaarfiche Datum] = @Eigenaarfiche_Datum,
										[Eigenaarfiche Bedrag] = @Eigenaarfiche_Bedrag,
										[Eigenaarfiche Bedrag Euro] = @Eigenaarfiche_Bedrag_Euro,
										[Eigenaarfiche Commentaar] = @Eigenaarfiche_Commentaar,
										[Eigenaarfiche Commissiebedrag] = @Eigenaarfiche_Commissiebedrag,
										[Eigenaarfiche Commissiebedrag Euro] = @Eigenaarfiche_Commissiebedrag_Euro,
										[Eigenaarfiche BTWBedrag] = @Eigenaarfiche_BTWBedrag,
										[Eigenaarfiche BTWBedrag Euro] = @Eigenaarfiche_BTWBedrag_Euro,
										[Eigenaarfiche Lock] = @Eigenaarfiche_Lock
								WHERE	[Eigenaarfiche-ID] = @Eigenaarfiche_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigenaarfiche_Eigenaar_ID", OleDbType.Integer).Value = Eigenaarfiche_Eigenaar_ID == null ? (Object)DBNull.Value : Eigenaarfiche_Eigenaar_ID;
					cmd.Parameters.Add("@Eigenaarfiche_Eigendom_Eigenaar_ID", OleDbType.Integer).Value = Eigenaarfiche_Eigendom_Eigenaar_ID == null ? (Object)DBNull.Value : Eigenaarfiche_Eigendom_Eigenaar_ID;
					cmd.Parameters.Add("@Eigenaarfiche_Huurder_ID", OleDbType.Integer).Value = Eigenaarfiche_Huurder_ID == null ? (Object)DBNull.Value : Eigenaarfiche_Huurder_ID;
					cmd.Parameters.Add("@Eigenaarfiche_Datum", OleDbType.Date).Value = Eigenaarfiche_Datum == null ? (Object)DBNull.Value : Eigenaarfiche_Datum;
					cmd.Parameters.Add("@Eigenaarfiche_Referentie", OleDbType.Integer).Value = Eigenaarfiche_Referentie;
					cmd.Parameters.Add("@Eigenaarfiche_Bedrag", OleDbType.Integer).Value = Eigenaarfiche_Bedrag == null ? (Object)DBNull.Value : Eigenaarfiche_Bedrag;
					cmd.Parameters.Add("@Eigenaarfiche_Bedrag_Euro", OleDbType.Double).Value = Eigenaarfiche_Bedrag_Euro == null ? (Object)DBNull.Value : Eigenaarfiche_Bedrag_Euro;
					cmd.Parameters.Add("@Eigenaarfiche_Commentaar", OleDbType.WChar).Value = Eigenaarfiche_Commentaar == null ? (Object)DBNull.Value : Eigenaarfiche_Commentaar;
					cmd.Parameters.Add("@Eigenaarfiche_Commissiebedrag", OleDbType.Integer).Value = Eigenaarfiche_Commissiebedrag == null ? (Object)DBNull.Value : Eigenaarfiche_Commissiebedrag;
					cmd.Parameters.Add("@Eigenaarfiche_Commissiebedrag_Euro", OleDbType.Double).Value = Eigenaarfiche_Commissiebedrag_Euro == null ? (Object)DBNull.Value : Eigenaarfiche_Commissiebedrag_Euro;
					cmd.Parameters.Add("@Eigenaarfiche_BTWBedrag", OleDbType.Integer).Value = Eigenaarfiche_BTWBedrag == null ? (Object)DBNull.Value : Eigenaarfiche_BTWBedrag;
					cmd.Parameters.Add("@Eigenaarfiche_BTWBedrag_Euro", OleDbType.Double).Value = Eigenaarfiche_BTWBedrag_Euro == null ? (Object)DBNull.Value : Eigenaarfiche_BTWBedrag_Euro;
					cmd.Parameters.Add("@Eigenaarfiche_Lock", OleDbType.Boolean).Value = Eigenaarfiche_Lock;
					cmd.Parameters.Add("@Eigenaarfiche_ID", OleDbType.Integer).Value = Eigenaarfiche_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int eigenaarfiche_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Eigenaarfiche]
								WHERE	[Eigenaarfiche-ID] = @Eigenaarfiche_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigenaarfiche_ID", OleDbType.Integer).Value = eigenaarfiche_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Eigenaarfiche Get(int eigenaarfiche_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Eigenaarfiche-ID],
										[Eigenaarfiche Eigenaar-ID],
										[Eigenaarfiche Eigendom-Eigenaar-ID],
										[Eigenaarfiche Huurder-ID],
										[Eigenaarfiche Datum],
										[Eigenaarfiche Referentie],
										[Eigenaarfiche Bedrag],
										[Eigenaarfiche Bedrag Euro],
										[Eigenaarfiche Commentaar],
										[Eigenaarfiche Commissiebedrag],
										[Eigenaarfiche Commissiebedrag Euro],
										[Eigenaarfiche BTWBedrag],
										[Eigenaarfiche BTWBedrag Euro],
										[Eigenaarfiche Lock]
								FROM	[Eigenaarfiche]
								WHERE	[Eigenaarfiche-ID] = @Eigenaarfiche_ID;";

				Eigenaarfiche eigenaarfiche = new Eigenaarfiche();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Eigenaarfiche_ID", OleDbType.Integer).Value = eigenaarfiche_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							eigenaarfiche.Eigenaarfiche_ID = Convert.ToInt32(reader["Eigenaarfiche-ID"]);
							eigenaarfiche.Eigenaarfiche_Eigenaar_ID = reader["Eigenaarfiche Eigenaar-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigenaarfiche Eigenaar-ID"]);
							eigenaarfiche.Eigenaarfiche_Eigendom_Eigenaar_ID = reader["Eigenaarfiche Eigendom-Eigenaar-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigenaarfiche Eigendom-Eigenaar-ID"]);
							eigenaarfiche.Eigenaarfiche_Huurder_ID = reader["Eigenaarfiche Huurder-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigenaarfiche Huurder-ID"]);
							eigenaarfiche.Eigenaarfiche_Datum = reader["Eigenaarfiche Datum"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Eigenaarfiche Datum"]);
							eigenaarfiche.Eigenaarfiche_Referentie = Convert.ToInt32(reader["Eigenaarfiche Referentie"]);
							eigenaarfiche.Eigenaarfiche_Bedrag = reader["Eigenaarfiche Bedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigenaarfiche Bedrag"]);
							eigenaarfiche.Eigenaarfiche_Bedrag_Euro = reader["Eigenaarfiche Bedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigenaarfiche Bedrag Euro"]);
							eigenaarfiche.Eigenaarfiche_Commentaar = reader["Eigenaarfiche Commentaar"] == DBNull.Value ? null : reader["Eigenaarfiche Commentaar"].ToString();
							eigenaarfiche.Eigenaarfiche_Commissiebedrag = reader["Eigenaarfiche Commissiebedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigenaarfiche Commissiebedrag"]);
							eigenaarfiche.Eigenaarfiche_Commissiebedrag_Euro = reader["Eigenaarfiche Commissiebedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigenaarfiche Commissiebedrag Euro"]);
							eigenaarfiche.Eigenaarfiche_BTWBedrag = reader["Eigenaarfiche BTWBedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Eigenaarfiche BTWBedrag"]);
							eigenaarfiche.Eigenaarfiche_BTWBedrag_Euro = reader["Eigenaarfiche BTWBedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Eigenaarfiche BTWBedrag Euro"]);
							eigenaarfiche.Eigenaarfiche_Lock = Convert.ToBoolean(reader["Eigenaarfiche Lock"]);
						}
					}
				}

				return eigenaarfiche;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Eigenaarfiche-ID],
										[Eigenaarfiche Eigenaar-ID],
										[Eigenaarfiche Eigendom-Eigenaar-ID],
										[Eigenaarfiche Huurder-ID],
										[Eigenaarfiche Datum],
										[Eigenaarfiche Referentie],
										[Eigenaarfiche Bedrag],
										[Eigenaarfiche Bedrag Euro],
										[Eigenaarfiche Commentaar],
										[Eigenaarfiche Commissiebedrag],
										[Eigenaarfiche Commissiebedrag Euro],
										[Eigenaarfiche BTWBedrag],
										[Eigenaarfiche BTWBedrag Euro],
										[Eigenaarfiche Lock]
								FROM	[Eigenaarfiche];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}