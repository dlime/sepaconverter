using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Plaatsbeschrijving
	{
		#region Properties
		public int Plaatsbeschrijving_ID { get; set; }
		public int? Plaatsbeschrijving_Eigendom_ID { get; set; }
		public string Plaatsbeschrijving_Rubriek { get; set; }
		public string Plaatsbeschrijving_Commentaar { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Plaatsbeschrijving]
								(
									[Plaatsbeschrijving Eigendom-ID],
									[Plaatsbeschrijving Rubriek],
									[Plaatsbeschrijving Commentaar]
								)
								VALUES
								(
									@Plaatsbeschrijving_Eigendom_ID,
									@Plaatsbeschrijving_Rubriek,
									@Plaatsbeschrijving_Commentaar
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Plaatsbeschrijving_Eigendom_ID", OleDbType.Integer).Value = Plaatsbeschrijving_Eigendom_ID == null ? (Object)DBNull.Value : Plaatsbeschrijving_Eigendom_ID;
					cmd.Parameters.Add("@Plaatsbeschrijving_Rubriek", OleDbType.WChar).Value = Plaatsbeschrijving_Rubriek == null ? (Object)DBNull.Value : Plaatsbeschrijving_Rubriek;
					cmd.Parameters.Add("@Plaatsbeschrijving_Commentaar", OleDbType.WChar).Value = Plaatsbeschrijving_Commentaar == null ? (Object)DBNull.Value : Plaatsbeschrijving_Commentaar;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Plaatsbeschrijving_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Plaatsbeschrijving]
								SET		[Plaatsbeschrijving Eigendom-ID] = @Plaatsbeschrijving_Eigendom_ID,
										[Plaatsbeschrijving Rubriek] = @Plaatsbeschrijving_Rubriek,
										[Plaatsbeschrijving Commentaar] = @Plaatsbeschrijving_Commentaar
								WHERE	[Plaatsbeschrijving-ID] = @Plaatsbeschrijving_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Plaatsbeschrijving_Eigendom_ID", OleDbType.Integer).Value = Plaatsbeschrijving_Eigendom_ID == null ? (Object)DBNull.Value : Plaatsbeschrijving_Eigendom_ID;
					cmd.Parameters.Add("@Plaatsbeschrijving_Rubriek", OleDbType.WChar).Value = Plaatsbeschrijving_Rubriek == null ? (Object)DBNull.Value : Plaatsbeschrijving_Rubriek;
					cmd.Parameters.Add("@Plaatsbeschrijving_Commentaar", OleDbType.WChar).Value = Plaatsbeschrijving_Commentaar == null ? (Object)DBNull.Value : Plaatsbeschrijving_Commentaar;
					cmd.Parameters.Add("@Plaatsbeschrijving_ID", OleDbType.Integer).Value = Plaatsbeschrijving_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int plaatsbeschrijving_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Plaatsbeschrijving]
								WHERE	[Plaatsbeschrijving-ID] = @Plaatsbeschrijving_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Plaatsbeschrijving_ID", OleDbType.Integer).Value = plaatsbeschrijving_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Plaatsbeschrijving Get(int plaatsbeschrijving_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Plaatsbeschrijving-ID],
										[Plaatsbeschrijving Eigendom-ID],
										[Plaatsbeschrijving Rubriek],
										[Plaatsbeschrijving Commentaar]
								FROM	[Plaatsbeschrijving]
								WHERE	[Plaatsbeschrijving-ID] = @Plaatsbeschrijving_ID;";

				Plaatsbeschrijving plaatsbeschrijving = new Plaatsbeschrijving();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Plaatsbeschrijving_ID", OleDbType.Integer).Value = plaatsbeschrijving_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							plaatsbeschrijving.Plaatsbeschrijving_ID = Convert.ToInt32(reader["Plaatsbeschrijving-ID"]);
							plaatsbeschrijving.Plaatsbeschrijving_Eigendom_ID = reader["Plaatsbeschrijving Eigendom-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Plaatsbeschrijving Eigendom-ID"]);
							plaatsbeschrijving.Plaatsbeschrijving_Rubriek = reader["Plaatsbeschrijving Rubriek"] == DBNull.Value ? null : reader["Plaatsbeschrijving Rubriek"].ToString();
							plaatsbeschrijving.Plaatsbeschrijving_Commentaar = reader["Plaatsbeschrijving Commentaar"] == DBNull.Value ? null : reader["Plaatsbeschrijving Commentaar"].ToString();
						}
					}
				}

				return plaatsbeschrijving;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Plaatsbeschrijving-ID],
										[Plaatsbeschrijving Eigendom-ID],
										[Plaatsbeschrijving Rubriek],
										[Plaatsbeschrijving Commentaar]
								FROM	[Plaatsbeschrijving];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}