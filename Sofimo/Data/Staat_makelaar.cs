using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Staat_makelaar
	{
		#region Properties
		public int StaatMakelaar_ID { get; set; }
		public byte StaatMakelaar_Factuur_credietnota { get; set; }
		public int? StaatMakelaar_Nummer { get; set; }
		public int? StaatMakelaar_Dossiernummer { get; set; }
		public string StaatMakelaar_Omschrijving { get; set; }
		public string StaatMakelaar_Commissie_Code { get; set; }
		public string StaatMakelaar_Binnen_Code { get; set; }
		public string StaatMakelaar_Buiten_Code { get; set; }
		public int? StaatMakelaar_Bedrag { get; set; }
		public double? StaatMakelaar_Bedrag_Euro { get; set; }
		public int? StaatMakelaar_Bedrag_binnen { get; set; }
		public double? StaatMakelaar_Bedrag_binnen_Euro { get; set; }
		public int? StaatMakelaar_Bedrag_buiten { get; set; }
		public double? StaatMakelaar_Bedrag_buiten_Euro { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Staat makelaar]
								(
									[StaatMakelaar Factuur-credietnota],
									[StaatMakelaar Nummer],
									[StaatMakelaar Dossiernummer],
									[StaatMakelaar Omschrijving],
									[StaatMakelaar Commissie Code],
									[StaatMakelaar Binnen Code],
									[StaatMakelaar Buiten Code],
									[StaatMakelaar Bedrag],
									[StaatMakelaar Bedrag Euro],
									[StaatMakelaar Bedrag binnen],
									[StaatMakelaar Bedrag binnen Euro],
									[StaatMakelaar Bedrag buiten],
									[StaatMakelaar Bedrag buiten Euro]
								)
								VALUES
								(
									@StaatMakelaar_Factuur_credietnota,
									@StaatMakelaar_Nummer,
									@StaatMakelaar_Dossiernummer,
									@StaatMakelaar_Omschrijving,
									@StaatMakelaar_Commissie_Code,
									@StaatMakelaar_Binnen_Code,
									@StaatMakelaar_Buiten_Code,
									@StaatMakelaar_Bedrag,
									@StaatMakelaar_Bedrag_Euro,
									@StaatMakelaar_Bedrag_binnen,
									@StaatMakelaar_Bedrag_binnen_Euro,
									@StaatMakelaar_Bedrag_buiten,
									@StaatMakelaar_Bedrag_buiten_Euro
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@StaatMakelaar_Factuur_credietnota", OleDbType.UnsignedTinyInt).Value = StaatMakelaar_Factuur_credietnota;
					cmd.Parameters.Add("@StaatMakelaar_Nummer", OleDbType.Integer).Value = StaatMakelaar_Nummer == null ? (Object)DBNull.Value : StaatMakelaar_Nummer;
					cmd.Parameters.Add("@StaatMakelaar_Dossiernummer", OleDbType.Integer).Value = StaatMakelaar_Dossiernummer == null ? (Object)DBNull.Value : StaatMakelaar_Dossiernummer;
					cmd.Parameters.Add("@StaatMakelaar_Omschrijving", OleDbType.WChar).Value = StaatMakelaar_Omschrijving == null ? (Object)DBNull.Value : StaatMakelaar_Omschrijving;
					cmd.Parameters.Add("@StaatMakelaar_Commissie_Code", OleDbType.WChar).Value = StaatMakelaar_Commissie_Code == null ? (Object)DBNull.Value : StaatMakelaar_Commissie_Code;
					cmd.Parameters.Add("@StaatMakelaar_Binnen_Code", OleDbType.WChar).Value = StaatMakelaar_Binnen_Code == null ? (Object)DBNull.Value : StaatMakelaar_Binnen_Code;
					cmd.Parameters.Add("@StaatMakelaar_Buiten_Code", OleDbType.WChar).Value = StaatMakelaar_Buiten_Code == null ? (Object)DBNull.Value : StaatMakelaar_Buiten_Code;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag", OleDbType.Integer).Value = StaatMakelaar_Bedrag == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_Euro", OleDbType.Double).Value = StaatMakelaar_Bedrag_Euro == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_Euro;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_binnen", OleDbType.Integer).Value = StaatMakelaar_Bedrag_binnen == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_binnen;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_binnen_Euro", OleDbType.Double).Value = StaatMakelaar_Bedrag_binnen_Euro == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_binnen_Euro;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_buiten", OleDbType.Integer).Value = StaatMakelaar_Bedrag_buiten == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_buiten;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_buiten_Euro", OleDbType.Double).Value = StaatMakelaar_Bedrag_buiten_Euro == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_buiten_Euro;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					StaatMakelaar_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Staat makelaar]
								SET		[StaatMakelaar Factuur-credietnota] = @StaatMakelaar_Factuur_credietnota,
										[StaatMakelaar Nummer] = @StaatMakelaar_Nummer,
										[StaatMakelaar Dossiernummer] = @StaatMakelaar_Dossiernummer,
										[StaatMakelaar Omschrijving] = @StaatMakelaar_Omschrijving,
										[StaatMakelaar Commissie Code] = @StaatMakelaar_Commissie_Code,
										[StaatMakelaar Binnen Code] = @StaatMakelaar_Binnen_Code,
										[StaatMakelaar Buiten Code] = @StaatMakelaar_Buiten_Code,
										[StaatMakelaar Bedrag] = @StaatMakelaar_Bedrag,
										[StaatMakelaar Bedrag Euro] = @StaatMakelaar_Bedrag_Euro,
										[StaatMakelaar Bedrag binnen] = @StaatMakelaar_Bedrag_binnen,
										[StaatMakelaar Bedrag binnen Euro] = @StaatMakelaar_Bedrag_binnen_Euro,
										[StaatMakelaar Bedrag buiten] = @StaatMakelaar_Bedrag_buiten,
										[StaatMakelaar Bedrag buiten Euro] = @StaatMakelaar_Bedrag_buiten_Euro
								WHERE	[StaatMakelaar-ID] = @StaatMakelaar_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@StaatMakelaar_Factuur_credietnota", OleDbType.UnsignedTinyInt).Value = StaatMakelaar_Factuur_credietnota;
					cmd.Parameters.Add("@StaatMakelaar_Nummer", OleDbType.Integer).Value = StaatMakelaar_Nummer == null ? (Object)DBNull.Value : StaatMakelaar_Nummer;
					cmd.Parameters.Add("@StaatMakelaar_Dossiernummer", OleDbType.Integer).Value = StaatMakelaar_Dossiernummer == null ? (Object)DBNull.Value : StaatMakelaar_Dossiernummer;
					cmd.Parameters.Add("@StaatMakelaar_Omschrijving", OleDbType.WChar).Value = StaatMakelaar_Omschrijving == null ? (Object)DBNull.Value : StaatMakelaar_Omschrijving;
					cmd.Parameters.Add("@StaatMakelaar_Commissie_Code", OleDbType.WChar).Value = StaatMakelaar_Commissie_Code == null ? (Object)DBNull.Value : StaatMakelaar_Commissie_Code;
					cmd.Parameters.Add("@StaatMakelaar_Binnen_Code", OleDbType.WChar).Value = StaatMakelaar_Binnen_Code == null ? (Object)DBNull.Value : StaatMakelaar_Binnen_Code;
					cmd.Parameters.Add("@StaatMakelaar_Buiten_Code", OleDbType.WChar).Value = StaatMakelaar_Buiten_Code == null ? (Object)DBNull.Value : StaatMakelaar_Buiten_Code;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag", OleDbType.Integer).Value = StaatMakelaar_Bedrag == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_Euro", OleDbType.Double).Value = StaatMakelaar_Bedrag_Euro == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_Euro;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_binnen", OleDbType.Integer).Value = StaatMakelaar_Bedrag_binnen == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_binnen;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_binnen_Euro", OleDbType.Double).Value = StaatMakelaar_Bedrag_binnen_Euro == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_binnen_Euro;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_buiten", OleDbType.Integer).Value = StaatMakelaar_Bedrag_buiten == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_buiten;
					cmd.Parameters.Add("@StaatMakelaar_Bedrag_buiten_Euro", OleDbType.Double).Value = StaatMakelaar_Bedrag_buiten_Euro == null ? (Object)DBNull.Value : StaatMakelaar_Bedrag_buiten_Euro;
					cmd.Parameters.Add("@StaatMakelaar_ID", OleDbType.Integer).Value = StaatMakelaar_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int staatMakelaar_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Staat makelaar]
								WHERE	[StaatMakelaar-ID] = @StaatMakelaar_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@StaatMakelaar_ID", OleDbType.Integer).Value = staatMakelaar_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Staat_makelaar Get(int staatMakelaar_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[StaatMakelaar-ID],
										[StaatMakelaar Factuur-credietnota],
										[StaatMakelaar Nummer],
										[StaatMakelaar Dossiernummer],
										[StaatMakelaar Omschrijving],
										[StaatMakelaar Commissie Code],
										[StaatMakelaar Binnen Code],
										[StaatMakelaar Buiten Code],
										[StaatMakelaar Bedrag],
										[StaatMakelaar Bedrag Euro],
										[StaatMakelaar Bedrag binnen],
										[StaatMakelaar Bedrag binnen Euro],
										[StaatMakelaar Bedrag buiten],
										[StaatMakelaar Bedrag buiten Euro]
								FROM	[Staat makelaar]
								WHERE	[StaatMakelaar-ID] = @StaatMakelaar_ID;";

				Staat_makelaar staat_makelaar = new Staat_makelaar();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@StaatMakelaar_ID", OleDbType.Integer).Value = staatMakelaar_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							staat_makelaar.StaatMakelaar_ID = Convert.ToInt32(reader["StaatMakelaar-ID"]);
							staat_makelaar.StaatMakelaar_Factuur_credietnota = Convert.ToByte(reader["StaatMakelaar Factuur-credietnota"]);
							staat_makelaar.StaatMakelaar_Nummer = reader["StaatMakelaar Nummer"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["StaatMakelaar Nummer"]);
							staat_makelaar.StaatMakelaar_Dossiernummer = reader["StaatMakelaar Dossiernummer"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["StaatMakelaar Dossiernummer"]);
							staat_makelaar.StaatMakelaar_Omschrijving = reader["StaatMakelaar Omschrijving"] == DBNull.Value ? null : reader["StaatMakelaar Omschrijving"].ToString();
							staat_makelaar.StaatMakelaar_Commissie_Code = reader["StaatMakelaar Commissie Code"] == DBNull.Value ? null : reader["StaatMakelaar Commissie Code"].ToString();
							staat_makelaar.StaatMakelaar_Binnen_Code = reader["StaatMakelaar Binnen Code"] == DBNull.Value ? null : reader["StaatMakelaar Binnen Code"].ToString();
							staat_makelaar.StaatMakelaar_Buiten_Code = reader["StaatMakelaar Buiten Code"] == DBNull.Value ? null : reader["StaatMakelaar Buiten Code"].ToString();
							staat_makelaar.StaatMakelaar_Bedrag = reader["StaatMakelaar Bedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["StaatMakelaar Bedrag"]);
							staat_makelaar.StaatMakelaar_Bedrag_Euro = reader["StaatMakelaar Bedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["StaatMakelaar Bedrag Euro"]);
							staat_makelaar.StaatMakelaar_Bedrag_binnen = reader["StaatMakelaar Bedrag binnen"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["StaatMakelaar Bedrag binnen"]);
							staat_makelaar.StaatMakelaar_Bedrag_binnen_Euro = reader["StaatMakelaar Bedrag binnen Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["StaatMakelaar Bedrag binnen Euro"]);
							staat_makelaar.StaatMakelaar_Bedrag_buiten = reader["StaatMakelaar Bedrag buiten"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["StaatMakelaar Bedrag buiten"]);
							staat_makelaar.StaatMakelaar_Bedrag_buiten_Euro = reader["StaatMakelaar Bedrag buiten Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["StaatMakelaar Bedrag buiten Euro"]);
						}
					}
				}

				return staat_makelaar;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[StaatMakelaar-ID],
										[StaatMakelaar Factuur-credietnota],
										[StaatMakelaar Nummer],
										[StaatMakelaar Dossiernummer],
										[StaatMakelaar Omschrijving],
										[StaatMakelaar Commissie Code],
										[StaatMakelaar Binnen Code],
										[StaatMakelaar Buiten Code],
										[StaatMakelaar Bedrag],
										[StaatMakelaar Bedrag Euro],
										[StaatMakelaar Bedrag binnen],
										[StaatMakelaar Bedrag binnen Euro],
										[StaatMakelaar Bedrag buiten],
										[StaatMakelaar Bedrag buiten Euro]
								FROM	[Staat makelaar];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}