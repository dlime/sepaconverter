using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Factuur
	{
		#region Properties
		public int Factuur_ID { get; set; }
		public int Factuur_Firma_ID { get; set; }
		public int Factuur_Aard_ID { get; set; }
		public int Factuur_Commissie_ID { get; set; }
		public int Factuur_Makelaar_ID_binnen { get; set; }
		public int Factuur_Makelaar_ID_buiten { get; set; }
		public int Factuur_Gemeente_ID_dossier { get; set; }
		public int Factuur_Gemeente_ID_klant { get; set; }
		public int Factuur_Notaris_ID { get; set; }
		public byte Factuur_Factuur_credietnota { get; set; }
		public int? Factuur_Nummer { get; set; }
		public DateTime? Factuur_Datum { get; set; }
		public int? Factuur_Factuurnummer { get; set; }
		public bool Factuur_Aangetekend { get; set; }
		public int? Factuur_Dossiernummer { get; set; }
		public string Factuur_Straat_dossier { get; set; }
		public string Factuur_Naam { get; set; }
		public string Factuur_Straat_klant { get; set; }
		public int? Factuur_Aanspreektitel { get; set; }
		public string Factuur_BTW_nr { get; set; }
		public byte? Factuur_Commissie_binnen { get; set; }
		public byte? Factuur_Commissie_buiten { get; set; }
		public int? Factuur_Bedrag { get; set; }
		public double? Factuur_Bedrag_Euro { get; set; }
		public byte? Factuur_Coëfficiënt { get; set; }
		public int? Factuur_Kopersvoorschot { get; set; }
		public double? Factuur_Kopersvoorschot_Euro { get; set; }
		public float? Factuur_BTW { get; set; }
		public string Factuur_Rekeningnummer { get; set; }
		public string Factuur_Omschrijving { get; set; }
		public byte? Factuur_Lijnen { get; set; }
		public byte? Factuur_Betaling { get; set; }
		public string Factuur_Betalingmemo { get; set; }
		public DateTime? Factuur_Vervaldag { get; set; }
		public bool Factuur_Stortingsformulier { get; set; }
		public bool Factuur_Bij_advokaat { get; set; }
		public bool Factuur_Betwist { get; set; }
		public bool Factuur_Rappel_1G { get; set; }
		public bool Factuur_Rappel_2G { get; set; }
		public bool Factuur_Rappel_1A { get; set; }
		public bool Factuur_Rappel_2A { get; set; }
		public byte? Factuur_Af_te_drukken_rappel { get; set; }
		public string Factuur_Memo { get; set; }
		public string Factuur_Bankrekening { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Factuur]
								(
									[Factuur Factuur-credietnota],
									[Factuur Nummer],
									[Factuur Datum],
									[Factuur Factuurnummer],
									[Factuur Aangetekend],
									[Factuur Dossiernummer],
									[Factuur Straat dossier],
									[Factuur Naam],
									[Factuur Straat klant],
									[Factuur Aanspreektitel],
									[Factuur BTW-nr],
									[Factuur Commissie binnen],
									[Factuur Commissie buiten],
									[Factuur Bedrag],
									[Factuur Bedrag Euro],
									[Factuur Coëfficiënt],
									[Factuur Kopersvoorschot],
									[Factuur Kopersvoorschot Euro],
									[Factuur BTW],
									[Factuur Rekeningnummer],
									[Factuur Omschrijving],
									[Factuur Lijnen],
									[Factuur Betaling],
									[Factuur Betalingmemo],
									[Factuur Vervaldag],
									[Factuur Stortingsformulier],
									[Factuur Bij advokaat],
									[Factuur Betwist],
									[Factuur Rappel 1G],
									[Factuur Rappel 2G],
									[Factuur Rappel 1A],
									[Factuur Rappel 2A],
									[Factuur Af te drukken rappel],
									[Factuur Memo],
									[Factuur Bankrekening]
								)
								VALUES
								(
									@Factuur_Factuur_credietnota,
									@Factuur_Nummer,
									@Factuur_Datum,
									@Factuur_Factuurnummer,
									@Factuur_Aangetekend,
									@Factuur_Dossiernummer,
									@Factuur_Straat_dossier,
									@Factuur_Naam,
									@Factuur_Straat_klant,
									@Factuur_Aanspreektitel,
									@Factuur_BTW_nr,
									@Factuur_Commissie_binnen,
									@Factuur_Commissie_buiten,
									@Factuur_Bedrag,
									@Factuur_Bedrag_Euro,
									@Factuur_Coëfficiënt,
									@Factuur_Kopersvoorschot,
									@Factuur_Kopersvoorschot_Euro,
									@Factuur_BTW,
									@Factuur_Rekeningnummer,
									@Factuur_Omschrijving,
									@Factuur_Lijnen,
									@Factuur_Betaling,
									@Factuur_Betalingmemo,
									@Factuur_Vervaldag,
									@Factuur_Stortingsformulier,
									@Factuur_Bij_advokaat,
									@Factuur_Betwist,
									@Factuur_Rappel_1G,
									@Factuur_Rappel_2G,
									@Factuur_Rappel_1A,
									@Factuur_Rappel_2A,
									@Factuur_Af_te_drukken_rappel,
									@Factuur_Memo,
									@Factuur_Bankrekening
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Factuur_Factuur_credietnota", OleDbType.UnsignedTinyInt).Value = Factuur_Factuur_credietnota;
					cmd.Parameters.Add("@Factuur_Nummer", OleDbType.Integer).Value = Factuur_Nummer == null ? (Object)DBNull.Value : Factuur_Nummer;
					cmd.Parameters.Add("@Factuur_Datum", OleDbType.Date).Value = Factuur_Datum == null ? (Object)DBNull.Value : Factuur_Datum;
					cmd.Parameters.Add("@Factuur_Factuurnummer", OleDbType.Integer).Value = Factuur_Factuurnummer == null ? (Object)DBNull.Value : Factuur_Factuurnummer;
					cmd.Parameters.Add("@Factuur_Aangetekend", OleDbType.Boolean).Value = Factuur_Aangetekend;
					cmd.Parameters.Add("@Factuur_Dossiernummer", OleDbType.Integer).Value = Factuur_Dossiernummer == null ? (Object)DBNull.Value : Factuur_Dossiernummer;
					cmd.Parameters.Add("@Factuur_Straat_dossier", OleDbType.WChar).Value = Factuur_Straat_dossier == null ? (Object)DBNull.Value : Factuur_Straat_dossier;
					cmd.Parameters.Add("@Factuur_Naam", OleDbType.WChar).Value = Factuur_Naam == null ? (Object)DBNull.Value : Factuur_Naam;
					cmd.Parameters.Add("@Factuur_Straat_klant", OleDbType.WChar).Value = Factuur_Straat_klant == null ? (Object)DBNull.Value : Factuur_Straat_klant;
					cmd.Parameters.Add("@Factuur_Aanspreektitel", OleDbType.Integer).Value = Factuur_Aanspreektitel == null ? (Object)DBNull.Value : Factuur_Aanspreektitel;
					cmd.Parameters.Add("@Factuur_BTW_nr", OleDbType.WChar).Value = Factuur_BTW_nr == null ? (Object)DBNull.Value : Factuur_BTW_nr;
					cmd.Parameters.Add("@Factuur_Commissie_binnen", OleDbType.UnsignedTinyInt).Value = Factuur_Commissie_binnen == null ? (Object)DBNull.Value : Factuur_Commissie_binnen;
					cmd.Parameters.Add("@Factuur_Commissie_buiten", OleDbType.UnsignedTinyInt).Value = Factuur_Commissie_buiten == null ? (Object)DBNull.Value : Factuur_Commissie_buiten;
					cmd.Parameters.Add("@Factuur_Bedrag", OleDbType.Integer).Value = Factuur_Bedrag == null ? (Object)DBNull.Value : Factuur_Bedrag;
					cmd.Parameters.Add("@Factuur_Bedrag_Euro", OleDbType.Double).Value = Factuur_Bedrag_Euro == null ? (Object)DBNull.Value : Factuur_Bedrag_Euro;
					cmd.Parameters.Add("@Factuur_Coëfficiënt", OleDbType.UnsignedTinyInt).Value = Factuur_Coëfficiënt == null ? (Object)DBNull.Value : Factuur_Coëfficiënt;
					cmd.Parameters.Add("@Factuur_Kopersvoorschot", OleDbType.Integer).Value = Factuur_Kopersvoorschot == null ? (Object)DBNull.Value : Factuur_Kopersvoorschot;
					cmd.Parameters.Add("@Factuur_Kopersvoorschot_Euro", OleDbType.Double).Value = Factuur_Kopersvoorschot_Euro == null ? (Object)DBNull.Value : Factuur_Kopersvoorschot_Euro;
					cmd.Parameters.Add("@Factuur_BTW", OleDbType.Single).Value = Factuur_BTW == null ? (Object)DBNull.Value : Factuur_BTW;
					cmd.Parameters.Add("@Factuur_Rekeningnummer", OleDbType.WChar).Value = Factuur_Rekeningnummer == null ? (Object)DBNull.Value : Factuur_Rekeningnummer;
					cmd.Parameters.Add("@Factuur_Omschrijving", OleDbType.WChar).Value = Factuur_Omschrijving == null ? (Object)DBNull.Value : Factuur_Omschrijving;
					cmd.Parameters.Add("@Factuur_Lijnen", OleDbType.UnsignedTinyInt).Value = Factuur_Lijnen == null ? (Object)DBNull.Value : Factuur_Lijnen;
					cmd.Parameters.Add("@Factuur_Betaling", OleDbType.UnsignedTinyInt).Value = Factuur_Betaling == null ? (Object)DBNull.Value : Factuur_Betaling;
					cmd.Parameters.Add("@Factuur_Betalingmemo", OleDbType.WChar).Value = Factuur_Betalingmemo == null ? (Object)DBNull.Value : Factuur_Betalingmemo;
					cmd.Parameters.Add("@Factuur_Vervaldag", OleDbType.Date).Value = Factuur_Vervaldag == null ? (Object)DBNull.Value : Factuur_Vervaldag;
					cmd.Parameters.Add("@Factuur_Stortingsformulier", OleDbType.Boolean).Value = Factuur_Stortingsformulier;
					cmd.Parameters.Add("@Factuur_Bij_advokaat", OleDbType.Boolean).Value = Factuur_Bij_advokaat;
					cmd.Parameters.Add("@Factuur_Betwist", OleDbType.Boolean).Value = Factuur_Betwist;
					cmd.Parameters.Add("@Factuur_Rappel_1G", OleDbType.Boolean).Value = Factuur_Rappel_1G;
					cmd.Parameters.Add("@Factuur_Rappel_2G", OleDbType.Boolean).Value = Factuur_Rappel_2G;
					cmd.Parameters.Add("@Factuur_Rappel_1A", OleDbType.Boolean).Value = Factuur_Rappel_1A;
					cmd.Parameters.Add("@Factuur_Rappel_2A", OleDbType.Boolean).Value = Factuur_Rappel_2A;
					cmd.Parameters.Add("@Factuur_Af_te_drukken_rappel", OleDbType.UnsignedTinyInt).Value = Factuur_Af_te_drukken_rappel == null ? (Object)DBNull.Value : Factuur_Af_te_drukken_rappel;
					cmd.Parameters.Add("@Factuur_Memo", OleDbType.WChar).Value = Factuur_Memo == null ? (Object)DBNull.Value : Factuur_Memo;
					cmd.Parameters.Add("@Factuur_Bankrekening", OleDbType.WChar).Value = Factuur_Bankrekening;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Factuur_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Factuur]
								SET		[Factuur Factuur-credietnota] = @Factuur_Factuur_credietnota,
										[Factuur Nummer] = @Factuur_Nummer,
										[Factuur Datum] = @Factuur_Datum,
										[Factuur Factuurnummer] = @Factuur_Factuurnummer,
										[Factuur Aangetekend] = @Factuur_Aangetekend,
										[Factuur Dossiernummer] = @Factuur_Dossiernummer,
										[Factuur Straat dossier] = @Factuur_Straat_dossier,
										[Factuur Naam] = @Factuur_Naam,
										[Factuur Straat klant] = @Factuur_Straat_klant,
										[Factuur Aanspreektitel] = @Factuur_Aanspreektitel,
										[Factuur BTW-nr] = @Factuur_BTW_nr,
										[Factuur Commissie binnen] = @Factuur_Commissie_binnen,
										[Factuur Commissie buiten] = @Factuur_Commissie_buiten,
										[Factuur Bedrag] = @Factuur_Bedrag,
										[Factuur Bedrag Euro] = @Factuur_Bedrag_Euro,
										[Factuur Coëfficiënt] = @Factuur_Coëfficiënt,
										[Factuur Kopersvoorschot] = @Factuur_Kopersvoorschot,
										[Factuur Kopersvoorschot Euro] = @Factuur_Kopersvoorschot_Euro,
										[Factuur BTW] = @Factuur_BTW,
										[Factuur Rekeningnummer] = @Factuur_Rekeningnummer,
										[Factuur Omschrijving] = @Factuur_Omschrijving,
										[Factuur Lijnen] = @Factuur_Lijnen,
										[Factuur Betaling] = @Factuur_Betaling,
										[Factuur Betalingmemo] = @Factuur_Betalingmemo,
										[Factuur Vervaldag] = @Factuur_Vervaldag,
										[Factuur Stortingsformulier] = @Factuur_Stortingsformulier,
										[Factuur Bij advokaat] = @Factuur_Bij_advokaat,
										[Factuur Betwist] = @Factuur_Betwist,
										[Factuur Rappel 1G] = @Factuur_Rappel_1G,
										[Factuur Rappel 2G] = @Factuur_Rappel_2G,
										[Factuur Rappel 1A] = @Factuur_Rappel_1A,
										[Factuur Rappel 2A] = @Factuur_Rappel_2A,
										[Factuur Af te drukken rappel] = @Factuur_Af_te_drukken_rappel,
										[Factuur Memo] = @Factuur_Memo,
										[Factuur Bankrekening] = @Factuur_Bankrekening
								WHERE	[Factuur-ID] = @Factuur_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Factuur_Firma_ID", OleDbType.Integer).Value = Factuur_Firma_ID;
					cmd.Parameters.Add("@Factuur_Aard_ID", OleDbType.Integer).Value = Factuur_Aard_ID;
					cmd.Parameters.Add("@Factuur_Commissie_ID", OleDbType.Integer).Value = Factuur_Commissie_ID;
					cmd.Parameters.Add("@Factuur_Makelaar_ID_binnen", OleDbType.Integer).Value = Factuur_Makelaar_ID_binnen;
					cmd.Parameters.Add("@Factuur_Makelaar_ID_buiten", OleDbType.Integer).Value = Factuur_Makelaar_ID_buiten;
					cmd.Parameters.Add("@Factuur_Gemeente_ID_dossier", OleDbType.Integer).Value = Factuur_Gemeente_ID_dossier;
					cmd.Parameters.Add("@Factuur_Gemeente_ID_klant", OleDbType.Integer).Value = Factuur_Gemeente_ID_klant;
					cmd.Parameters.Add("@Factuur_Notaris_ID", OleDbType.Integer).Value = Factuur_Notaris_ID;
					cmd.Parameters.Add("@Factuur_Factuur_credietnota", OleDbType.UnsignedTinyInt).Value = Factuur_Factuur_credietnota;
					cmd.Parameters.Add("@Factuur_Nummer", OleDbType.Integer).Value = Factuur_Nummer == null ? (Object)DBNull.Value : Factuur_Nummer;
					cmd.Parameters.Add("@Factuur_Datum", OleDbType.Date).Value = Factuur_Datum == null ? (Object)DBNull.Value : Factuur_Datum;
					cmd.Parameters.Add("@Factuur_Factuurnummer", OleDbType.Integer).Value = Factuur_Factuurnummer == null ? (Object)DBNull.Value : Factuur_Factuurnummer;
					cmd.Parameters.Add("@Factuur_Aangetekend", OleDbType.Boolean).Value = Factuur_Aangetekend;
					cmd.Parameters.Add("@Factuur_Dossiernummer", OleDbType.Integer).Value = Factuur_Dossiernummer == null ? (Object)DBNull.Value : Factuur_Dossiernummer;
					cmd.Parameters.Add("@Factuur_Straat_dossier", OleDbType.WChar).Value = Factuur_Straat_dossier == null ? (Object)DBNull.Value : Factuur_Straat_dossier;
					cmd.Parameters.Add("@Factuur_Naam", OleDbType.WChar).Value = Factuur_Naam == null ? (Object)DBNull.Value : Factuur_Naam;
					cmd.Parameters.Add("@Factuur_Straat_klant", OleDbType.WChar).Value = Factuur_Straat_klant == null ? (Object)DBNull.Value : Factuur_Straat_klant;
					cmd.Parameters.Add("@Factuur_Aanspreektitel", OleDbType.Integer).Value = Factuur_Aanspreektitel == null ? (Object)DBNull.Value : Factuur_Aanspreektitel;
					cmd.Parameters.Add("@Factuur_BTW_nr", OleDbType.WChar).Value = Factuur_BTW_nr == null ? (Object)DBNull.Value : Factuur_BTW_nr;
					cmd.Parameters.Add("@Factuur_Commissie_binnen", OleDbType.UnsignedTinyInt).Value = Factuur_Commissie_binnen == null ? (Object)DBNull.Value : Factuur_Commissie_binnen;
					cmd.Parameters.Add("@Factuur_Commissie_buiten", OleDbType.UnsignedTinyInt).Value = Factuur_Commissie_buiten == null ? (Object)DBNull.Value : Factuur_Commissie_buiten;
					cmd.Parameters.Add("@Factuur_Bedrag", OleDbType.Integer).Value = Factuur_Bedrag == null ? (Object)DBNull.Value : Factuur_Bedrag;
					cmd.Parameters.Add("@Factuur_Bedrag_Euro", OleDbType.Double).Value = Factuur_Bedrag_Euro == null ? (Object)DBNull.Value : Factuur_Bedrag_Euro;
					cmd.Parameters.Add("@Factuur_Coëfficiënt", OleDbType.UnsignedTinyInt).Value = Factuur_Coëfficiënt == null ? (Object)DBNull.Value : Factuur_Coëfficiënt;
					cmd.Parameters.Add("@Factuur_Kopersvoorschot", OleDbType.Integer).Value = Factuur_Kopersvoorschot == null ? (Object)DBNull.Value : Factuur_Kopersvoorschot;
					cmd.Parameters.Add("@Factuur_Kopersvoorschot_Euro", OleDbType.Double).Value = Factuur_Kopersvoorschot_Euro == null ? (Object)DBNull.Value : Factuur_Kopersvoorschot_Euro;
					cmd.Parameters.Add("@Factuur_BTW", OleDbType.Single).Value = Factuur_BTW == null ? (Object)DBNull.Value : Factuur_BTW;
					cmd.Parameters.Add("@Factuur_Rekeningnummer", OleDbType.WChar).Value = Factuur_Rekeningnummer == null ? (Object)DBNull.Value : Factuur_Rekeningnummer;
					cmd.Parameters.Add("@Factuur_Omschrijving", OleDbType.WChar).Value = Factuur_Omschrijving == null ? (Object)DBNull.Value : Factuur_Omschrijving;
					cmd.Parameters.Add("@Factuur_Lijnen", OleDbType.UnsignedTinyInt).Value = Factuur_Lijnen == null ? (Object)DBNull.Value : Factuur_Lijnen;
					cmd.Parameters.Add("@Factuur_Betaling", OleDbType.UnsignedTinyInt).Value = Factuur_Betaling == null ? (Object)DBNull.Value : Factuur_Betaling;
					cmd.Parameters.Add("@Factuur_Betalingmemo", OleDbType.WChar).Value = Factuur_Betalingmemo == null ? (Object)DBNull.Value : Factuur_Betalingmemo;
					cmd.Parameters.Add("@Factuur_Vervaldag", OleDbType.Date).Value = Factuur_Vervaldag == null ? (Object)DBNull.Value : Factuur_Vervaldag;
					cmd.Parameters.Add("@Factuur_Stortingsformulier", OleDbType.Boolean).Value = Factuur_Stortingsformulier;
					cmd.Parameters.Add("@Factuur_Bij_advokaat", OleDbType.Boolean).Value = Factuur_Bij_advokaat;
					cmd.Parameters.Add("@Factuur_Betwist", OleDbType.Boolean).Value = Factuur_Betwist;
					cmd.Parameters.Add("@Factuur_Rappel_1G", OleDbType.Boolean).Value = Factuur_Rappel_1G;
					cmd.Parameters.Add("@Factuur_Rappel_2G", OleDbType.Boolean).Value = Factuur_Rappel_2G;
					cmd.Parameters.Add("@Factuur_Rappel_1A", OleDbType.Boolean).Value = Factuur_Rappel_1A;
					cmd.Parameters.Add("@Factuur_Rappel_2A", OleDbType.Boolean).Value = Factuur_Rappel_2A;
					cmd.Parameters.Add("@Factuur_Af_te_drukken_rappel", OleDbType.UnsignedTinyInt).Value = Factuur_Af_te_drukken_rappel == null ? (Object)DBNull.Value : Factuur_Af_te_drukken_rappel;
					cmd.Parameters.Add("@Factuur_Memo", OleDbType.WChar).Value = Factuur_Memo == null ? (Object)DBNull.Value : Factuur_Memo;
					cmd.Parameters.Add("@Factuur_Bankrekening", OleDbType.WChar).Value = Factuur_Bankrekening;
					cmd.Parameters.Add("@Factuur_ID", OleDbType.Integer).Value = Factuur_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int factuur_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Factuur]
								WHERE	[Factuur-ID] = @Factuur_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Factuur_ID", OleDbType.Integer).Value = factuur_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Factuur Get(int factuur_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Factuur-ID],
										[Factuur Firma-ID],
										[Factuur Aard-ID],
										[Factuur Commissie-ID],
										[Factuur Makelaar-ID binnen],
										[Factuur Makelaar-ID buiten],
										[Factuur Gemeente-ID dossier],
										[Factuur Gemeente-ID klant],
										[Factuur Notaris-ID],
										[Factuur Factuur-credietnota],
										[Factuur Nummer],
										[Factuur Datum],
										[Factuur Factuurnummer],
										[Factuur Aangetekend],
										[Factuur Dossiernummer],
										[Factuur Straat dossier],
										[Factuur Naam],
										[Factuur Straat klant],
										[Factuur Aanspreektitel],
										[Factuur BTW-nr],
										[Factuur Commissie binnen],
										[Factuur Commissie buiten],
										[Factuur Bedrag],
										[Factuur Bedrag Euro],
										[Factuur Coëfficiënt],
										[Factuur Kopersvoorschot],
										[Factuur Kopersvoorschot Euro],
										[Factuur BTW],
										[Factuur Rekeningnummer],
										[Factuur Omschrijving],
										[Factuur Lijnen],
										[Factuur Betaling],
										[Factuur Betalingmemo],
										[Factuur Vervaldag],
										[Factuur Stortingsformulier],
										[Factuur Bij advokaat],
										[Factuur Betwist],
										[Factuur Rappel 1G],
										[Factuur Rappel 2G],
										[Factuur Rappel 1A],
										[Factuur Rappel 2A],
										[Factuur Af te drukken rappel],
										[Factuur Memo],
										[Factuur Bankrekening]
								FROM	[Factuur]
								WHERE	[Factuur-ID] = @Factuur_ID;";

				Factuur factuur = new Factuur();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Factuur_ID", OleDbType.Integer).Value = factuur_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							factuur.Factuur_ID = Convert.ToInt32(reader["Factuur-ID"]);
							factuur.Factuur_Firma_ID = Convert.ToInt32(reader["Factuur Firma-ID"]);
							factuur.Factuur_Aard_ID = Convert.ToInt32(reader["Factuur Aard-ID"]);
							factuur.Factuur_Commissie_ID = Convert.ToInt32(reader["Factuur Commissie-ID"]);
							factuur.Factuur_Makelaar_ID_binnen = Convert.ToInt32(reader["Factuur Makelaar-ID binnen"]);
							factuur.Factuur_Makelaar_ID_buiten = Convert.ToInt32(reader["Factuur Makelaar-ID buiten"]);
							factuur.Factuur_Gemeente_ID_dossier = Convert.ToInt32(reader["Factuur Gemeente-ID dossier"]);
							factuur.Factuur_Gemeente_ID_klant = Convert.ToInt32(reader["Factuur Gemeente-ID klant"]);
							factuur.Factuur_Notaris_ID = Convert.ToInt32(reader["Factuur Notaris-ID"]);
							factuur.Factuur_Factuur_credietnota = Convert.ToByte(reader["Factuur Factuur-credietnota"]);
							factuur.Factuur_Nummer = reader["Factuur Nummer"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Factuur Nummer"]);
							factuur.Factuur_Datum = reader["Factuur Datum"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Factuur Datum"]);
							factuur.Factuur_Factuurnummer = reader["Factuur Factuurnummer"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Factuur Factuurnummer"]);
							factuur.Factuur_Aangetekend = Convert.ToBoolean(reader["Factuur Aangetekend"]);
							factuur.Factuur_Dossiernummer = reader["Factuur Dossiernummer"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Factuur Dossiernummer"]);
							factuur.Factuur_Straat_dossier = reader["Factuur Straat dossier"] == DBNull.Value ? null : reader["Factuur Straat dossier"].ToString();
							factuur.Factuur_Naam = reader["Factuur Naam"] == DBNull.Value ? null : reader["Factuur Naam"].ToString();
							factuur.Factuur_Straat_klant = reader["Factuur Straat klant"] == DBNull.Value ? null : reader["Factuur Straat klant"].ToString();
							factuur.Factuur_Aanspreektitel = reader["Factuur Aanspreektitel"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Factuur Aanspreektitel"]);
							factuur.Factuur_BTW_nr = reader["Factuur BTW-nr"] == DBNull.Value ? null : reader["Factuur BTW-nr"].ToString();
							factuur.Factuur_Commissie_binnen = reader["Factuur Commissie binnen"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Factuur Commissie binnen"]);
							factuur.Factuur_Commissie_buiten = reader["Factuur Commissie buiten"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Factuur Commissie buiten"]);
							factuur.Factuur_Bedrag = reader["Factuur Bedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Factuur Bedrag"]);
							factuur.Factuur_Bedrag_Euro = reader["Factuur Bedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Factuur Bedrag Euro"]);
							factuur.Factuur_Coëfficiënt = reader["Factuur Coëfficiënt"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Factuur Coëfficiënt"]);
							factuur.Factuur_Kopersvoorschot = reader["Factuur Kopersvoorschot"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Factuur Kopersvoorschot"]);
							factuur.Factuur_Kopersvoorschot_Euro = reader["Factuur Kopersvoorschot Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Factuur Kopersvoorschot Euro"]);
							factuur.Factuur_BTW = reader["Factuur BTW"] == DBNull.Value ? (float?)null : Convert.ToSingle(reader["Factuur BTW"]);
							factuur.Factuur_Rekeningnummer = reader["Factuur Rekeningnummer"] == DBNull.Value ? null : reader["Factuur Rekeningnummer"].ToString();
							factuur.Factuur_Omschrijving = reader["Factuur Omschrijving"] == DBNull.Value ? null : reader["Factuur Omschrijving"].ToString();
							factuur.Factuur_Lijnen = reader["Factuur Lijnen"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Factuur Lijnen"]);
							factuur.Factuur_Betaling = reader["Factuur Betaling"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Factuur Betaling"]);
							factuur.Factuur_Betalingmemo = reader["Factuur Betalingmemo"] == DBNull.Value ? null : reader["Factuur Betalingmemo"].ToString();
							factuur.Factuur_Vervaldag = reader["Factuur Vervaldag"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Factuur Vervaldag"]);
							factuur.Factuur_Stortingsformulier = Convert.ToBoolean(reader["Factuur Stortingsformulier"]);
							factuur.Factuur_Bij_advokaat = Convert.ToBoolean(reader["Factuur Bij advokaat"]);
							factuur.Factuur_Betwist = Convert.ToBoolean(reader["Factuur Betwist"]);
							factuur.Factuur_Rappel_1G = Convert.ToBoolean(reader["Factuur Rappel 1G"]);
							factuur.Factuur_Rappel_2G = Convert.ToBoolean(reader["Factuur Rappel 2G"]);
							factuur.Factuur_Rappel_1A = Convert.ToBoolean(reader["Factuur Rappel 1A"]);
							factuur.Factuur_Rappel_2A = Convert.ToBoolean(reader["Factuur Rappel 2A"]);
							factuur.Factuur_Af_te_drukken_rappel = reader["Factuur Af te drukken rappel"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Factuur Af te drukken rappel"]);
							factuur.Factuur_Memo = reader["Factuur Memo"] == DBNull.Value ? null : reader["Factuur Memo"].ToString();
							factuur.Factuur_Bankrekening = reader["Factuur Bankrekening"].ToString();
						}
					}
				}

				return factuur;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Factuur-ID],
										[Factuur Firma-ID],
										[Factuur Aard-ID],
										[Factuur Commissie-ID],
										[Factuur Makelaar-ID binnen],
										[Factuur Makelaar-ID buiten],
										[Factuur Gemeente-ID dossier],
										[Factuur Gemeente-ID klant],
										[Factuur Notaris-ID],
										[Factuur Factuur-credietnota],
										[Factuur Nummer],
										[Factuur Datum],
										[Factuur Factuurnummer],
										[Factuur Aangetekend],
										[Factuur Dossiernummer],
										[Factuur Straat dossier],
										[Factuur Naam],
										[Factuur Straat klant],
										[Factuur Aanspreektitel],
										[Factuur BTW-nr],
										[Factuur Commissie binnen],
										[Factuur Commissie buiten],
										[Factuur Bedrag],
										[Factuur Bedrag Euro],
										[Factuur Coëfficiënt],
										[Factuur Kopersvoorschot],
										[Factuur Kopersvoorschot Euro],
										[Factuur BTW],
										[Factuur Rekeningnummer],
										[Factuur Omschrijving],
										[Factuur Lijnen],
										[Factuur Betaling],
										[Factuur Betalingmemo],
										[Factuur Vervaldag],
										[Factuur Stortingsformulier],
										[Factuur Bij advokaat],
										[Factuur Betwist],
										[Factuur Rappel 1G],
										[Factuur Rappel 2G],
										[Factuur Rappel 1A],
										[Factuur Rappel 2A],
										[Factuur Af te drukken rappel],
										[Factuur Memo],
										[Factuur Bankrekening]
								FROM	[Factuur];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}