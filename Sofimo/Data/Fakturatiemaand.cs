using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Fakturatiemaand
	{
		#region Properties
		public int Fakturatiemaand_ID { get; set; }
		public DateTime? Fakturatiemaand_Datum { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Fakturatiemaand]
								(
									[Fakturatiemaand Datum]
								)
								VALUES
								(
									@Fakturatiemaand_Datum
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Fakturatiemaand_Datum", OleDbType.Date).Value = Fakturatiemaand_Datum == null ? (Object)DBNull.Value : Fakturatiemaand_Datum;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Fakturatiemaand_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Fakturatiemaand]
								SET		[Fakturatiemaand Datum] = @Fakturatiemaand_Datum
								WHERE	[Fakturatiemaand-ID] = @Fakturatiemaand_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Fakturatiemaand_Datum", OleDbType.Date).Value = Fakturatiemaand_Datum == null ? (Object)DBNull.Value : Fakturatiemaand_Datum;
					cmd.Parameters.Add("@Fakturatiemaand_ID", OleDbType.Integer).Value = Fakturatiemaand_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int fakturatiemaand_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Fakturatiemaand]
								WHERE	[Fakturatiemaand-ID] = @Fakturatiemaand_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Fakturatiemaand_ID", OleDbType.Integer).Value = fakturatiemaand_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Fakturatiemaand Get(int fakturatiemaand_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Fakturatiemaand-ID],
										[Fakturatiemaand Datum]
								FROM	[Fakturatiemaand]
								WHERE	[Fakturatiemaand-ID] = @Fakturatiemaand_ID;";

				Fakturatiemaand fakturatiemaand = new Fakturatiemaand();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Fakturatiemaand_ID", OleDbType.Integer).Value = fakturatiemaand_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							fakturatiemaand.Fakturatiemaand_ID = Convert.ToInt32(reader["Fakturatiemaand-ID"]);
							fakturatiemaand.Fakturatiemaand_Datum = reader["Fakturatiemaand Datum"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Fakturatiemaand Datum"]);
						}
					}
				}

				return fakturatiemaand;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Fakturatiemaand-ID],
										[Fakturatiemaand Datum]
								FROM	[Fakturatiemaand];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}