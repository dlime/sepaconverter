using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Omzet_per_maand
	{
		#region Properties
		public int OmzetMaandID { get; set; }
		public string OmzetMaandMakelaarCode { get; set; }
		public byte? OmzetMaand { get; set; }
		public int? OmzetMaandBedrag { get; set; }
		public double? OmzetMaandBedragEuro { get; set; }
		public string OmzetMaandNaam { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Omzet per maand]
								(
									[OmzetMaandMakelaarCode],
									[OmzetMaand],
									[OmzetMaandBedrag],
									[OmzetMaandBedragEuro],
									[OmzetMaandNaam]
								)
								VALUES
								(
									@OmzetMaandMakelaarCode,
									@OmzetMaand,
									@OmzetMaandBedrag,
									@OmzetMaandBedragEuro,
									@OmzetMaandNaam
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzetMaandMakelaarCode", OleDbType.WChar).Value = OmzetMaandMakelaarCode == null ? (Object)DBNull.Value : OmzetMaandMakelaarCode;
					cmd.Parameters.Add("@OmzetMaand", OleDbType.UnsignedTinyInt).Value = OmzetMaand == null ? (Object)DBNull.Value : OmzetMaand;
					cmd.Parameters.Add("@OmzetMaandBedrag", OleDbType.Integer).Value = OmzetMaandBedrag == null ? (Object)DBNull.Value : OmzetMaandBedrag;
					cmd.Parameters.Add("@OmzetMaandBedragEuro", OleDbType.Double).Value = OmzetMaandBedragEuro == null ? (Object)DBNull.Value : OmzetMaandBedragEuro;
					cmd.Parameters.Add("@OmzetMaandNaam", OleDbType.WChar).Value = OmzetMaandNaam == null ? (Object)DBNull.Value : OmzetMaandNaam;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					OmzetMaandID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Omzet per maand]
								SET		[OmzetMaandMakelaarCode] = @OmzetMaandMakelaarCode,
										[OmzetMaand] = @OmzetMaand,
										[OmzetMaandBedrag] = @OmzetMaandBedrag,
										[OmzetMaandBedragEuro] = @OmzetMaandBedragEuro,
										[OmzetMaandNaam] = @OmzetMaandNaam
								WHERE	[OmzetMaandID] = @OmzetMaandID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzetMaandMakelaarCode", OleDbType.WChar).Value = OmzetMaandMakelaarCode == null ? (Object)DBNull.Value : OmzetMaandMakelaarCode;
					cmd.Parameters.Add("@OmzetMaand", OleDbType.UnsignedTinyInt).Value = OmzetMaand == null ? (Object)DBNull.Value : OmzetMaand;
					cmd.Parameters.Add("@OmzetMaandBedrag", OleDbType.Integer).Value = OmzetMaandBedrag == null ? (Object)DBNull.Value : OmzetMaandBedrag;
					cmd.Parameters.Add("@OmzetMaandBedragEuro", OleDbType.Double).Value = OmzetMaandBedragEuro == null ? (Object)DBNull.Value : OmzetMaandBedragEuro;
					cmd.Parameters.Add("@OmzetMaandNaam", OleDbType.WChar).Value = OmzetMaandNaam == null ? (Object)DBNull.Value : OmzetMaandNaam;
					cmd.Parameters.Add("@OmzetMaandID", OleDbType.Integer).Value = OmzetMaandID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int omzetMaandID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Omzet per maand]
								WHERE	[OmzetMaandID] = @OmzetMaandID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzetMaandID", OleDbType.Integer).Value = omzetMaandID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Omzet_per_maand Get(int omzetMaandID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[OmzetMaandID],
										[OmzetMaandMakelaarCode],
										[OmzetMaand],
										[OmzetMaandBedrag],
										[OmzetMaandBedragEuro],
										[OmzetMaandNaam]
								FROM	[Omzet per maand]
								WHERE	[OmzetMaandID] = @OmzetMaandID;";

				Omzet_per_maand omzet_per_maand = new Omzet_per_maand();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzetMaandID", OleDbType.Integer).Value = omzetMaandID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							omzet_per_maand.OmzetMaandID = Convert.ToInt32(reader["OmzetMaandID"]);
							omzet_per_maand.OmzetMaandMakelaarCode = reader["OmzetMaandMakelaarCode"] == DBNull.Value ? null : reader["OmzetMaandMakelaarCode"].ToString();
							omzet_per_maand.OmzetMaand = reader["OmzetMaand"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["OmzetMaand"]);
							omzet_per_maand.OmzetMaandBedrag = reader["OmzetMaandBedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["OmzetMaandBedrag"]);
							omzet_per_maand.OmzetMaandBedragEuro = reader["OmzetMaandBedragEuro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["OmzetMaandBedragEuro"]);
							omzet_per_maand.OmzetMaandNaam = reader["OmzetMaandNaam"] == DBNull.Value ? null : reader["OmzetMaandNaam"].ToString();
						}
					}
				}

				return omzet_per_maand;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[OmzetMaandID],
										[OmzetMaandMakelaarCode],
										[OmzetMaand],
										[OmzetMaandBedrag],
										[OmzetMaandBedragEuro],
										[OmzetMaandNaam]
								FROM	[Omzet per maand];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}