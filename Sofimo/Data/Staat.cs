using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Staat
	{
		#region Properties
		public int Staat_ID { get; set; }
		public int? Staat_Commissie_ID { get; set; }
		public int? Staat_Makelaar_ID { get; set; }
		public DateTime? Staat_Datum { get; set; }
		public byte? Staat_Commissie { get; set; }
		public int? Staat_Commissiebedrag { get; set; }
		public double? Staat_Commissiebedrag_Euro { get; set; }
		public int? Staat_Bedrag { get; set; }
		public double? Staat_Bedrag_Euro { get; set; }
		public string Staat_Omschrijving { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Staat]
								(
									[Staat Commissie-ID],
									[Staat Makelaar-ID],
									[Staat Datum],
									[Staat Commissie],
									[Staat Commissiebedrag],
									[Staat Commissiebedrag Euro],
									[Staat Bedrag],
									[Staat Bedrag Euro],
									[Staat Omschrijving]
								)
								VALUES
								(
									@Staat_Commissie_ID,
									@Staat_Makelaar_ID,
									@Staat_Datum,
									@Staat_Commissie,
									@Staat_Commissiebedrag,
									@Staat_Commissiebedrag_Euro,
									@Staat_Bedrag,
									@Staat_Bedrag_Euro,
									@Staat_Omschrijving
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Staat_Commissie_ID", OleDbType.Integer).Value = Staat_Commissie_ID == null ? (Object)DBNull.Value : Staat_Commissie_ID;
					cmd.Parameters.Add("@Staat_Makelaar_ID", OleDbType.Integer).Value = Staat_Makelaar_ID == null ? (Object)DBNull.Value : Staat_Makelaar_ID;
					cmd.Parameters.Add("@Staat_Datum", OleDbType.Date).Value = Staat_Datum == null ? (Object)DBNull.Value : Staat_Datum;
					cmd.Parameters.Add("@Staat_Commissie", OleDbType.UnsignedTinyInt).Value = Staat_Commissie == null ? (Object)DBNull.Value : Staat_Commissie;
					cmd.Parameters.Add("@Staat_Commissiebedrag", OleDbType.Integer).Value = Staat_Commissiebedrag == null ? (Object)DBNull.Value : Staat_Commissiebedrag;
					cmd.Parameters.Add("@Staat_Commissiebedrag_Euro", OleDbType.Double).Value = Staat_Commissiebedrag_Euro == null ? (Object)DBNull.Value : Staat_Commissiebedrag_Euro;
					cmd.Parameters.Add("@Staat_Bedrag", OleDbType.Integer).Value = Staat_Bedrag == null ? (Object)DBNull.Value : Staat_Bedrag;
					cmd.Parameters.Add("@Staat_Bedrag_Euro", OleDbType.Double).Value = Staat_Bedrag_Euro == null ? (Object)DBNull.Value : Staat_Bedrag_Euro;
					cmd.Parameters.Add("@Staat_Omschrijving", OleDbType.WChar).Value = Staat_Omschrijving == null ? (Object)DBNull.Value : Staat_Omschrijving;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Staat_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Staat]
								SET		[Staat Commissie-ID] = @Staat_Commissie_ID,
										[Staat Makelaar-ID] = @Staat_Makelaar_ID,
										[Staat Datum] = @Staat_Datum,
										[Staat Commissie] = @Staat_Commissie,
										[Staat Commissiebedrag] = @Staat_Commissiebedrag,
										[Staat Commissiebedrag Euro] = @Staat_Commissiebedrag_Euro,
										[Staat Bedrag] = @Staat_Bedrag,
										[Staat Bedrag Euro] = @Staat_Bedrag_Euro,
										[Staat Omschrijving] = @Staat_Omschrijving
								WHERE	[Staat-ID] = @Staat_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Staat_Commissie_ID", OleDbType.Integer).Value = Staat_Commissie_ID == null ? (Object)DBNull.Value : Staat_Commissie_ID;
					cmd.Parameters.Add("@Staat_Makelaar_ID", OleDbType.Integer).Value = Staat_Makelaar_ID == null ? (Object)DBNull.Value : Staat_Makelaar_ID;
					cmd.Parameters.Add("@Staat_Datum", OleDbType.Date).Value = Staat_Datum == null ? (Object)DBNull.Value : Staat_Datum;
					cmd.Parameters.Add("@Staat_Commissie", OleDbType.UnsignedTinyInt).Value = Staat_Commissie == null ? (Object)DBNull.Value : Staat_Commissie;
					cmd.Parameters.Add("@Staat_Commissiebedrag", OleDbType.Integer).Value = Staat_Commissiebedrag == null ? (Object)DBNull.Value : Staat_Commissiebedrag;
					cmd.Parameters.Add("@Staat_Commissiebedrag_Euro", OleDbType.Double).Value = Staat_Commissiebedrag_Euro == null ? (Object)DBNull.Value : Staat_Commissiebedrag_Euro;
					cmd.Parameters.Add("@Staat_Bedrag", OleDbType.Integer).Value = Staat_Bedrag == null ? (Object)DBNull.Value : Staat_Bedrag;
					cmd.Parameters.Add("@Staat_Bedrag_Euro", OleDbType.Double).Value = Staat_Bedrag_Euro == null ? (Object)DBNull.Value : Staat_Bedrag_Euro;
					cmd.Parameters.Add("@Staat_Omschrijving", OleDbType.WChar).Value = Staat_Omschrijving == null ? (Object)DBNull.Value : Staat_Omschrijving;
					cmd.Parameters.Add("@Staat_ID", OleDbType.Integer).Value = Staat_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int staat_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Staat]
								WHERE	[Staat-ID] = @Staat_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Staat_ID", OleDbType.Integer).Value = staat_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Staat Get(int staat_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Staat-ID],
										[Staat Commissie-ID],
										[Staat Makelaar-ID],
										[Staat Datum],
										[Staat Commissie],
										[Staat Commissiebedrag],
										[Staat Commissiebedrag Euro],
										[Staat Bedrag],
										[Staat Bedrag Euro],
										[Staat Omschrijving]
								FROM	[Staat]
								WHERE	[Staat-ID] = @Staat_ID;";

				Staat staat = new Staat();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Staat_ID", OleDbType.Integer).Value = staat_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							staat.Staat_ID = Convert.ToInt32(reader["Staat-ID"]);
							staat.Staat_Commissie_ID = reader["Staat Commissie-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Staat Commissie-ID"]);
							staat.Staat_Makelaar_ID = reader["Staat Makelaar-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Staat Makelaar-ID"]);
							staat.Staat_Datum = reader["Staat Datum"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(reader["Staat Datum"]);
							staat.Staat_Commissie = reader["Staat Commissie"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Staat Commissie"]);
							staat.Staat_Commissiebedrag = reader["Staat Commissiebedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Staat Commissiebedrag"]);
							staat.Staat_Commissiebedrag_Euro = reader["Staat Commissiebedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Staat Commissiebedrag Euro"]);
							staat.Staat_Bedrag = reader["Staat Bedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Staat Bedrag"]);
							staat.Staat_Bedrag_Euro = reader["Staat Bedrag Euro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["Staat Bedrag Euro"]);
							staat.Staat_Omschrijving = reader["Staat Omschrijving"] == DBNull.Value ? null : reader["Staat Omschrijving"].ToString();
						}
					}
				}

				return staat;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Staat-ID],
										[Staat Commissie-ID],
										[Staat Makelaar-ID],
										[Staat Datum],
										[Staat Commissie],
										[Staat Commissiebedrag],
										[Staat Commissiebedrag Euro],
										[Staat Bedrag],
										[Staat Bedrag Euro],
										[Staat Omschrijving]
								FROM	[Staat];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}