using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class OmzetPerJaar
	{
		#region Properties
		public int OmzetJaarID { get; set; }
		public string OmzetJaarKantoor { get; set; }
		public string OmzetJaarJaar { get; set; }
		public int? OmzetJaarBedrag { get; set; }
		public double? OmzetJaarBedragEuro { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [OmzetPerJaar]
								(
									[OmzetJaarKantoor],
									[OmzetJaarJaar],
									[OmzetJaarBedrag],
									[OmzetJaarBedragEuro]
								)
								VALUES
								(
									@OmzetJaarKantoor,
									@OmzetJaarJaar,
									@OmzetJaarBedrag,
									@OmzetJaarBedragEuro
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzetJaarKantoor", OleDbType.WChar).Value = OmzetJaarKantoor == null ? (Object)DBNull.Value : OmzetJaarKantoor;
					cmd.Parameters.Add("@OmzetJaarJaar", OleDbType.WChar).Value = OmzetJaarJaar == null ? (Object)DBNull.Value : OmzetJaarJaar;
					cmd.Parameters.Add("@OmzetJaarBedrag", OleDbType.Integer).Value = OmzetJaarBedrag == null ? (Object)DBNull.Value : OmzetJaarBedrag;
					cmd.Parameters.Add("@OmzetJaarBedragEuro", OleDbType.Double).Value = OmzetJaarBedragEuro == null ? (Object)DBNull.Value : OmzetJaarBedragEuro;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					OmzetJaarID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[OmzetPerJaar]
								SET		[OmzetJaarKantoor] = @OmzetJaarKantoor,
										[OmzetJaarJaar] = @OmzetJaarJaar,
										[OmzetJaarBedrag] = @OmzetJaarBedrag,
										[OmzetJaarBedragEuro] = @OmzetJaarBedragEuro
								WHERE	[OmzetJaarID] = @OmzetJaarID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzetJaarKantoor", OleDbType.WChar).Value = OmzetJaarKantoor == null ? (Object)DBNull.Value : OmzetJaarKantoor;
					cmd.Parameters.Add("@OmzetJaarJaar", OleDbType.WChar).Value = OmzetJaarJaar == null ? (Object)DBNull.Value : OmzetJaarJaar;
					cmd.Parameters.Add("@OmzetJaarBedrag", OleDbType.Integer).Value = OmzetJaarBedrag == null ? (Object)DBNull.Value : OmzetJaarBedrag;
					cmd.Parameters.Add("@OmzetJaarBedragEuro", OleDbType.Double).Value = OmzetJaarBedragEuro == null ? (Object)DBNull.Value : OmzetJaarBedragEuro;
					cmd.Parameters.Add("@OmzetJaarID", OleDbType.Integer).Value = OmzetJaarID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int omzetJaarID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [OmzetPerJaar]
								WHERE	[OmzetJaarID] = @OmzetJaarID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzetJaarID", OleDbType.Integer).Value = omzetJaarID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static OmzetPerJaar Get(int omzetJaarID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[OmzetJaarID],
										[OmzetJaarKantoor],
										[OmzetJaarJaar],
										[OmzetJaarBedrag],
										[OmzetJaarBedragEuro]
								FROM	[OmzetPerJaar]
								WHERE	[OmzetJaarID] = @OmzetJaarID;";

				OmzetPerJaar omzetPerJaar = new OmzetPerJaar();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@OmzetJaarID", OleDbType.Integer).Value = omzetJaarID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							omzetPerJaar.OmzetJaarID = Convert.ToInt32(reader["OmzetJaarID"]);
							omzetPerJaar.OmzetJaarKantoor = reader["OmzetJaarKantoor"] == DBNull.Value ? null : reader["OmzetJaarKantoor"].ToString();
							omzetPerJaar.OmzetJaarJaar = reader["OmzetJaarJaar"] == DBNull.Value ? null : reader["OmzetJaarJaar"].ToString();
							omzetPerJaar.OmzetJaarBedrag = reader["OmzetJaarBedrag"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["OmzetJaarBedrag"]);
							omzetPerJaar.OmzetJaarBedragEuro = reader["OmzetJaarBedragEuro"] == DBNull.Value ? (double?)null : Convert.ToDouble(reader["OmzetJaarBedragEuro"]);
						}
					}
				}

				return omzetPerJaar;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[OmzetJaarID],
										[OmzetJaarKantoor],
										[OmzetJaarJaar],
										[OmzetJaarBedrag],
										[OmzetJaarBedragEuro]
								FROM	[OmzetPerJaar];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}