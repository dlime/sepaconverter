using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.OleDb;

namespace Sofimo.Data
{
	public class Huurder
	{
		#region Properties
		public int Huurder_ID { get; set; }
		public int? Huurder_Gemeente_ID { get; set; }
		public int? Huurder_Land_ID { get; set; }
		public int? Huurder_Advokaat_ID { get; set; }
		public int? Huurder_Eigendom__ID { get; set; }
		public string Huurder_Oud_nummer { get; set; }
		public string Huurder_Naam { get; set; }
		public string Huurder_Straat { get; set; }
		public string Huurder_Bus { get; set; }
		public byte? Huurder_Aanspreking { get; set; }
		public string Huurder_Telefoon { get; set; }
		public string Huurder_Rekeningnummer { get; set; }
		public string Huurder_BTW_nummer { get; set; }
		public bool Huurder_Export { get; set; }
		public string Huurder_Memo { get; set; }
		#endregion

		#region Add
		/// <summary>
		/// Adds a new record.
		/// </summary>
		public void Add()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"INSERT INTO [Huurder]
								(
									[Huurder Gemeente-ID],
									[Huurder Land-ID],
									[Huurder Advokaat-ID],
									[Huurder Eigendom -ID],
									[Huurder Oud nummer],
									[Huurder Naam],
									[Huurder Straat],
									[Huurder Bus],
									[Huurder Aanspreking],
									[Huurder Telefoon],
									[Huurder Rekeningnummer],
									[Huurder BTW-nummer],
									[Huurder Export],
									[Huurder Memo]
								)
								VALUES
								(
									@Huurder_Gemeente_ID,
									@Huurder_Land_ID,
									@Huurder_Advokaat_ID,
									@Huurder_Eigendom__ID,
									@Huurder_Oud_nummer,
									@Huurder_Naam,
									@Huurder_Straat,
									@Huurder_Bus,
									@Huurder_Aanspreking,
									@Huurder_Telefoon,
									@Huurder_Rekeningnummer,
									@Huurder_BTW_nummer,
									@Huurder_Export,
									@Huurder_Memo
								);";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Huurder_Gemeente_ID", OleDbType.Integer).Value = Huurder_Gemeente_ID == null ? (Object)DBNull.Value : Huurder_Gemeente_ID;
					cmd.Parameters.Add("@Huurder_Land_ID", OleDbType.Integer).Value = Huurder_Land_ID == null ? (Object)DBNull.Value : Huurder_Land_ID;
					cmd.Parameters.Add("@Huurder_Advokaat_ID", OleDbType.Integer).Value = Huurder_Advokaat_ID == null ? (Object)DBNull.Value : Huurder_Advokaat_ID;
					cmd.Parameters.Add("@Huurder_Eigendom__ID", OleDbType.Integer).Value = Huurder_Eigendom__ID == null ? (Object)DBNull.Value : Huurder_Eigendom__ID;
					cmd.Parameters.Add("@Huurder_Oud_nummer", OleDbType.WChar).Value = Huurder_Oud_nummer == null ? (Object)DBNull.Value : Huurder_Oud_nummer;
					cmd.Parameters.Add("@Huurder_Naam", OleDbType.WChar).Value = Huurder_Naam == null ? (Object)DBNull.Value : Huurder_Naam;
					cmd.Parameters.Add("@Huurder_Straat", OleDbType.WChar).Value = Huurder_Straat == null ? (Object)DBNull.Value : Huurder_Straat;
					cmd.Parameters.Add("@Huurder_Bus", OleDbType.WChar).Value = Huurder_Bus == null ? (Object)DBNull.Value : Huurder_Bus;
					cmd.Parameters.Add("@Huurder_Aanspreking", OleDbType.UnsignedTinyInt).Value = Huurder_Aanspreking == null ? (Object)DBNull.Value : Huurder_Aanspreking;
					cmd.Parameters.Add("@Huurder_Telefoon", OleDbType.WChar).Value = Huurder_Telefoon == null ? (Object)DBNull.Value : Huurder_Telefoon;
					cmd.Parameters.Add("@Huurder_Rekeningnummer", OleDbType.WChar).Value = Huurder_Rekeningnummer == null ? (Object)DBNull.Value : Huurder_Rekeningnummer;
					cmd.Parameters.Add("@Huurder_BTW_nummer", OleDbType.WChar).Value = Huurder_BTW_nummer == null ? (Object)DBNull.Value : Huurder_BTW_nummer;
					cmd.Parameters.Add("@Huurder_Export", OleDbType.Boolean).Value = Huurder_Export;
					cmd.Parameters.Add("@Huurder_Memo", OleDbType.WChar).Value = Huurder_Memo == null ? (Object)DBNull.Value : Huurder_Memo;
					cmd.ExecuteNonQuery();

					// Get value of the identity column.
					cmd.CommandText = "SELECT @@Identity;";
					Huurder_ID = Convert.ToInt32(cmd.ExecuteScalar());
				}

				con.Close();
			}
		}
		#endregion

		#region Update
		/// <summary>
		/// Updates an existing record.
		/// </summary>
		public void Update()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"UPDATE	[Huurder]
								SET		[Huurder Gemeente-ID] = @Huurder_Gemeente_ID,
										[Huurder Land-ID] = @Huurder_Land_ID,
										[Huurder Advokaat-ID] = @Huurder_Advokaat_ID,
										[Huurder Eigendom -ID] = @Huurder_Eigendom__ID,
										[Huurder Oud nummer] = @Huurder_Oud_nummer,
										[Huurder Naam] = @Huurder_Naam,
										[Huurder Straat] = @Huurder_Straat,
										[Huurder Bus] = @Huurder_Bus,
										[Huurder Aanspreking] = @Huurder_Aanspreking,
										[Huurder Telefoon] = @Huurder_Telefoon,
										[Huurder Rekeningnummer] = @Huurder_Rekeningnummer,
										[Huurder BTW-nummer] = @Huurder_BTW_nummer,
										[Huurder Export] = @Huurder_Export,
										[Huurder Memo] = @Huurder_Memo
								WHERE	[Huurder-ID] = @Huurder_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Huurder_Gemeente_ID", OleDbType.Integer).Value = Huurder_Gemeente_ID == null ? (Object)DBNull.Value : Huurder_Gemeente_ID;
					cmd.Parameters.Add("@Huurder_Land_ID", OleDbType.Integer).Value = Huurder_Land_ID == null ? (Object)DBNull.Value : Huurder_Land_ID;
					cmd.Parameters.Add("@Huurder_Advokaat_ID", OleDbType.Integer).Value = Huurder_Advokaat_ID == null ? (Object)DBNull.Value : Huurder_Advokaat_ID;
					cmd.Parameters.Add("@Huurder_Eigendom__ID", OleDbType.Integer).Value = Huurder_Eigendom__ID == null ? (Object)DBNull.Value : Huurder_Eigendom__ID;
					cmd.Parameters.Add("@Huurder_Oud_nummer", OleDbType.WChar).Value = Huurder_Oud_nummer == null ? (Object)DBNull.Value : Huurder_Oud_nummer;
					cmd.Parameters.Add("@Huurder_Naam", OleDbType.WChar).Value = Huurder_Naam == null ? (Object)DBNull.Value : Huurder_Naam;
					cmd.Parameters.Add("@Huurder_Straat", OleDbType.WChar).Value = Huurder_Straat == null ? (Object)DBNull.Value : Huurder_Straat;
					cmd.Parameters.Add("@Huurder_Bus", OleDbType.WChar).Value = Huurder_Bus == null ? (Object)DBNull.Value : Huurder_Bus;
					cmd.Parameters.Add("@Huurder_Aanspreking", OleDbType.UnsignedTinyInt).Value = Huurder_Aanspreking == null ? (Object)DBNull.Value : Huurder_Aanspreking;
					cmd.Parameters.Add("@Huurder_Telefoon", OleDbType.WChar).Value = Huurder_Telefoon == null ? (Object)DBNull.Value : Huurder_Telefoon;
					cmd.Parameters.Add("@Huurder_Rekeningnummer", OleDbType.WChar).Value = Huurder_Rekeningnummer == null ? (Object)DBNull.Value : Huurder_Rekeningnummer;
					cmd.Parameters.Add("@Huurder_BTW_nummer", OleDbType.WChar).Value = Huurder_BTW_nummer == null ? (Object)DBNull.Value : Huurder_BTW_nummer;
					cmd.Parameters.Add("@Huurder_Export", OleDbType.Boolean).Value = Huurder_Export;
					cmd.Parameters.Add("@Huurder_Memo", OleDbType.WChar).Value = Huurder_Memo == null ? (Object)DBNull.Value : Huurder_Memo;
					cmd.Parameters.Add("@Huurder_ID", OleDbType.Integer).Value = Huurder_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Delete
		/// <summary>
		/// Deletes an existing record.
		/// </summary>
		public static void Delete(int huurder_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"DELETE	FROM [Huurder]
								WHERE	[Huurder-ID] = @Huurder_ID;";

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Huurder_ID", OleDbType.Integer).Value = huurder_ID;
					cmd.ExecuteNonQuery();
				}

				con.Close();
			}
		}
		#endregion

		#region Get
		/// <summary>
		/// Gets an existing record.
		/// </summary>
		public static Huurder Get(int huurder_ID)
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				String sql =  @"SELECT	[Huurder-ID],
										[Huurder Gemeente-ID],
										[Huurder Land-ID],
										[Huurder Advokaat-ID],
										[Huurder Eigendom -ID],
										[Huurder Oud nummer],
										[Huurder Naam],
										[Huurder Straat],
										[Huurder Bus],
										[Huurder Aanspreking],
										[Huurder Telefoon],
										[Huurder Rekeningnummer],
										[Huurder BTW-nummer],
										[Huurder Export],
										[Huurder Memo]
								FROM	[Huurder]
								WHERE	[Huurder-ID] = @Huurder_ID;";

				Huurder huurder = new Huurder();

				con.Open();

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					cmd.Parameters.Add("@Huurder_ID", OleDbType.Integer).Value = huurder_ID;

					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						if (reader.Read())
						{
							huurder.Huurder_ID = Convert.ToInt32(reader["Huurder-ID"]);
							huurder.Huurder_Gemeente_ID = reader["Huurder Gemeente-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurder Gemeente-ID"]);
							huurder.Huurder_Land_ID = reader["Huurder Land-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurder Land-ID"]);
							huurder.Huurder_Advokaat_ID = reader["Huurder Advokaat-ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurder Advokaat-ID"]);
							huurder.Huurder_Eigendom__ID = reader["Huurder Eigendom -ID"] == DBNull.Value ? (int?)null : Convert.ToInt32(reader["Huurder Eigendom -ID"]);
							huurder.Huurder_Oud_nummer = reader["Huurder Oud nummer"] == DBNull.Value ? null : reader["Huurder Oud nummer"].ToString();
							huurder.Huurder_Naam = reader["Huurder Naam"] == DBNull.Value ? null : reader["Huurder Naam"].ToString();
							huurder.Huurder_Straat = reader["Huurder Straat"] == DBNull.Value ? null : reader["Huurder Straat"].ToString();
							huurder.Huurder_Bus = reader["Huurder Bus"] == DBNull.Value ? null : reader["Huurder Bus"].ToString();
							huurder.Huurder_Aanspreking = reader["Huurder Aanspreking"] == DBNull.Value ? (byte?)null : Convert.ToByte(reader["Huurder Aanspreking"]);
							huurder.Huurder_Telefoon = reader["Huurder Telefoon"] == DBNull.Value ? null : reader["Huurder Telefoon"].ToString();
							huurder.Huurder_Rekeningnummer = reader["Huurder Rekeningnummer"] == DBNull.Value ? null : reader["Huurder Rekeningnummer"].ToString();
							huurder.Huurder_BTW_nummer = reader["Huurder BTW-nummer"] == DBNull.Value ? null : reader["Huurder BTW-nummer"].ToString();
							huurder.Huurder_Export = Convert.ToBoolean(reader["Huurder Export"]);
							huurder.Huurder_Memo = reader["Huurder Memo"] == DBNull.Value ? null : reader["Huurder Memo"].ToString();
						}
					}
				}

				return huurder;
			}
		}
		#endregion

		#region GetAll
		/// <summary>
		/// Gets all records.
		/// </summary>
		public static DataTable GetAll()
		{
			String connectionString = ConfigurationManager.ConnectionStrings["DatabaseConnectionString"].ConnectionString;

			using (OleDbConnection con = new OleDbConnection(connectionString))
			{
				DataTable dataTable = new DataTable();

				con.Open();

				String sql =  @"SELECT	[Huurder-ID],
										[Huurder Gemeente-ID],
										[Huurder Land-ID],
										[Huurder Advokaat-ID],
										[Huurder Eigendom -ID],
										[Huurder Oud nummer],
										[Huurder Naam],
										[Huurder Straat],
										[Huurder Bus],
										[Huurder Aanspreking],
										[Huurder Telefoon],
										[Huurder Rekeningnummer],
										[Huurder BTW-nummer],
										[Huurder Export],
										[Huurder Memo]
								FROM	[Huurder];";

				using (OleDbCommand cmd = new OleDbCommand(sql, con))
				{
					using (OleDbDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
					{
						dataTable.Load(reader);
					}
				}

				return dataTable;
			}
		}
		#endregion
	}
}